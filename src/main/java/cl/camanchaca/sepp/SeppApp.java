/*
 * SeppApp.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.controller.LoginController;
import cl.camanchaca.sepp.database.ConfigFileException;
import cl.camanchaca.sepp.database.DatabaseLoader;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.scene.JFXStage;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.HostServicesWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import net.sf.image4j.codec.ico.ICODecoder;

import java.io.File;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SeppApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        SQLServer db = null;

        try {
            db = DatabaseLoader.loadDatabase(new File("config.xml"));
            ServiceLocator.registerService(db);
        } catch (ConfigFileException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sistema de Cargos");
            alert.setHeaderText("Error en el archivo de configuracion");
            alert.setContentText("No se pudo cargar la instancia de base de datos, el archivo " +
                    "no poseee el formato correcto");
            alert.showAndWait();
            System.exit(1);
        }

        try {
            db.connect();

            ServiceLocator.registerService(db);
        } catch (SQLException | ClassNotFoundException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sistema de Cargos");
            alert.setHeaderText("Error en la Base de Datos");
            alert.setContentText("La base de datos especificada no existe");
            alert.showAndWait();
            System.exit(1);
        }

        ServiceLocator.registerService(new ExecutorWrapper(Executors.newSingleThreadExecutor()));
        ServiceLocator.registerService(new HostServicesWrapper(getHostServices()));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/login.fxml"));

        File icon = new File("../Sistema de Cargos.ico");

        if (icon.exists()) {
            JFXStage.setApplicationIcon(ICODecoder.read(icon));
        }

        JFXStage stageWrapper = new JFXStage(primaryStage, loader.load(), 500, 400, false);
        stageWrapper.setTitle("Sistema de Cargos");

        LoginController controller = loader.getController();
        ContentValues bundle = new ContentValues();
        bundle.put("stage", stageWrapper);
        controller.setData(bundle);

        stageWrapper.show();
    }
}
