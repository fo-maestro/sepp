/*
 * ActiveDirectory.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.login;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class ActiveDirectory {

    private ActiveDirectory() {
    }

    public static boolean login(String server, String user, String password) {
        Hashtable<String, String> props = new Hashtable<>();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://" + server);
        props.put(Context.SECURITY_AUTHENTICATION, "simple");
        props.put(Context.SECURITY_PRINCIPAL, user + "@" + extractDomain(server));
        props.put(Context.SECURITY_CREDENTIALS, password);

        try {
            new InitialDirContext(props);

            return true;
        } catch (NamingException e) {
            return false;
        }
    }

    private static String extractDomain(String server) {
        String[] splited = server.split("\\.");

        if (splited.length == 3) {
            return splited[1] + "." + splited[2];
        } else {
            return server;
        }
    }
}
