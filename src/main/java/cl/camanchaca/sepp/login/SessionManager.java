/*
 * Session.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.login;

import cl.camanchaca.sepp.model.Cuenta;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.service.Service;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SessionManager implements Service {
    private Cuenta account;
    private Personal user;

    public SessionManager(Cuenta account, Personal user) {
        this.account = account;
        this.user = user;
    }

    public Cuenta getAccount() {
        return account;
    }

    public Personal getUser() {
        return user;
    }
}
