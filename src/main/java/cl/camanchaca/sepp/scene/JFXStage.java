/*
 * JFSTage.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene;

import com.jfoenix.controls.JFXDecorator;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.ContextMenuEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class JFXStage {
    private JFXDecorator decorator;
    private Stage stage;
    private Scene scene;
    private static Collection<Image> ICON = new ArrayList<>();

    public JFXStage(Node content, double width, double height, boolean maximizable) {
        this(null, content, width, height, maximizable);
    }

    public JFXStage(Stage stage, Node content, double width, double height, boolean maximizable) {
        if (stage == null) {
            this.stage = new Stage();
        } else {
            this.stage = stage;
        }

        decorator = new JFXDecorator(this.stage, content, false, maximizable, true);
        decorator.setCustomMaximize(true);

        scene = new Scene(decorator, width, height);
        scene.getStylesheets().addAll(getClass().getResource("/css/jfoenix-fonts.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-design.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-main.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-custom.css")
                .toExternalForm());

        this.stage.setMinWidth(width - 100);
        this.stage.setMinHeight(height - 50);
        this.stage.setScene(scene);
        this.stage.setResizable(false);

        if (ICON != null) {
            setIcon(ICON);
        }
    }

    public JFXStage(Node content, boolean maximizable) {
        stage = new Stage();

        decorator = new JFXDecorator(this.stage, content, false, maximizable, true);
        decorator.setCustomMaximize(true);
        decorator.setMaximized(true);

        scene = new Scene(decorator, stage.getWidth(), stage.getHeight());
        scene.getStylesheets().addAll(getClass().getResource("/css/jfoenix-fonts.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-design.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-main.css")
                .toExternalForm(), getClass().getResource("/css/jfoenix-custom.css")
                .toExternalForm());

        this.stage.setMinWidth(stage.getWidth() - 100);
        this.stage.setMinHeight(stage.getHeight() - 50);
        this.stage.setScene(scene);
        this.stage.setResizable(false);

        if (ICON != null) {
            setIcon(ICON);
        }
    }

    public void setIcon(Image e) {
        stage.getIcons().add(e);
    }

    public void setIcon(Collection<? extends Image> e) {
        stage.getIcons().addAll(e);
    }

    public void setAlwaysOnTop(boolean value) {
        stage.setAlwaysOnTop(value);
    }

    public void setTitle(String title) {
        stage.setTitle(title);
    }

    public String getTitle() {
        return stage.getTitle();
    }

    public void setMaximized(boolean maximized) {
        decorator.setMaximized(maximized);
        stage.setMaximized(maximized);
    }

    public double getWidth() {
        return stage.getWidth();
    }

    public double getHeight() {
        return stage.getHeight();
    }

    public void disableContextMenu() {
        scene.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
    }

    public void setOnCloseRequest(Runnable action) {
        decorator.setOnCloseButtonAction(action);
    }

    public void show() {
        stage.show();
    }

    public void showAndWait() {
        stage.showAndWait();
    }

    public void initModality(Modality modality) {
        stage.initModality(modality);
    }

    public Stage getFxStage() {
        return stage;
    }

    public void close() {
        stage.close();
    }

    public static void setApplicationIcon(Image icon) {
        JFXStage.ICON.add(icon);
    }

    public static void setApplicationIcon(Collection<? extends BufferedImage> icons) {
        icons.forEach(o -> ICON.add(SwingFXUtils.toFXImage(o, null)));
    }
}
