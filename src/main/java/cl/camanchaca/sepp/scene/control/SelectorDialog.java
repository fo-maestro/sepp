/*
 * SelectorDialog.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SelectorDialog<T> extends Dialog {
    private VBox root;
    private TableView<T> table;
    private JFXTextField searchBar;
    private ObservableList<T> data;

    private T selected;

    public SelectorDialog(Node background) {
        super(background);

        root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);
        root.setPadding(new Insets(10));

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setSpacing(5);
        hBox.getChildren().add(searchBar = new JFXTextField());

        root.getChildren().add(hBox);

        table = new TableView<>();
        table.setPrefWidth(620);
        VBox.setVgrow(table, Priority.ALWAYS);

        root.getChildren().add(table);

        table.setRowFactory(param -> {
            TableRow<T> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                selected = row.getItem();

                close();
            });

            return row;
        });

        super.setContent(root, 700, 500);
    }

    public void setColumns(ObservableList<TableColumn<T, ?>> columns) {
        table.getColumns().addAll(columns);
        TableConfigurator.setImmutable(table, columns);
    }

    public void setColumnRelationSize(int base, int... relationSize) {
        TableConfigurator.setRelationSize(table, table.getColumns(), base, relationSize);
    }

    public void setData(ObservableList<T> data) {
        this.data = data;
    }

    public void setUpSearchBar(String promptText, TableConfigurator.Search<T> predicate) {
        searchBar.setPromptText(promptText);

        TableConfigurator.setUpSearchBar(searchBar, table, data, predicate);
    }

    public T showAndWait() {
        Platform.runLater(searchBar::requestFocus);
        super.showAndWait();

        return selected;
    }
}
