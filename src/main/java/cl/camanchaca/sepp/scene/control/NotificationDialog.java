/*
 * Dialog.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class NotificationDialog extends JFXDialog {
    private StackPane context;
    private Label header;
    private Label content;
    private JFXButton accept;

    public NotificationDialog(StackPane context) {
        this(context, false);
    }

    public NotificationDialog(StackPane context, boolean scrollable) {
        this.context = context;
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.getHeading().add(header = new Label("Header"));

        if (scrollable) {
            ScrollPane scroll = new ScrollPane(content = new Label("Content"));
            layout.getBody().add(scroll);
        } else {
            layout.getBody().add(content = new Label("Content"));
        }

        layout.getActions().add(accept = new JFXButton("Salir"));
        super.setContent(layout);

        accept.getStyleClass().add("dialog-accept");
        accept.setOnAction(event -> super.close());
        setOverlayClose(false);
    }

    public void setTitle(String text) {
        header.setText(text);
    }

    public void setContentText(String text) {
        content.setText(text);
    }

    public JFXButton getActionButton() {
        return accept;
    }

    @Override
    public void show() {
        super.show(context);
    }
}
