/*
 * JFXTableView.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class TableConfigurator {

    private TableConfigurator() {
    }

    public static <T>
    void setImmutable(TableView<T> table, ObservableList<TableColumn<T, ?>> columns) {
        columns.forEach(column -> column.setResizable(false));

        table.getColumns().addListener(new ListChangeListener<TableColumn<T, ?>>() {
            boolean suspend;

            @Override
            public void onChanged(Change<? extends TableColumn<T, ?>> c) {
                c.next();

                if (c.wasReplaced() && !suspend) {
                    this.suspend = true;
                    table.getColumns().setAll(columns);
                    this.suspend = false;
                }
            }
        });
    }

    public static <T>
    void setRelationSize(TableView<T> table, ObservableList<TableColumn<T, ?>> columns,
                         int base, int... relationSize) {
        for (int i = 0; i < columns.size(); i++) {
            columns.get(i).prefWidthProperty()
                    .bind(table.widthProperty().divide(base).multiply(relationSize[i]));
        }
    }

    public static <T>
    void setUpSearchBar(JFXTextField bar, TableView<T> table, ObservableList<T>
            data, Search<T> predicate) {
        FilteredList<T> filter = new FilteredList<>(data, p -> true);

        bar.textProperty().addListener((observable, oldValue, newValue) ->
            filter.setPredicate(t -> predicate.eval(newValue, t)));

        SortedList<T> sorted = new SortedList<T>(filter);
        sorted.comparatorProperty().bind(table.comparatorProperty());

        table.setItems(sorted);
    }

    public interface Search<T> {
        boolean eval(String val, T item);
    }
}
