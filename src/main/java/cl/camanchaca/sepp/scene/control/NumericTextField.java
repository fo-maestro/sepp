/*
 * NumericTextField.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ObservableValue;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class NumericTextField extends JFXTextField {

    public NumericTextField() {
        super.textProperty().addListener(this::listenerHandler);
    }

    public NumericTextField(String text) {
        super(text);
        super.textProperty().addListener(this::listenerHandler);
    }

    private void listenerHandler(ObservableValue<? extends String> observable, String oldValue,
                                 String newValue) {
        if (!newValue.matches("\\d*")) {
            super.setText(newValue.replaceAll("[^\\d]", ""));
        }
    }


}
