/*
 * LimiterTextField.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ObservableValue;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class LimiterTextField extends JFXTextField {
    private int limitLenght;

    public LimiterTextField(int limitLenght) {
        this.limitLenght = limitLenght;
        super.textProperty().addListener(this::listenerHanler);
    }

    public LimiterTextField(String text, int limitLenght) {
        super(text);
        this.limitLenght = limitLenght;
        super.textProperty().addListener(this::listenerHanler);
    }

    private void listenerHanler(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
        if (newValue.length() > limitLenght) {
            super.setText(newValue.substring(0, limitLenght));
        }
    }
}
