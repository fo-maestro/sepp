/*
 * TableColumn.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ContainerTableCell<T, K> extends TableCell<T, K> {
    private Filler<K> filler;

    public ContainerTableCell(Filler<K> filler) {
        this.filler = filler;
    }

    @Override
    protected void updateItem(K item, boolean empty) {
        super.updateItem(item, empty);

        if (item != null) {
            HBox container = new HBox();
            container.setSpacing(5);
            container.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
            container.setPrefSize(USE_COMPUTED_SIZE, USE_COMPUTED_SIZE);
            container.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

            if (filler != null) {
                filler.fill(item, container);
            }

            setGraphic(container);
        } else {
            setGraphic(null);
        }
    }

    public interface Filler<T> {
        void fill(T item, HBox container);
    }
}
