/*
 * Dialog.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.scene.control;

import cl.camanchaca.sepp.scene.JFXStage;
import javafx.scene.Node;
import javafx.stage.Modality;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Dialog<T> {
    private JFXStage context;
    private Node background;
    private boolean active;

    public Dialog() {
        this(null);
    }

    public Dialog(Node background) {
        this.background = background;
    }

    public boolean isActive() {
        return active;
    }

    public void setContent(Node conent, double width, double height) {
        context = new JFXStage(conent, width, height, false);
        context.disableContextMenu();
        context.setOnCloseRequest(this::close);
    }

    public void setContent(Node content, boolean maximized) {
        setContent(content, background.getLayoutBounds().getWidth(), background.getLayoutBounds().getHeight());
        context.setMaximized(maximized);
    }

    public void setTitle(String title) {
        context.setTitle(title);
    }

    public JFXStage getContext() {
        return context;
    }

    public void show() {
        active = true;
        context.initModality(Modality.WINDOW_MODAL);
        context.show();
    }

    public T showAndWait() {
        active = true;
        context.initModality(Modality.APPLICATION_MODAL);

        if (background != null) {
            background.setDisable(true);
            context.showAndWait();
            background.setDisable(false);
        } else {
            context.showAndWait();
        }

        active = false;

        return null;
    }

    public void close() {
        active = false;

        if (background != null) {
            background.setDisable(false);
        }

        context.close();
    }
}
