/*
 * Spinner.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXSpinner;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Spinner {
    private StackPane root;
    private BorderPane container;
    private boolean enable;

    public Spinner(StackPane root) {
        this.root = root;
        container = new BorderPane();
        container.maxWidth(Double.MAX_VALUE);
        container.maxHeight(Double.MAX_VALUE);
        container.setStyle("-fx-background-color: rgba(211, 211, 211, 0.5)");
        JFXSpinner spinner = new JFXSpinner();
        spinner.setRadius(24);
        spinner.setStartingAngle(0);
        spinner.getStyleClass().add("materialDesign-blue");
        BorderPane.setAlignment(spinner, Pos.CENTER);
        container.setCenter(spinner);
    }

    public void enable() {
        root.getChildren().add(container);
        enable = true;
    }

    public void disable() {
        root.getChildren().remove(container);
        enable = false;
    }

    public boolean isEnable() {
        return enable;
    }
}
