/*
 * AlertDialog.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.scene.control;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class AlertDialog extends Dialog<Void> {
    private Label contentText;
    private JFXButton accept;
    private JFXButton cancel;

    public AlertDialog(Node background, double width, double height) {
        super(background);

        VBox content = new VBox();
        content.setSpacing(20);
        content.setAlignment(Pos.BOTTOM_LEFT);
        content.setPadding(new Insets(10, 10, 10, 10));
        content.getChildren().add(contentText = new Label());
        HBox buttons = new HBox();
        buttons.setSpacing(5);
        buttons.setAlignment(Pos.CENTER_RIGHT);
        buttons.getChildren().addAll(accept = new JFXButton(), cancel = new JFXButton());
        content.getChildren().add(buttons);

        accept.getStyleClass().add("create-button");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);

        super.setContent(content, width, height);
    }

    public void setMessage(String message) {
        contentText.setText(message);
    }

    public void setPossitiveButton(String name, EventHandler<ActionEvent> event) {
        accept.setText(name);
        accept.setOnAction(event);
    }

    public void setNegativeButton(String name) {
        cancel.setText(name);
        cancel.setOnAction(event -> super.close());
    }
}
