/*
 * NumericTextFieldEditorBuilder.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.scene.control;

import javafx.application.Platform;
import javafx.scene.control.TableCell;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class NumericTextFieldEditorBuilder<T> extends TableCell<T, Integer> {

    private NumericTextField textField;

    @Override
    public void startEdit() {
        if (editableProperty().get()){
            if (!isEmpty()) {
                super.startEdit();
            }
        }
    }

    private void createTextField() {
        textField = new NumericTextField();
        textField.setText(null);
    }


    @Override
    public void cancelEdit() {
    }

    @Override
    public void updateItem(Integer item, boolean empty) {
        Platform.runLater(() -> {
            textField.selectAll();
            textField.requestFocus();
        });
    }
}
