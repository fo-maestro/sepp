/*
 * HistorialDetalleController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.model.SolicitudInsumoDetalle;
import cl.camanchaca.sepp.model.SolicitudPersonal;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudInsumoDetalleDaoImpl;
import cl.camanchaca.sepp.scene.control.NotificationDialog;
import cl.camanchaca.sepp.scene.control.TableConfigurator;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class HistorialDetalleController implements Communicable {
    @FXML
    private Label solicitudLabel;

    @FXML
    private Label rutSupervisorLabel;

    @FXML
    private Label rutColaboradorLabel;

    @FXML
    private Label rutAdministradorLabel;

    @FXML
    private Label estadoLabel;

    @FXML
    private Label fechaLabel;

    @FXML
    private Label nombreSupervisorLabel;

    @FXML
    private Label nombreColaboradorLabel;

    @FXML
    private Label nombreAdministradorLabel;

    @FXML
    private Label solicitud;

    @FXML
    private Label fecha;

    @FXML
    private Label rutSupervisor;

    @FXML
    private Label nombreSupervisor;

    @FXML
    private Label rutColaborador;

    @FXML
    private Label nombreColaborador;

    @FXML
    private Label rutAdministrador;

    @FXML
    private Label nombreAdministrador;

    @FXML
    private Label estado;

    @FXML
    private StackPane root;

    @FXML
    private TableView<SolicitudInsumoDetalle> table;

    private SolicitudPersonal solicitudPersonal;
    private ObservableList<SolicitudInsumoDetalle> insumos;
    private ObservableList<SolicitudInsumo> cloned;

    @FXML
    public void initialize() {
        Text calculator = new Text(rutAdministradorLabel.getText());
        solicitudLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());
        rutSupervisorLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());
        rutColaboradorLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());
        estadoLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());

        rutAdministradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        calculator = new Text(nombreAdministradorLabel.getText());
        fechaLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreSupervisorLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreColaboradorLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreAdministradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        initTable();
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        TableColumn<SolicitudInsumoDetalle, Integer> codigoColumn =
                new TableColumn<>("Codigo");

        codigoColumn.setCellValueFactory(param ->
                param.getValue().getInsumo().codigoProperty().asObject());

        TableColumn<SolicitudInsumoDetalle, String> nombreColumn =
                new TableColumn<>("Nombre");

        nombreColumn.setCellValueFactory(param ->
                param.getValue().getInsumo().nombreProperty());

        TableColumn<SolicitudInsumoDetalle, Integer> solicitadoColumn =
                new TableColumn<>("Solicitado");

        solicitadoColumn.setCellValueFactory(param ->
                param.getValue().getSolicitudInsumo().cantidadSolicitadaProperty().asObject());

        TableColumn<SolicitudInsumoDetalle, String> rutEntregaColumn =
                new TableColumn<>("Rut Entrega");

        rutEntregaColumn.setCellValueFactory(param -> {
            Personal personal = new PersonalDaoImpl().getPersonal(param.getValue()
                    .getSolicitudInsumo().getRutEntrega());

            if (personal != null) {
                return new SimpleStringProperty(personal.getRut() + "-" + personal.getDvd());
            } else {
                return new SimpleStringProperty();
            }
        });

        TableColumn<SolicitudInsumoDetalle, String> nombreEntregaColumn =
                new TableColumn<>("Nombre Entrega");

        nombreEntregaColumn.setCellValueFactory(param -> {
            Personal personal = new PersonalDaoImpl().getPersonal(param.getValue()
                    .getSolicitudInsumo().getRutEntrega());

            if (personal != null) {
                return new SimpleStringProperty(personal.getNombre() + " " +
                        personal.getNombre2() + " " + personal.getApellidoP() + " " +
                        personal.getApellidoM());
            } else {
                return new SimpleStringProperty();
            }
        });

        TableColumn<SolicitudInsumoDetalle, String> fechaEntregaColumn =
                new TableColumn<>("Fecha Entrega");

        fechaEntregaColumn.setCellValueFactory(param ->
                param.getValue().getSolicitudInsumo().fechaEntregaProperty());

        TableColumn<SolicitudInsumoDetalle, String> obsColumn =
                new TableColumn<>("Observacion");

        obsColumn.setCellValueFactory(param ->
                param.getValue().getSolicitudInsumo().observacionProperty());

        ObservableList<TableColumn<SolicitudInsumoDetalle, ?>> columns =
                FXCollections.observableArrayList(codigoColumn, nombreColumn,
                        solicitadoColumn, rutEntregaColumn, nombreEntregaColumn, fechaEntregaColumn,
                        obsColumn);

        table.getColumns().addAll(columns);
        TableConfigurator.setRelationSize(table, columns, 15, 1, 3, 1, 1, 3, 1, 5);

        columns.forEach(column -> column.setSortable(false));
        TableConfigurator.setImmutable(table, columns);
    }

    @FXML
    private void handleBtnObs() {
        NotificationDialog dialog = new NotificationDialog(root);
        dialog.setTitle("Observacion Administrador");
        dialog.setContentText(solicitudPersonal.getSolicitud().getAdminComentario());
        dialog.show();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setData(ContentValues bundle) {
        solicitudPersonal = (SolicitudPersonal) bundle.get("solicitud");
        insumos = new SolicitudInsumoDetalleDaoImpl()
                .getSolicitudInsumo(solicitudPersonal.getSolicitud().getNumero());
    }

    @Override
    public void update() {
        Personal supervisor =
                new PersonalDaoImpl()
                        .getPersonal(solicitudPersonal.getSolicitud().getRutSupervisor());

        Personal solicitante = solicitudPersonal.getSolicitante();

        Personal administrador = new PersonalDaoImpl()
                .getPersonal(solicitudPersonal.getSolicitud().getRutAprueba());

        solicitud.setText(Integer.toString(solicitudPersonal.getSolicitud().getNumero()));
        fecha.setText(solicitudPersonal.getSolicitud().getFechaSolicitud());

        if (supervisor != null) {
            rutSupervisor.setText(supervisor.getRut() + "-" + supervisor.getDvd());
            nombreSupervisor.setText(supervisor.getNombre() + " " + supervisor.getNombre2() + " " +
                    supervisor.getApellidoP() + " " + supervisor.getApellidoM());
        }

        rutColaborador.setText(solicitante.getRut() + "-" + solicitante.getDvd());
        nombreColaborador.setText(solicitante.getNombre() + " " + solicitante.getNombre2() + " " +
                solicitante.getApellidoP() + " " + solicitante.getApellidoM());

        if (administrador != null) {
            rutAdministrador.setText(administrador.getRut() + "-" + administrador.getDvd());
            nombreAdministrador.setText(administrador.getNombre() + " " +
                    administrador.getNombre2() + " " + administrador.getApellidoP() + " " +
                    administrador.getApellidoM());
        }

        estado.setText(solicitudPersonal.getSolicitud().getEstado());

        table.setItems(insumos);

        cloned = FXCollections.observableArrayList();

        insumos.forEach(consumer -> cloned.add(consumer.getSolicitudInsumo().clone()));
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
