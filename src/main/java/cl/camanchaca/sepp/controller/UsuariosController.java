/*
 * UsuariosController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.model.Cuenta;
import cl.camanchaca.sepp.model.CuentaPersonal;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.access.CuentaDaoImpl;
import cl.camanchaca.sepp.model.access.CuentaPersonalDaoImpl;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.scene.control.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class UsuariosController implements Communicable {
    private StackPane root;

    @FXML
    private Label usuarioLabel;

    @FXML
    private JFXTextField usuarioField;

    @FXML
    private Label nivelLabel;

    @FXML
    private JFXComboBox<String> nivelBox;

    @FXML
    private JFXTextField rutField;

    @FXML
    private JFXTextField searchBar;

    @FXML
    private TableView<CuentaPersonal> table;

    @FXML
    private TableColumn<CuentaPersonal, String> usuarioColumn;

    @FXML
    private TableColumn<CuentaPersonal, Integer> rutColumn;

    @FXML
    private TableColumn<CuentaPersonal, String> nombreColumn;

    @FXML
    private TableColumn<CuentaPersonal, String> nivelColumn;

    @FXML
    private TableColumn<CuentaPersonal, CuentaPersonal> eliminar;

    private ObservableList<CuentaPersonal> data;

    private Personal personalCuenta;

    @FXML
    public void initialize() {
        Text calculator = new Text(usuarioLabel.getText());
        nivelLabel.prefWidthProperty().bind(usuarioLabel.prefWidthProperty());
        usuarioLabel.setPrefWidth(calculator.getLayoutBounds().getWidth() + 1);

        nivelBox.setItems(FXCollections.observableArrayList("ADMINISTRADOR",
                "PAÑOL", "SUPERVISOR", "CENTRO COSTOS", "ENCARGADO"));

        initTable();
    }

    @FXML
    private void handleBtnPersonal() {
        SelectorDialog<Personal> colaboradorDialog = initPersonalDialog();

        Personal temp = colaboradorDialog.showAndWait();

        if (temp != null) {
            personalCuenta = temp;
        }

        if (personalCuenta != null) {
            rutField.setText(personalCuenta.getNombre() + " " + personalCuenta.getNombre2() +
                    " " + personalCuenta.getApellidoP() + " " + personalCuenta.getApellidoM());
        }
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Personal> initPersonalDialog() {
        TableColumn<Personal, Integer> rut = new TableColumn<>("Rut");
        TableColumn<Personal, String> nombre = new TableColumn<>("Nombre");
        TableColumn<Personal, String> cargo = new TableColumn<>("Cargo");

        rut.setCellValueFactory(param -> param.getValue().rutProperty().asObject());
        nombre.setCellValueFactory(param ->
                new SimpleStringProperty(param.getValue().getNombre() + " " +
                        param.getValue().getNombre2() + " " +
                        param.getValue().getApellidoP() + " " +
                        param.getValue().getApellidoM()));

        cargo.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rut, nombre, cargo);

        SelectorDialog<Personal> personalDialog = new SelectorDialog<>(root);
        personalDialog.setTitle("Seleccionar Colaborador");
        personalDialog.setColumns(columns);
        personalDialog.setColumnRelationSize(6, 1, 3, 2);
        personalDialog.setData(new PersonalDaoImpl().getAllPersonal());
        personalDialog.setUpSearchBar("Rut/Nombre/Apellido/Cargo", (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase())
                        || item.getCargo().contains(val.toUpperCase())
        );

        return personalDialog;
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        usuarioColumn.setCellValueFactory(param ->
                param.getValue().getCuenta().usuarioProperty());

        rutColumn.setCellValueFactory(param ->
                param.getValue().getCuenta().rutProperty().asObject());

        nombreColumn.setCellValueFactory(param -> {
            Personal personal = param.getValue().getPersonal();

            if (personal != null) {
                return new SimpleStringProperty(personal.getNombre() + " " +
                        personal.getNombre2() + " " + personal.getApellidoP() + " " +
                        personal.getApellidoM());
            } else {
                return new SimpleStringProperty();
            }
        });

        nivelColumn.setCellValueFactory(param ->
                new SimpleStringProperty(nivelBox.getItems().get(
                        param.getValue().getCuenta().getNivel() - 1)));

        eliminar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        eliminar.setCellFactory(param -> new ContainerTableCell<>((item, container) -> {
            JFXButton button = new JFXButton("Eliminar");
            button.getStyleClass().add("delete-button");

            button.setOnAction(event -> {
                AlertDialog dialog = new AlertDialog(root, 400, 120);
                dialog.setTitle("Eliminar");
                dialog.setMessage("¿Esta seguro que desea eliminar el elemento seleccionado?");

                dialog.setPossitiveButton("Aceptar", event1 -> {
                    new CuentaDaoImpl().deleteCuenta(item.getCuenta().getRut());
                    data.remove(item);
                    dialog.close();
                    searchBar.setText("");
                });

                dialog.setNegativeButton("Cancelar");

                dialog.showAndWait();
            });

            container.getChildren().add(button);
        }));

        table.setRowFactory(param -> {
            TableRow<CuentaPersonal> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                if (row.getItem() != null) {
                    usuarioField.setText(row.getItem().getCuenta().getUsuario());

                    personalCuenta = row.getItem().getPersonal();

                    if (personalCuenta != null) {
                        rutField.setText(personalCuenta.getNombre() + " " + personalCuenta.getNombre2() +
                                " " + personalCuenta.getApellidoP() + " " + personalCuenta.getApellidoM());
                    }

                    nivelBox.getSelectionModel().select(row.getItem().getCuenta().getNivel() - 1);
                }
            });

            return row;
        });

        ObservableList<TableColumn<CuentaPersonal, ?>> columns =
                FXCollections.observableArrayList(usuarioColumn, rutColumn, nombreColumn,
                        nivelColumn, eliminar);

        data = new CuentaPersonalDaoImpl().getAllCuentas();

        TableConfigurator.setImmutable(table, columns);
        TableConfigurator.setRelationSize(table, columns, 6, 1, 1, 2, 1, 1);
        TableConfigurator.setUpSearchBar(searchBar, table, data, (val, item) ->
                item.getCuenta().getUsuario().contains(val)
                        || item.getPersonal().getNombre().contains(val.toUpperCase())
                        || item.getPersonal().getNombre2().contains(val.toUpperCase())
                        || item.getPersonal().getApellidoP().contains(val.toUpperCase())
                        || item.getPersonal().getApellidoM().contains(val.toUpperCase())
                        || Integer.toString(item.getCuenta().getRut()).contains(val.toUpperCase()
                ));
    }

    private void resetFields() {
        usuarioField.setText("");
        rutField.setText("");
        searchBar.setText("");
        nivelBox.getSelectionModel().selectFirst();
    }

    private boolean validateFields() {
        if (usuarioField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Usuario esta vacio");
            dialog.show();

            return false;
        }

        if (rutField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Personal esta vacio");
            dialog.show();

            return false;
        }

        return true;
    }

    @FXML
    private void handleBtnCrear() {
        if (validateFields()) {
            Cuenta create = new Cuenta.Builder()
                    .setUsuario(usuarioField.getText())
                    .setRut(personalCuenta.getRut())
                    .setNivel(nivelBox.getSelectionModel().getSelectedIndex() + 1)
                    .build();

            Personal user = new PersonalDaoImpl().getPersonal(create.getRut());

            if (user != null) {
                if (new CuentaDaoImpl().createCuenta(create)) {
                    data.add(new CuentaPersonal(create, user));
                    NotificationDialog dialog = new NotificationDialog(root);
                    dialog.setTitle("Informacion");
                    dialog.setContentText("Usuario Registrado Correctamente");
                    dialog.show();
                    resetFields();
                } else {
                    NotificationDialog dialog = new NotificationDialog(root);
                    dialog.setTitle("Error");
                    dialog.setContentText("No se ha podido registrar el usuario por uno de los " +
                            "siguientes motivos:\n\n" +
                            "-El nombre de usuario ya existe\n" +
                            "-El Rut ingresado ya posee una cuenta\n" +
                            "-El Rut ingresado no corresponde a un Personal valido");

                    dialog.show();
                }
            } else {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("No se ha podido registrar el usuario por uno de los " +
                        "siguientes motivos:\n\n" +
                        "-El nombre de usuario ya existe\n" +
                        "-El Rut ingresado ya posee una cuenta\n" +
                        "-El Rut ingresado no corresponde a un Personal valido");

                dialog.show();
            }
        }
    }

    @FXML
    private void handleBtnActualizar() {
        if (validateFields()) {
            Cuenta updated = new Cuenta.Builder()
                    .setUsuario(usuarioField.getText())
                    .setRut(personalCuenta.getRut())
                    .setNivel(nivelBox.getSelectionModel().getSelectedIndex() + 1)
                    .build();

            if (new CuentaDaoImpl().updateCuenta(updated)) {
                update();
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Informacion");
                dialog.setContentText("El usuario ha sido actualizado con exito");

                dialog.show();
                resetFields();
            } else {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("No se ha podido actualizar: la asociacion Usuario-Rut no " +
                        "existe");

                dialog.show();
            }
        }
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
    }

    @Override
    public void update() {
        resetFields();
        data.setAll(new CuentaPersonalDaoImpl().getAllCuentas());
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
