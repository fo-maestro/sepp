/*
 * Observacion.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.scene.control.Dialog;
import com.jfoenix.controls.JFXButton;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class Observacion implements Communicable {

    private VBox root;
    private Label headerText;
    private TextArea contentText;
    private String retVal;

    private Dialog<Void> context;

    public Observacion() {
        root = new VBox();
        root.setSpacing(10);
        root.setPadding(new Insets(10));
        headerText = new Label();
        contentText = new TextArea();
        VBox.setVgrow(contentText, Priority.ALWAYS);
        contentText.setWrapText(true);
        JFXButton anadir = new JFXButton("Añadir");

        anadir.setOnAction(event -> {
            retVal = contentText.getText();
            context.close();
        });

        anadir.getStyleClass().add("create-button");
        BorderPane pane = new BorderPane();
        pane.setCenter(anadir);
        root.getChildren().addAll(headerText, contentText, pane);
    }

    public void setHeaderText(String headerText) {
        this.headerText.setText(headerText);
    }


    public Node getView() {
        return root;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setData(ContentValues bundle) {
        context = (Dialog<Void>) bundle.get("context");
    }

    @Override
    public void update() {

    }

    @Override
    public ContentValues getData() {
        ContentValues bundle = new ContentValues();
        bundle.put("text", retVal);

        return bundle;
    }
}
