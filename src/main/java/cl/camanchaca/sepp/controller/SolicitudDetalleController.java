/*
 * SolicitudDetalleController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.ImageBanner;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.*;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.Font;
import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.*;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudInsumoDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudInsumoDetalleDaoImpl;
import cl.camanchaca.sepp.scene.control.*;
import cl.camanchaca.sepp.scene.control.Dialog;
import cl.camanchaca.sepp.scene.control.Spinner;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.HostServicesWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudDetalleController implements Communicable {
    @FXML
    private Label solicitudLabel;

    @FXML
    private Label rutSupervisorLabel;

    @FXML
    private Label rutColaboradorLabel;

    @FXML
    private Label rutAdministradorLabel;

    @FXML
    private Label fechaLabel;

    @FXML
    private Label nombreSupervisorLabel;

    @FXML
    private Label nombreColaboradorLabel;

    @FXML
    private Label nombreAdministradorLabel;

    @FXML
    private Label solicitud;

    @FXML
    private Label fecha;

    @FXML
    private Label rutSupervisor;

    @FXML
    private Label nombreSupervisor;

    @FXML
    private Label rutColaborador;

    @FXML
    private Label nombreColaborador;

    @FXML
    private Label rutAdministrador;

    @FXML
    private Label nombreAdministrador;

    @FXML
    private VBox container;

    @FXML
    private StackPane root;

    private Dialog<Void> context;
    private boolean response;

    private Spinner sp;

    @FXML
    private TableView<SolicitudInsumoDetalle> table;

    private SolicitudPersonal solicitudPersonal;
    private ObservableList<SolicitudInsumoDetalle> insumos;
    private SessionManager user;
    private ObservableList<SolicitudInsumo> cloned;

    @FXML
    public void initialize() {
        user = ServiceLocator.getService(SessionManager.class);
        sp = new Spinner(root);

        Text calculator = new Text(rutAdministradorLabel.getText());
        solicitudLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());
        rutSupervisorLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());
        rutColaboradorLabel.prefWidthProperty().bind(rutAdministradorLabel.prefWidthProperty());

        rutAdministradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        calculator = new Text(nombreAdministradorLabel.getText());
        fechaLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreSupervisorLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreColaboradorLabel.prefWidthProperty().bind(nombreAdministradorLabel.prefWidthProperty());
        nombreAdministradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        initTable();

        switch (user.getAccount().getNivel()) {
            case 1:
                initAdmin();
                break;

            case 2:
                initPanol();
                break;
        }
    }

    private void initPanol() {
        HBox box = new HBox();
        box.setAlignment(Pos.CENTER);
        JFXButton button = new JFXButton("Entregar");

        button.setOnAction(event -> {
            if (validateFields()) {
                AlertDialog dialog = new AlertDialog(root, 350, 120);
                dialog.setTitle("Entregar Insumos");
                dialog.setMessage("¿Esta seguro que desea procesar esta Solicitud?");
                dialog.setPossitiveButton("Aceptar", event1 -> {
                    dialog.close();

                    for (int i = 0; i < insumos.size(); i++) {
                        SolicitudInsumo retrive = insumos.get(i).getSolicitudInsumo();
                        SolicitudInsumo reflection = cloned.get(i);

                        if (retrive.getCantidadEntregada() > reflection.getCantidadEntregada()) {
                            retrive.setRutEntrega(user.getUser().getRut());
                            retrive.setFechaEntrega(
                                    DateFormatWrapper.toLatinForm(LocalDate.now().toString()));

                            new SolicitudInsumoDaoImpl().updateSolicitud(retrive);
                        }
                    }

                    Platform.runLater(() -> {
                        AlertDialog imp = new AlertDialog(root, 350, 120);
                        imp.setTitle("Imprimir");
                        imp.setMessage("¿Desea generar un comprobante de Solicitud?");
                        imp.setPossitiveButton("Imprimir", event2 -> {
                            ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
                            sp.enable();
                            executor.getExecutor().execute(imprimirComprobante());
                            imp.close();
                        });

                        imp.setNegativeButton("Cerrar");
                        imp.showAndWait();

                        response = true;
                        context.close();
                    });
                });

                dialog.setNegativeButton("Cancelar");
                dialog.showAndWait();
            }
        });

        button.getStyleClass().add("create-button");
        box.getChildren().add(button);

        container.getChildren().add(box);
    }

    private Task<Void> imprimirComprobante() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    File export = File.createTempFile("report", ".pdf");

                    Style headerStyle = new Style();
                    headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
                    headerStyle.setBorder(Border.PEN_2_POINT());
                    headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
                    headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
                    headerStyle.setBackgroundColor(Color.WHITE);
                    headerStyle.setTextColor(Color.BLACK);
                    headerStyle.setTransparency(Transparency.TRANSPARENT);

                    Style detail = new Style();
                    detail.setBorder(Border.PEN_1_POINT());

                    DynamicReportBuilder builder = new FastReportBuilder()
                            .addColumn("Codigo", "codigo", Integer.class.getName(), 30, detail)
                            .addColumn("Descripcion", "nombre", String.class.getName(), 50)
                            .addColumn("Solicitado", "solicitado", Integer.class.getName(), 30, detail)
                            .addColumn("Entregado", "entregado", Integer.class.getName(), 30, detail)
                            .addColumn("Fecha Entrega", "fecha", String.class.getName(), 50)
                            .addColumn("Observacion", "observacion", String.class.getName(), 70)
                            .addColumn("Firma", "firma", String.class.getName(), 50)
                            .setTitle("Comprobante de Solicitud")
                            .setSubtitle("Fecha Comprobante: " + DateFormatWrapper.toLatinForm(LocalDate.now().toString()))
                            .setDefaultStyles(null, null, headerStyle, detail)
                            .setPrintBackgroundOnOddRows(false)
                            .addImageBanner(getClass().getResource("/images/camanchaca.png").toExternalForm(),
                                    300, 200, ImageBanner.Alignment.Center)
                            .setUseFullPageWidth(true)
                            .setPageSizeAndOrientation(Page.Page_A4_Landscape());

                    builder.addAutoText(solicitudLabel.getText() + " " + solicitud.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_LEFT, 200)
                            .addAutoText(fechaLabel.getText() + " " + fecha.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200);

                    if (!rutSupervisor.getText().isEmpty()) {
                        builder.addAutoText(rutSupervisorLabel.getText() + " " + rutSupervisor.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_LEFT, 200)
                                .addAutoText("Nombre Supervisor: " + nombreSupervisor.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 400);
                    }

                    builder.addAutoText(rutColaboradorLabel.getText() + " " + rutColaborador.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_LEFT, 200)
                            .addAutoText("Nombre Colaborador: " + nombreColaborador.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 400);

                    if (!rutAdministrador.getText().isEmpty()) {
                        builder.addAutoText(rutAdministradorLabel.getText() + " " + rutAdministrador.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_LEFT, 200)
                                .addAutoText("Nombre Administrador: " + nombreAdministrador.getText(), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200);
                    }

                    DynamicReport dr = builder.build();

                    List<Boucher> bouchers = new ArrayList<>();

                    insumos.stream()
                            .filter(solicitudInsumoDetalle -> solicitudInsumoDetalle.getSolicitudInsumo().getCantidadEntregada() != 0)
                            .forEach(consumer ->
                                    bouchers.add(new Boucher.Builder()
                                            .setCodigo(consumer.getInsumo().getCodigo())
                                            .setNombre(consumer.getInsumo().getNombre())
                                            .setSolicitado(consumer.getSolicitudInsumo().getCantidadSolicitada())
                                            .setEntregado(consumer.getSolicitudInsumo().getCantidadEntregada())
                                            .setFecha(consumer.getSolicitudInsumo().getFechaEntrega())
                                            .setObservacion(consumer.getSolicitudInsumo().getObservacion())
                                            .build()));

                    JRDataSource src = new JRBeanCollectionDataSource(bouchers);
                    JasperPrint print = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), src);
                    JasperExportManager.exportReportToPdfFile(print, export.getPath());

                    HostServicesWrapper hostServices = ServiceLocator.getService(HostServicesWrapper.class);

                    hostServices.getHostServices().showDocument(export.getPath());
                } catch (Exception e) {
                    LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                }

                Platform.runLater(sp::disable);

                return null;
            }
        };
    }

    private boolean validateFields() {
        boolean equals = insumos.stream()
                .map(solicitudInsumoDetalle -> solicitudInsumoDetalle.getSolicitudInsumo().getCantidadEntregada())
                .collect(Collectors.toList())
                .equals(cloned.stream()
                        .map(SolicitudInsumo::getCantidadEntregada)
                        .collect(Collectors.toList()));

        if (equals) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se ha entregado ningun insumo");

            dialog.show();

            return false;
        }

        for (int i = 0; i < insumos.size(); i++) {
            SolicitudInsumoDetalle retrive = insumos.get(i);
            SolicitudInsumo reflection = cloned.get(i);

            if (retrive.getSolicitudInsumo().getCantidadEntregada()
                    < reflection.getCantidadEntregada()) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("La cantidad entregada anteriormente para el insumo " +
                        retrive.getInsumo().getNombre() + " es " +
                        reflection.getCantidadEntregada() +
                        " pero esta entregando actualmente " +
                        retrive.getSolicitudInsumo().getCantidadEntregada());

                dialog.show();

                return false;
            }

            if (retrive.getSolicitudInsumo().getCantidadEntregada()
                    > retrive.getSolicitudInsumo().getCantidadSolicitada()) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("La cantidad solicitada para el insumo " +
                        retrive.getInsumo().getNombre() + " es " +
                        retrive.getSolicitudInsumo().getCantidadSolicitada() +
                        " pero esta entregando actualmente " +
                        retrive.getSolicitudInsumo().getCantidadEntregada());

                dialog.show();

                return false;
            }

            if ((retrive.getSolicitudInsumo().getCantidadEntregada() - reflection.getCantidadEntregada())
                    > retrive.getInsumo().getCantidad()) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("La cantidad entregada para el insumo " +
                        retrive.getInsumo().getNombre() + " es " +
                        retrive.getSolicitudInsumo().getCantidadEntregada() +
                        " pero el stock actual del insumo es " +
                        retrive.getInsumo().getCantidad());

                dialog.show();

                return false;
            }
        }

        return true;
    }

    private void initAdmin() {
        HBox box = new HBox();
        box.setAlignment(Pos.CENTER);
        box.setSpacing(5);

        JFXButton aceptar = new JFXButton("Aceptar");

        aceptar.setOnAction(event -> {
            AlertDialog dialog = new AlertDialog(root, 350, 120);
            dialog.setTitle("Aceptar");
            dialog.setMessage("¿Esta seguro que desea Aceptar esta Solicitud?");
            dialog.setPossitiveButton("Aceptar", event1 -> dialog.close());

            dialog.setNegativeButton("Cancelar");
            dialog.showAndWait();
        });

        JFXButton rechazar = new JFXButton("Rechazar");

        aceptar.getStyleClass().add("create-button");

        aceptar.setOnAction(event -> {
            AlertDialog dialog = new AlertDialog(root, 400, 120);
            dialog.setTitle("Aprobar");
            dialog.setMessage("¿Esta seguro que desea aprobar esta solicitud?");

            dialog.setPossitiveButton("Aprobar", event1 -> {
                solicitudPersonal.getSolicitud().setEstado("PENDIENTE");
                solicitudPersonal.getSolicitud().setRutAprueba(user.getAccount().getRut());
                new SolicitudDaoImpl().updateEstadoSolicitud(solicitudPersonal.getSolicitud());

                dialog.close();
                response = true;
                context.close();
            });

            dialog.setNegativeButton("Cancelar");

            dialog.showAndWait();
        });

        rechazar.getStyleClass().add("delete-button");

        rechazar.setOnAction(event -> {
            Dialog<Void> dialog = new Dialog<>(root);
            Observacion obs = new Observacion();
            dialog.setContent(obs.getView(), 350, 350);
            dialog.setTitle("Observacion");
            obs.setHeaderText("Especifique la razon del rechazo:");

            ContentValues bundle = new ContentValues();
            bundle.put("context", dialog);
            obs.setData(bundle);

            dialog.showAndWait();
            String text = (String) obs.getData().get("text");

            if (text != null) {
                solicitudPersonal.getSolicitud().setEstado("RECHAZADO");
                solicitudPersonal.getSolicitud().setRutAprueba(user.getAccount().getRut());
                solicitudPersonal.getSolicitud().setAdminComentario(text);
                new SolicitudDaoImpl().updateEstadoSolicitud(solicitudPersonal.getSolicitud());

                response = true;
                context.close();
            }
        });

        box.getChildren().addAll(aceptar, rechazar);

        container.getChildren().add(box);
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        TableColumn<SolicitudInsumoDetalle, Integer> codigoColumn =
                new TableColumn<>("Codigo");

        codigoColumn.setCellValueFactory(param ->
                param.getValue().getInsumo().codigoProperty().asObject());

        TableColumn<SolicitudInsumoDetalle, String> nombreColumn =
                new TableColumn<>("Descripcion");

        TableColumn<SolicitudInsumoDetalle, Integer> stockColumn =
                new TableColumn<>("Stock");

        stockColumn.setCellValueFactory(param ->
                param.getValue().getInsumo().cantidadProperty().asObject());

        nombreColumn.setCellValueFactory(param ->
                param.getValue().getInsumo().nombreProperty());

        TableColumn<SolicitudInsumoDetalle, Integer> solicitadoColumn =
                new TableColumn<>("Solicitado");

        solicitadoColumn.setCellValueFactory(param ->
                param.getValue().getSolicitudInsumo().cantidadSolicitadaProperty().asObject());

        TableColumn<SolicitudInsumoDetalle, Integer> entregadoColumn =
                new TableColumn<>("Entregado");

        entregadoColumn.setCellValueFactory(param ->
                param.getValue().getSolicitudInsumo().cantidadEntregadaProperty().asObject());

        TableColumn<SolicitudInsumoDetalle, String> estadoColumn =
                new TableColumn<>("Estado");

        estadoColumn.setCellValueFactory(param -> {
            if (param.getValue().getSolicitudInsumo().getCantidadEntregada()
                    == param.getValue().getSolicitudInsumo().getCantidadSolicitada()) {
                return new SimpleStringProperty("ENTREGADO");
            } else if (param.getValue().getInsumo().getCantidad() == 0) {
                return new SimpleStringProperty("SIN STOCK");
            } else {
                return new SimpleStringProperty("DISPONIBLE");
            }
        });

        estadoColumn.setEditable(false);

        table.setRowFactory(param -> {
            TableRow<SolicitudInsumoDetalle> row = new TableRow<>();

            row.itemProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    if (newValue.getInsumo().getCantidad() == 0
                            || newValue.getSolicitudInsumo().getCantidadEntregada()
                            == newValue.getSolicitudInsumo().getCantidadSolicitada()) {
                        row.setDisable(true);
                    }
                }
            });

            return row;
        });

        ObservableList<TableColumn<SolicitudInsumoDetalle, ?>> columns;

        if (user.getAccount().getNivel() == 2) {
            table.setEditable(true);

            entregadoColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

            TableColumn<SolicitudInsumoDetalle, String> observacionColumn =
                    new TableColumn<>("Observacion(50 caracteres Max)");

            observacionColumn.setCellValueFactory(param ->
                    param.getValue().getSolicitudInsumo().observacionProperty());

            observacionColumn.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<String>() {
                @Override
                public String toString(String object) {
                    if (object == null) {
                        return "";
                    }

                    return object;
                }

                @Override
                public String fromString(String string) {
                    if (string == null) {
                        return "";
                    }

                    if (string.length() > 50) {
                        return string.substring(0, 50);
                    }

                    return string;
                }
            }));

            columns = FXCollections.observableArrayList(codigoColumn, nombreColumn, stockColumn,
                    solicitadoColumn, entregadoColumn, estadoColumn, observacionColumn);

            table.getColumns().addAll(columns);
            TableConfigurator.setRelationSize(table, columns, 13, 1, 3, 1, 1, 1, 1, 5);
        } else {
            TableColumn<SolicitudInsumoDetalle, String> supObs = new TableColumn<>("Observacion Supervisor");

            supObs.setCellValueFactory(param -> param.getValue().getSolicitudInsumo().supComentProperty());

            if (user.getAccount().getNivel() == 1) {
                columns = FXCollections.observableArrayList(codigoColumn, nombreColumn,
                        solicitadoColumn, estadoColumn, supObs);

                TableConfigurator.setRelationSize(table, columns, 11, 1, 3, 1, 1, 5);
            } else {
                columns = FXCollections.observableArrayList(codigoColumn, nombreColumn,
                        solicitadoColumn, entregadoColumn, estadoColumn, supObs);

                TableConfigurator.setRelationSize(table, columns, 12, 1, 3, 1, 1, 1, 5);
            }

            table.getColumns().addAll(columns);
        }

        columns.forEach(column -> column.setSortable(false));
        TableConfigurator.setImmutable(table, columns);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setData(ContentValues bundle) {
        solicitudPersonal = (SolicitudPersonal) bundle.get("solicitud");
        context = (Dialog<Void>) bundle.get("context");
        insumos = new SolicitudInsumoDetalleDaoImpl()
                .getSolicitudInsumo(solicitudPersonal.getSolicitud().getNumero());
    }

    @Override
    public void update() {
        Personal supervisor =
                new PersonalDaoImpl()
                        .getPersonal(solicitudPersonal.getSolicitud().getRutSupervisor());

        Personal solicitante = solicitudPersonal.getSolicitante();

        Personal administrador = new PersonalDaoImpl()
                .getPersonal(solicitudPersonal.getSolicitud().getRutAprueba());

        solicitud.setText(Integer.toString(solicitudPersonal.getSolicitud().getNumero()));
        fecha.setText(solicitudPersonal.getSolicitud().getFechaSolicitud());

        if (supervisor != null) {
            rutSupervisor.setText(supervisor.getRut() + "-" + supervisor.getDvd());
            nombreSupervisor.setText(supervisor.getNombre() + " " + supervisor.getNombre2() + " " +
                    supervisor.getApellidoP() + " " + supervisor.getApellidoM());
        }

        if (solicitante != null) {
            rutColaborador.setText(solicitante.getRut() + "-" + solicitante.getDvd());
            nombreColaborador.setText(solicitante.getNombre() + " " + solicitante.getNombre2() + " " +
                    solicitante.getApellidoP() + " " + solicitante.getApellidoM());
        }

        if (administrador != null) {
            rutAdministrador.setText(administrador.getRut() + "-" + administrador.getDvd());
            nombreAdministrador.setText(administrador.getNombre() + " " +
                    administrador.getNombre2() + " " + administrador.getApellidoP() + " " +
                    administrador.getApellidoM());
        }

        table.setItems(insumos);

        cloned = FXCollections.observableArrayList();

        insumos.forEach(consumer -> cloned.add(consumer.getSolicitudInsumo().clone()));
    }

    @Override
    public ContentValues getData() {
        ContentValues bundle = new ContentValues();
        bundle.put("response", response);

        return bundle;
    }
}
