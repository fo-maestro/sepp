/*
 * InsumoController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.InsumoCentroCostos;
import cl.camanchaca.sepp.model.InsumoCentroCostosDetalle;
import cl.camanchaca.sepp.model.access.CentroCostosDaoImpl;
import cl.camanchaca.sepp.model.access.InsumoCentroCostosDaoImpl;
import cl.camanchaca.sepp.model.access.InsumoCentroCostosDetalleDaoImpl;
import cl.camanchaca.sepp.model.access.InsumoDaoImpl;
import cl.camanchaca.sepp.scene.control.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumosCentroCostosController implements Communicable {
    @FXML
    private TableView<InsumoCentroCostosDetalle> table;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, Integer> codigoInsumo;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, String> nombreInsumo;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, Integer> codCcto;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, String> nombreCcto;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, Integer> duracion;

    @FXML
    private TableColumn<InsumoCentroCostosDetalle, InsumoCentroCostosDetalle> eliminar;

    @FXML
    private Label insumoLabel;

    @FXML
    private Label duracionLabel;

    @FXML
    private JFXTextField insumoField;

    @FXML
    private JFXTextField cctoField;

    @FXML
    private NumericTextField duracionField;

    @FXML
    private JFXTextField searchBar;

    private Insumo insumo;
    private CentroCostos ccto;
    private ObservableList<InsumoCentroCostosDetalle> data;

    private StackPane root;

    @FXML
    public void initialize() {
        Text calculator = new Text(duracionLabel.getText());
        insumoLabel.prefWidthProperty().bind(duracionLabel.prefWidthProperty());
        duracionLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        initTable();
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        codigoInsumo.setCellValueFactory(param ->
                param.getValue().getInsumoCcto().codigoInsumoProperty().asObject());

        nombreInsumo.setCellValueFactory(param -> param.getValue().getInsumo().nombreProperty());

        codCcto.setCellValueFactory(param ->
                param.getValue().getInsumoCcto().centroCostosProperty().asObject());

        nombreCcto.setCellValueFactory(param ->
                param.getValue().getCcto().nombreProperty());

        duracion.setCellValueFactory(param -> param.getValue().getInsumoCcto().duracionProperty().asObject());
        eliminar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        eliminar.setCellFactory(param -> new ContainerTableCell<>((item, container) -> {
            JFXButton button = new JFXButton("Eliminar");
            button.getStyleClass().add("delete-button");

            button.setOnAction(event -> {
                AlertDialog dialog = new AlertDialog(root, 400, 120);
                dialog.setTitle("Eliminar");
                dialog.setMessage("¿Esta seguro que desea eliminar el elemento seleccionado?");

                dialog.setPossitiveButton("Aceptar", event1 -> {
                    new InsumoCentroCostosDaoImpl().deleteInsumoCentroCostos(item.getInsumoCcto());
                    data.remove(item);
                    searchBar.setText("");
                    dialog.close();
                });

                dialog.setNegativeButton("Cancelar");

                dialog.showAndWait();
            });

            container.getChildren().add(button);
        }));

        table.setRowFactory(param -> {
            TableRow<InsumoCentroCostosDetalle> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                if (row.getItem() != null) {
                    insumo = new InsumoDaoImpl().getInsumo(row.getItem()
                            .getInsumoCcto().getCodigoInsumo());

                    if (insumo != null) {
                        insumoField.setText(insumo.getNombre());
                    }

                    ccto = new CentroCostosDaoImpl().getCentroCostos(row.getItem()
                            .getInsumoCcto().getCentroCostos());

                    if (ccto != null) {
                        cctoField.setText(ccto.getNombre());
                    }

                    duracionField.setText(Integer.toString(row.getItem()
                            .getInsumoCcto().getDuracion()));
                }
            });

            return row;
        });

        data = new InsumoCentroCostosDetalleDaoImpl().getAllInsumoCentroCostos();

        ObservableList<TableColumn<InsumoCentroCostosDetalle, ?>> columns =
                FXCollections.observableArrayList(codigoInsumo, nombreInsumo, codCcto, nombreCcto,
                        duracion, eliminar);

        TableConfigurator.setImmutable(table, columns);
        TableConfigurator.setRelationSize(table, columns, 8, 1, 2, 1, 2, 1, 1);
        TableConfigurator.setUpSearchBar(searchBar, table, data, (val, item) ->
                Integer.toString(item.getInsumoCcto().getCodigoInsumo()).contains(val.toUpperCase())
                        || item.getInsumo().getNombre().contains(val.toUpperCase())
                        || Integer.toString(item.getInsumoCcto().getCentroCostos()).contains(val.toUpperCase())
                        || item.getCcto().getNombre().contains(val.toUpperCase()));
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Insumo> initInsumoDialog() {
        TableColumn<Insumo, Integer> codigo = new TableColumn<>("Codigo");
        TableColumn<Insumo, String> nombre = new TableColumn<>("Descripcion");
        TableColumn<Insumo, Integer> cantidad = new TableColumn<>("Stock");

        codigo.setCellValueFactory(param -> param.getValue().codigoProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());
        cantidad.setCellValueFactory(param -> param.getValue().cantidadProperty().asObject());

        ObservableList<TableColumn<Insumo, ?>> columns = FXCollections.observableArrayList(codigo, nombre, cantidad);
        SelectorDialog<Insumo> insumoDialog = new SelectorDialog<>(root);
        insumoDialog.setTitle("Seleccionar Insumo");
        insumoDialog.setColumns(columns);
        insumoDialog.setColumnRelationSize(4, 1, 2, 1);
        insumoDialog.setData(new InsumoDaoImpl().getAllInsumos());
        insumoDialog.setUpSearchBar("Codigo/Descripcion", (val, item) ->
                Integer.toString(item.getCodigo()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase()));

        return insumoDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<CentroCostos> initCctoDialog() {
        TableColumn<CentroCostos, Integer> ccto = new TableColumn<>("CCTO");
        TableColumn<CentroCostos, Integer> area = new TableColumn<>("Area");
        TableColumn<CentroCostos, Integer> empresa = new TableColumn<>("Empresa");
        TableColumn<CentroCostos, Integer> planta = new TableColumn<>("Planta");
        TableColumn<CentroCostos, String> nombre = new TableColumn<>("Nombre");

        ccto.setCellValueFactory(param -> param.getValue().codCctoProperty().asObject());
        area.setCellValueFactory(param -> param.getValue().codAreaProperty().asObject());
        empresa.setCellValueFactory(param -> param.getValue().codEmpresaProperty().asObject());
        planta.setCellValueFactory(param -> param.getValue().codPlantaProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());

        ObservableList<TableColumn<CentroCostos, ?>> columns = FXCollections
                .observableArrayList(ccto, area, empresa, planta, nombre);

        SelectorDialog<CentroCostos> cctoDialog = new SelectorDialog<>(root);
        cctoDialog.setTitle("Seleccionar CCCTO");
        cctoDialog.setColumns(columns);
        cctoDialog.setColumnRelationSize(8, 1, 1, 1, 1, 4);
        cctoDialog.setData(new CentroCostosDaoImpl().getAllCentroCostos());
        cctoDialog.setUpSearchBar("Nombre", (val, item) ->
                item.getNombre().contains(val.toUpperCase()));

        return cctoDialog;
    }

    @FXML
    private void handleBtnInsumo() {
        SelectorDialog<Insumo> insumoDialog = initInsumoDialog();

        Insumo temp = insumoDialog.showAndWait();

        if (temp != null) {
            insumo = temp;
        }

        if (insumo != null) {
            insumoField.setText(insumo.getNombre());
        }
    }

    @FXML
    private void handleBtnCcto() {
        SelectorDialog<CentroCostos> cctoDialog = initCctoDialog();

        CentroCostos temp = cctoDialog.showAndWait();

        if (temp != null) {
            this.ccto = temp;
        }

        if (this.ccto != null) {
            cctoField.setText(this.ccto.getNombre());
        }
    }

    @FXML
    private void handleBtnCrear() {
        if (validateFields()) {
            InsumoCentroCostos create = new InsumoCentroCostos.Builder()
                    .setCodigoInsumo(insumo.getCodigo())
                    .setCentroCostos(CentroCostos.codigoCctoCompleto(ccto.getCodCcto(), ccto
                            .getCodArea(), ccto.getCodEmpresa(), ccto.getCodPlanta()))
                    .setDuracion(Integer.parseInt(duracionField.getText()))
                    .build();

            if (new InsumoCentroCostosDaoImpl().createInsumoCentroCostos(create)) {
                update();
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Informacion");
                dialog.setContentText("Operacion realizada satisfactoriamente");
                dialog.show();
            } else {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("La asociacion Insumo-Centro de costos ingresada ya existe");
                dialog.show();
            }
        }
    }

    @FXML
    private void handleBtnActualizar() {
        if (validateFields()) {
            InsumoCentroCostos updated = new InsumoCentroCostos.Builder()
                    .setCodigoInsumo(insumo.getCodigo())
                    .setCentroCostos(CentroCostos.codigoCctoCompleto(ccto.getCodCcto(), ccto
                            .getCodArea(), ccto.getCodEmpresa(), ccto.getCodPlanta()))
                    .setDuracion(Integer.parseInt(duracionField.getText()))
                    .build();

            if (new InsumoCentroCostosDaoImpl().updateInsumoCentroCostos(updated)) {
                update();
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Informacion");
                dialog.setContentText("Duracion Actualizada con exito");

                dialog.show();
                resetFields();
            } else {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("La asociacion Insumo-Centro de costos ingresada no existe");
                dialog.show();
            }
        }
    }

    private boolean validateFields() {
        if (insumoField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Insumo esta vacio");
            dialog.show();

            return false;
        }

        if (cctoField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Centro de Costos esta vacio");
            dialog.show();

            return false;
        }

        if (duracionField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Duracion esta vacio");
            dialog.show();

            return false;
        }

        int duracion = Integer.parseInt(duracionField.getText());

        if (duracion < 1 || duracion > 12) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("La duracion ingresada no corresponde a un valor de tipo mes");
            dialog.show();

            return false;
        }

        return true;
    }

    private void resetFields() {
        insumoField.setText("");
        cctoField.setText("");
        duracionField.setText("");
        searchBar.setText("");
        insumo = null;
        ccto = null;
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
    }

    @Override
    public void update() {
        resetFields();
        data.setAll(new InsumoCentroCostosDetalleDaoImpl().getAllInsumoCentroCostos());
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
