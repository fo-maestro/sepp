/*
 * LoginController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.login.ActiveDirectory;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.Cuenta;
import cl.camanchaca.sepp.model.access.CuentaDaoImpl;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.scene.JFXStage;
import cl.camanchaca.sepp.scene.control.NotificationDialog;
import cl.camanchaca.sepp.scene.control.Spinner;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class LoginController implements Communicable {

    @FXML
    private JFXTextField usuario;

    @FXML
    private JFXPasswordField contra;

    @FXML
    private StackPane container;

    private JFXStage context;
    private NotificationDialog alert;
    private Cuenta user;

    private Spinner sp;

    @FXML
    public void initialize() {
        sp = new Spinner(container);
        alert = new NotificationDialog(container);

        EventHandler<KeyEvent> handler = (event -> {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    if (!alert.isVisible()) {
                        handleButtonIngresar();
                    } else {
                        alert.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        usuario.setOnKeyPressed(handler);
        contra.setOnKeyPressed(handler);

        Platform.runLater(usuario::requestFocus);
    }

    @Override
    public void setData(ContentValues bundle) {
        context = (JFXStage) bundle.get("stage");
    }

    @Override
    public void update() {
    }

    @Override
    public ContentValues getData() {
        return null;
    }

    @FXML
    public void handleButtonIngresar() throws Exception {
        if (usuario.getText().isEmpty()) {
            alert.setTitle("Error");
            alert.setContentText("El campo de usuario esta vacio");
            alert.getActionButton().setOnAction(event -> {
                alert.close();
                usuario.requestFocus();
            });
            alert.show();

            return;
        }

        /*if (contra.getText().isEmpty()) {
            alert.setTitle("Error");
            alert.setContentText("El campo de contraseña esta vacio");
            alert.getActionButton().setOnAction(event -> {
                alert.close();
                contra.requestFocus();
            });
            alert.show();

            return;
        }*/

        user = new CuentaDaoImpl().getCuenta(usuario.getText());

        if (user == null) {
            alert.setTitle("Error");
            alert.setContentText("El usuario ingresado no es valido");
            alert.getActionButton().setOnAction(event -> {
                alert.close();
                usuario.requestFocus();
            });

            alert.show();

            return;
        }

        if (user.getNivel() == 0) {
            if (!user.getContra().equals(contra.getText())) {
                alert.setTitle("Error");
                alert.setContentText("La contraseña ingresada no es valida");
                alert.getActionButton().setOnAction(event -> {
                    alert.close();
                    contra.requestFocus();
                });
                alert.show();

                return;
            }
        } else {
            if (!ActiveDirectory.login("dc02.camanchaca.intranet:389", usuario.getText(), contra.getText())) {
                alert.setTitle("Error");
                alert.setContentText("La contraseña ingresada no es valida");
                alert.getActionButton().setOnAction(event -> {
                    alert.close();
                    contra.requestFocus();
                });
                alert.show();

                return;
            }
        }

        ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
        sp.enable();
        executor.getExecutor().execute(loadViews());

        ServiceLocator.registerService(new SessionManager(user, new PersonalDaoImpl()
                .getPersonal(user.getRut())));
    }

    private Task<Void> loadViews() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> {
                    usuario.setDisable(true);
                    contra.setDisable(true);

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/root.fxml"));
                    StackPane root = null;

                    try {
                        root = loader.load();
                    } catch (IOException e) {
                        LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                    }

                    Map<String, Pair<Node, Communicable>> views = null;

                    JFXStage stage = new JFXStage(root, false);
                    stage.setTitle(context.getTitle());
                    stage.disableContextMenu();

                    stage.setOnCloseRequest(() -> {
                        try {
                            ServiceLocator.getService(SQLServer.class).close();
                        } catch (SQLException e) {
                            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                        }

                        System.exit(0);
                    });

                    switch (user.getNivel()) {
                        case 0:
                            try {
                                views = setUpComponents(root, stage, "inicio", "usuarios");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }

                            break;

                        case 1:
                            try {
                                views = setUpComponents(root, stage, "inicio",
                                        "historial_de_solicitudes", "asignacion_masiva", "insumos",
                                        "insumos_centro_costos", "solicitudes");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }
                            break;

                        case 2:
                            try {
                                views = setUpComponents(root, stage, "inicio",
                                        "historial_de_solicitudes");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }
                            break;

                        case 3:
                            try {
                                views = setUpComponents(root, stage, "inicio",
                                        "crear_solicitud", "historial_de_solicitudes");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }
                            break;

                        case 4:
                            try {
                                views = setUpComponents(root, stage, "historial_de_solicitudes");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }
                            break;

                        case 5:
                            try {
                                views = setUpComponents(root, stage, "inicio",
                                        "crear_solicitud", "historial_de_solicitudes");
                            } catch (Exception e) {
                                LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
                            }
                            break;
                    }

                    RootController controller = loader.getController();
                    ContentValues bundle = new ContentValues();
                    bundle.put("views", views);
                    controller.setData(bundle);
                    controller.update();

                    sp.disable();

                    context.close();
                    stage.show();
                });

                return null;
            }
        };
    }

    private Map<String, Pair<Node, Communicable>> setUpComponents(StackPane root,
                                                                  JFXStage stage,
                                                                  String... componentsName)
            throws Exception {
        Map<String, Pair<Node, Communicable>> views = new HashMap<>();

        for (String component : componentsName) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/" + component +
                    ".fxml"));

            Node node = loader.load();
            HBox.setHgrow(node, Priority.ALWAYS);

            Communicable controller = loader.getController();
            ContentValues bundle = new ContentValues();
            bundle.put("stack_pane", root);
            bundle.put("stage", stage);
            controller.setData(bundle);
            views.put(component.replace("_", " "), new Pair<>(node, controller));
        }

        return views;
    }
}
