/*
 * RootController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.JFXListView;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.Pair;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class RootController implements Communicable {
    @FXML
    private HBox container;

    @FXML
    private JFXListView<?> list;

    @FXML
    private JFXListView<?> subListSolicitudes;

    @FXML
    private JFXListView<?> subListMantenedor;

    @FXML
    private Label name;

    @FXML
    private Label pcName;

    @FXML
    private Label date;

    private Node node;

    private ChangeListener<Object> listener;

    private Map<String, Pair<Node, Communicable>> preComputedMenus;

    private SessionManager session;

    @FXML
    public void initialize() {
        listener = (observable, oldValue, newValue) -> {
            date.setText(DateFormatWrapper.toLatinForm(LocalDate.now().toString()));

            if (newValue != null) {
                if (node != null) {
                    container.getChildren().remove(node);
                }

                Label label = (Label) newValue;
                loadView(preComputedMenus.get(label.getText().toLowerCase()));
            }
        };

        session = ServiceLocator.getService(SessionManager.class);

        if (session.getUser() != null) {
            name.setText(session.getUser().getNombre() + " " + session.getUser().getNombre2() +
                    "\n" + session.getUser().getApellidoP() + " " + session.getUser().getApellidoM());
        }

        try {
            InetAddress addr = InetAddress.getLocalHost();
            pcName.setText(addr.getHostName().toUpperCase());
        } catch (UnknownHostException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            pcName.setText("");
        }

        date.setText(DateFormatWrapper.toLatinForm(LocalDate.now().toString()));

        switch (session.getAccount().getNivel()) {
            case 0:
                loadSuperAdmin();
                break;

            case 1:
                loadAdmin();
                break;

            case 2:
                loadPanol();
                break;

            case 3:
                loadSupervisor();
                break;

            case 4:
                loadCcto();
                break;

            case 5:
                loadSupervisor();
                break;
        }
    }

    private void loadView(Pair<Node, Communicable> view) {
        container.getChildren().add(node = view.getKey());
        view.getValue().update();
    }

    private void loadSuperAdmin() {
        list.getItems().remove(subListSolicitudes);

        subListMantenedor.getItems().remove(0, subListMantenedor.getItems().size() - 1);

        list.getSelectionModel().selectedItemProperty().addListener(listener);
        subListMantenedor.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    private void loadPanol() {
        ObservableList<?> items = subListSolicitudes.getItems();

        items.remove(0);
        items.remove(items.size() - 1);

        list.getItems().remove(subListMantenedor);

        list.getSelectionModel().selectedItemProperty().addListener(listener);
        subListSolicitudes.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    private void loadSupervisor() {
        list.getItems().remove(subListMantenedor);

        subListSolicitudes.getItems().remove(subListSolicitudes.getItems().size() - 1);

        list.getSelectionModel().selectedItemProperty().addListener(listener);
        subListSolicitudes.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    private void loadAdmin() {
        ObservableList<?> items = subListSolicitudes.getItems();

        items.remove(0);

        subListMantenedor.getItems().remove(subListMantenedor.getItems().size() - 1);

        list.getSelectionModel().selectedItemProperty().addListener(listener);
        subListSolicitudes.getSelectionModel().selectedItemProperty().addListener(listener);
        subListMantenedor.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    private void loadCcto() {
        list.getItems().remove(0);

        subListSolicitudes.getItems().remove(0);
        subListSolicitudes.getItems().remove(subListSolicitudes.getItems().size() - 1);

        list.getItems().remove(subListMantenedor);

        subListSolicitudes.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setData(ContentValues bundle) {
        preComputedMenus = (Map<String, Pair<Node, Communicable>>) bundle.get("views");
    }

    @Override
    public void update() {
        if (session.getAccount().getNivel() == 4) {
            subListSolicitudes.getSelectionModel().selectFirst();
        } else {
            list.getSelectionModel().selectFirst();
        }

        Platform.runLater(list::requestFocus);
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
