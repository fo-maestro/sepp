/*
 * GenerarReporteController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.SolicitudDetalle;
import cl.camanchaca.sepp.model.access.*;
import cl.camanchaca.sepp.scene.control.Dialog;
import cl.camanchaca.sepp.scene.control.NotificationDialog;
import cl.camanchaca.sepp.scene.control.SelectorDialog;
import cl.camanchaca.sepp.scene.control.Spinner;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class GenerarReporteController implements Communicable {
    @FXML
    private JFXCheckBox solicitud;

    @FXML
    private JFXCheckBox fecha;

    @FXML
    private JFXCheckBox rutSupervisor;

    @FXML
    private JFXCheckBox nombreSupervisor;

    @FXML
    private JFXCheckBox rutColaborador;

    @FXML
    private JFXCheckBox nombreColaborador;

    @FXML
    private JFXCheckBox insumo;

    @FXML
    private JFXCheckBox descripcion;

    @FXML
    private JFXCheckBox total;

    @FXML
    private JFXCheckBox codigoCcto;

    @FXML
    private JFXCheckBox ccto;

    @FXML
    private JFXCheckBox estado;

    @FXML
    private JFXCheckBox selectAll;

    @FXML
    private JFXDatePicker from;

    @FXML
    private JFXDatePicker to;

    @FXML
    private JFXComboBox<String> filtroBox;

    @FXML
    private JFXTextField searchField;

    @FXML
    private JFXButton buscar;

    private Object filter;
    private StackPane root;
    private Dialog<Void> context;

    private List<JFXCheckBox> checks;

    private Spinner sp;

    private SessionManager user;

    @FXML
    public void initialize() {
        user = ServiceLocator.getService(SessionManager.class);

        checks = new ArrayList<>();
        checks.add(solicitud);
        checks.add(fecha);
        checks.add(rutSupervisor);
        checks.add(nombreSupervisor);
        checks.add(rutColaborador);
        checks.add(nombreColaborador);
        checks.add(insumo);
        checks.add(descripcion);
        checks.add(total);
        checks.add(codigoCcto);
        checks.add(ccto);
        checks.add(estado);

        selectAll.setOnAction(event -> {
            if (selectAll.isSelected()) {
                checks.forEach(checks -> checks.setSelected(true));
            } else {
                checks.forEach(checks -> checks.setSelected(false));
            }
        });

        if (user.getAccount().getNivel() == 3) {
            filtroBox.setItems(FXCollections.observableArrayList("SIN FILTRO", "COLABORADOR",
                    "INSUMO", "CENTRO DE COSTOS"));
        } else {
            filtroBox.setItems(FXCollections.observableArrayList("SIN FILTRO", "SUPERVISOR",
                    "COLABORADOR", "INSUMO", "CENTRO DE COSTOS"));
        }

        filtroBox.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    buscar.setDisable(newValue.equals("SIN FILTRO"));
                    searchField.setText("");

                    if (newValue.equals("SIN FILTRO") && user.getAccount().getNivel() == 3) {
                        filter = user.getUser();
                    } else {
                        filter = null;
                    }
                });

        filtroBox.getSelectionModel().selectFirst();
    }

    @FXML
    private void handleBtnBuscar() {
        if (filtroBox.getSelectionModel().getSelectedItem().equals("INSUMO")) {
            SelectorDialog<Insumo> dialog = initInsumoDialog();

            Insumo temp = dialog.showAndWait();

            if (temp != null) {
                filter = temp;
            }

            if (filter != null) {
                Insumo insumo = (Insumo) filter;
                searchField.setText(insumo.getNombre());
            }
        } else if (filtroBox.getSelectionModel().getSelectedItem().equals("CENTRO DE COSTOS")) {
            SelectorDialog<CentroCostos> dialog = initCctoDialog();

            CentroCostos temp = dialog.showAndWait();

            if (temp != null) {
                filter = temp;
            }

            if (filter != null) {
                CentroCostos ccto = (CentroCostos) filter;
                searchField.setText(ccto.getNombre());
            }
        } else {
            SelectorDialog<Personal> dialog = initPersonalDialog();

            Personal temp = dialog.showAndWait();

            if (temp != null) {
                filter = temp;
            }

            if (filter != null) {
                Personal personal = (Personal) filter;
                searchField.setText(personal.getNombre() + " " + personal.getNombre2() +
                        personal.getApellidoP() + " " + personal.getApellidoM());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<CentroCostos> initCctoDialog() {
        TableColumn<CentroCostos, Integer> ccto = new TableColumn<>("CCTO");
        TableColumn<CentroCostos, Integer> area = new TableColumn<>("Area");
        TableColumn<CentroCostos, Integer> empresa = new TableColumn<>("Empresa");
        TableColumn<CentroCostos, Integer> planta = new TableColumn<>("Planta");
        TableColumn<CentroCostos, String> nombre = new TableColumn<>("Nombre");

        ccto.setCellValueFactory(param -> param.getValue().codCctoProperty().asObject());
        area.setCellValueFactory(param -> param.getValue().codAreaProperty().asObject());
        empresa.setCellValueFactory(param -> param.getValue().codEmpresaProperty().asObject());
        planta.setCellValueFactory(param -> param.getValue().codPlantaProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());

        ObservableList<TableColumn<CentroCostos, ?>> columns = FXCollections
                .observableArrayList(ccto, area, empresa, planta, nombre);

        SelectorDialog<CentroCostos> cctoDialog = new SelectorDialog<>(root);
        cctoDialog.setTitle("Seleccionar CCCTO");
        cctoDialog.setColumns(columns);
        cctoDialog.setColumnRelationSize(8, 1, 1, 1, 1, 4);

        if (user.getAccount().getNivel() == 3) {
            cctoDialog.setData(new CentroCostosDaoImpl().getCentroCostosCompleto(user.getUser().getCodigoCcto()));
        } else {
            cctoDialog.setData(new CentroCostosDaoImpl().getAllCentroCostos());
        }

        cctoDialog.setUpSearchBar("Nombre", (val, item) ->
                item.getNombre().contains(val.toUpperCase()));

        return cctoDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Personal> initPersonalDialog() {
        TableColumn<Personal, Integer> rut = new TableColumn<>("Rut");
        TableColumn<Personal, String> nombre = new TableColumn<>("Nombre");
        TableColumn<Personal, String> cargo = new TableColumn<>("Cargo");

        rut.setCellValueFactory(param -> param.getValue().rutProperty().asObject());
        nombre.setCellValueFactory(param ->
                new SimpleStringProperty(param.getValue().getNombre() + " " +
                        param.getValue().getNombre2() + " " +
                        param.getValue().getApellidoP() + " " +
                        param.getValue().getApellidoM()));

        cargo.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rut, nombre, cargo);

        SelectorDialog<Personal> colaboradorDialog = new SelectorDialog<>(root);
        colaboradorDialog.setTitle("Seleccionar Personal");
        colaboradorDialog.setColumns(columns);
        colaboradorDialog.setColumnRelationSize(6, 1, 3, 2);

        if (filtroBox.getSelectionModel().getSelectedIndex() == 1) {
            if (user.getAccount().getNivel() == 3) {
                colaboradorDialog.setData(new PersonalDaoImpl()
                        .getPersonalAsociadoSupervisor(user.getUser()));
            } else {
                colaboradorDialog.setData(new PersonalDaoImpl().getAllSupervisores());
            }
        } else {
            colaboradorDialog.setData(new PersonalDaoImpl().getAllPersonal());
        }

        colaboradorDialog.setUpSearchBar("Rut/Nombre/Apellido/Cargo", (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase())
                        || item.getCargo().contains(val.toUpperCase())
        );

        return colaboradorDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Insumo> initInsumoDialog() {
        TableColumn<Insumo, Integer> codigo = new TableColumn<>("Codigo");
        TableColumn<Insumo, String> nombre = new TableColumn<>("Descripcion");
        TableColumn<Insumo, Integer> cantidad = new TableColumn<>("Stock");

        codigo.setCellValueFactory(param -> param.getValue().codigoProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());
        cantidad.setCellValueFactory(param -> param.getValue().cantidadProperty().asObject());

        ObservableList<TableColumn<Insumo, ?>> columns = FXCollections.observableArrayList(codigo, nombre, cantidad);

        SelectorDialog<Insumo> insumoDialog = new SelectorDialog<>(root);
        insumoDialog.setTitle("Seleccionar Insumo");
        insumoDialog.setColumns(columns);
        insumoDialog.setColumnRelationSize(4, 1, 2, 1);

        if (user.getAccount().getNivel() == 3) {
            insumoDialog.setData(new InsumoCentroCostosDaoImpl().getInsumos(user.getUser().getCodigoCcto()));
        } else {
            insumoDialog.setData(new InsumoDaoImpl().getAllInsumos());
        }

        insumoDialog.setUpSearchBar("Codigo/Descripcion", (val, item) ->
                Integer.toString(item.getCodigo()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase()));

        return insumoDialog;
    }

    @FXML
    private void handleBtnCrear() {
        if (validateFields()) {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Guardar Reporte");
            chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Microsoft Excel", "*.xlsx"));
            File file = chooser.showSaveDialog(context.getContext().getFxStage());

            if (file == null) {
                return;
            }

            ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
            sp.enable();
            executor.getExecutor().execute(createReport(file));
        }
    }

    private Task<Void> createReport(File file) {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                ObservableList<SolicitudDetalle> data;

                if (user.getAccount().getNivel() == 3) {
                    String option = filtroBox.getSelectionModel().getSelectedItem();

                    String result = option.equals("SIN FILTRO") ? "SUPERVISOR" : option;

                    if (result.equals("INSUMO")) {
                        int ninsumo = 0;

                        if (filter instanceof Insumo) {
                            ninsumo = ((Insumo) filter).getCodigo();
                        }

                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()), ninsumo, user.getAccount().getRut());
                    } else if (filtroBox.getSelectionModel().getSelectedItem().equals("CENTRO DE COSTOS")) {
                        int ccto = 0;

                        if (filter instanceof CentroCostos) {
                            CentroCostos centroCostos = (CentroCostos) filter;

                            ccto = CentroCostos.codigoCctoCompleto(centroCostos.getCodCcto(), centroCostos.getCodArea(),
                                    centroCostos.getCodEmpresa(), centroCostos.getCodPlanta());
                        }

                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalleCtto(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()), ccto);
                    } else {
                        if (result.equals("SUPERVISOR")) {
                            data = new SolicitudDetalleDaoImpl()
                                    .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                            DateFormatWrapper.toLatinForm(to.getValue().toString()), user.getAccount().getRut(), result);
                        } else {
                            int rut = 0;

                            if (filter instanceof Personal) {
                                rut = ((Personal) filter).getRut();
                            }

                            data = new SolicitudDetalleDaoImpl()
                                    .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                            DateFormatWrapper.toLatinForm(to.getValue().toString()), rut, result);
                        }
                    }
                } else {
                    if (filtroBox.getSelectionModel().getSelectedItem().equals("SIN FILTRO")) {
                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()));
                    } else if (filtroBox.getSelectionModel().getSelectedItem().equals("INSUMO")) {
                        int ninsumo = 0;

                        if (filter instanceof Insumo) {
                            ninsumo = ((Insumo) filter).getCodigo();
                        }

                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()), ninsumo);
                    } else if (filtroBox.getSelectionModel().getSelectedItem().equals("CENTRO DE COSTOS")) {
                        int ccto = 0;

                        if (filter instanceof CentroCostos) {
                            CentroCostos centroCostos = (CentroCostos) filter;

                            ccto = CentroCostos.codigoCctoCompleto(centroCostos.getCodCcto(), centroCostos.getCodArea(),
                                    centroCostos.getCodEmpresa(), centroCostos.getCodPlanta());
                        }

                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalleCtto(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()), ccto);
                    } else {
                        int rut = 0;

                        if (filter instanceof Personal) {
                            rut = ((Personal) filter).getRut();
                        }

                        data = new SolicitudDetalleDaoImpl()
                                .getSolicitudDetalle(DateFormatWrapper.toLatinForm(from.getValue().toString()),
                                        DateFormatWrapper.toLatinForm(to.getValue().toString()),
                                        rut, filtroBox.getSelectionModel().getSelectedItem());
                    }
                }

                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet spreadSheet = workbook.createSheet("Solicitudes");

                XSSFRow row = spreadSheet.createRow(0);

                CreationHelper helper = workbook.getCreationHelper();
                CellStyle cellStyle = workbook.createCellStyle();
                cellStyle.setDataFormat(helper.createDataFormat().getFormat("dd-mm-yyyy"));

                for (int i = 0, j = 0; i < checks.size(); i++) {
                    if (checks.get(i).isSelected()) {
                        row.createCell(j++).setCellValue(checks.get(i).getText());
                    }
                }

                for (int i = 0; i < data.size(); i++) {
                    String[] tokens = data.get(i).toString().split("@");
                    row = spreadSheet.createRow(i + 1);

                    for (int j = 0, k = 0; j < checks.size(); j++) {
                        if (checks.get(j).isSelected()) {
                            Cell cell = row.createCell(k++);

                            try {
                                cell.setCellValue(Double.parseDouble(tokens[j]));
                            } catch (NumberFormatException e) {
                                DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                                try {
                                    cell.setCellValue(format.parse(tokens[j]));
                                    cell.setCellStyle(cellStyle);
                                } catch (ParseException ex) {
                                    cell.setCellValue(tokens[j]);
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                    spreadSheet.autoSizeColumn(i);
                }

                workbook.write(new FileOutputStream(file));
                workbook.close();

                Platform.runLater(() -> {
                    NotificationDialog dialog = new NotificationDialog(root);
                    dialog.setTitle("Informacion");
                    dialog.setContentText("Reporte generado satisfactoriamente");
                    dialog.show();
                });

                Platform.runLater(sp::disable);

                return null;
            }
        };
    }

    private boolean validateFields() {
        if (from.getValue() == null) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se ha seleccionado una fecha para el campo \"Desde\"");
            dialog.show();

            return false;
        }

        if (to.getValue() == null) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se ha seleccionado una fecha para el campo \"Hasta\"");
            dialog.show();

            return false;
        }

        if (from.getValue().isAfter(to.getValue())) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El rango de fecha es incorrecto");
            dialog.show();

            return false;
        }

        if (!filtroBox.getSelectionModel().getSelectedItem().equals("SIN FILTRO")) {
            if (filter == null) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("No se ha especificado el valor del filtro");
                dialog.show();

                return false;
            }
        }

        boolean rev = false;

        for (JFXCheckBox consumer : checks) {
            if (consumer.isSelected()) {
                rev = true;
                break;
            }
        }

        if (!rev) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se ha seleccionado ningun campo para mostrar");
            dialog.show();

            return false;
        }

        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
        context = (Dialog<Void>) bundle.get("context");
        sp = new Spinner(root);
    }

    @Override
    public void update() {

    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
