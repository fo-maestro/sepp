/*
 * SolicitudesController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.Solicitud;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.model.access.*;
import cl.camanchaca.sepp.scene.control.*;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.converter.IntegerStringConverter;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudesController implements Communicable {

    @FXML
    private Label supervisorLabel;

    @FXML
    private JFXTextField supervisorField;

    @FXML
    private Label colaboradorLabel;

    @FXML
    private JFXTextField colaboradorField;

    @FXML
    private JFXDatePicker fecha;

    @FXML
    private JFXButton colaboradorBtn;

    @FXML
    private JFXButton insumoBtn;

    @FXML
    private TableView<SolicitudInsumo> table;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> codigoInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, String> nombreInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> cantidad;

    @FXML
    private TableColumn<SolicitudInsumo, SolicitudInsumo> eliminar;

    private Personal supervisor;
    private Personal colaborador;
    private StackPane root;

    private ObservableList<SolicitudInsumo> data;

    @FXML
    public void initialize() {
        Text calculator = new Text(colaboradorLabel.getText());
        supervisorLabel.prefWidthProperty().bind(colaboradorLabel.prefWidthProperty());
        colaboradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        initTable();
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        codigoInsumo.setCellValueFactory(param -> param.getValue().insumoProperty().asObject());
        nombreInsumo.setCellValueFactory(param -> {
            Insumo insumo = new InsumoDaoImpl().getInsumo(param.getValue().getInsumo());

            if (insumo != null) {
                return insumo.nombreProperty();
            } else {
                return new SimpleStringProperty();
            }
        });

        cantidad.setCellValueFactory(param ->
                param.getValue().cantidadSolicitadaProperty().asObject());

        cantidad.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

        eliminar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        eliminar.setCellFactory(param -> new ContainerTableCell<>((item, container) -> {
            JFXButton button = new JFXButton("Eliminar");
            button.getStyleClass().add("delete-button");

            button.setOnAction(event -> data.remove(item));

            container.getChildren().add(button);
        }));

        ObservableList<TableColumn<SolicitudInsumo, ?>> columns =
                FXCollections.observableArrayList(codigoInsumo, nombreInsumo, cantidad, eliminar);

        TableConfigurator.setImmutable(table, columns);
        TableConfigurator.setRelationSize(table, columns, 5, 1, 2, 1, 1);

        data = FXCollections.observableArrayList();
        table.setItems(data);
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Personal> initSupervisorDialog() {
        TableColumn<Personal, Integer> rut = new TableColumn<>("Rut");
        TableColumn<Personal, String> nombre = new TableColumn<>("Nombre");
        TableColumn<Personal, String> cargo = new TableColumn<>("Cargo");

        rut.setCellValueFactory(param -> param.getValue().rutProperty().asObject());

        nombre.setCellValueFactory(param ->
                new SimpleStringProperty(param.getValue().getNombre() + " " +
                        param.getValue().getNombre2() + " " +
                        param.getValue().getApellidoP() + " " +
                        param.getValue().getApellidoM()));

        cargo.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rut, nombre, cargo);

        SelectorDialog<Personal> supervisorDialog = new SelectorDialog<>(root);
        supervisorDialog.setTitle("Seleccionar Supervisor");
        supervisorDialog.setColumns(columns);
        supervisorDialog.setColumnRelationSize(6, 1, 3, 2);
        supervisorDialog.setData(new PersonalDaoImpl().getAllSupervisores());
        supervisorDialog.setUpSearchBar("Rut/Nombre/Apellido/Cargo", (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase())
                        || item.getCargo().contains(val.toUpperCase())
        );

        return supervisorDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Personal> initColaboradorDialog() {
        TableColumn<Personal, Integer> rut = new TableColumn<>("Rut");
        TableColumn<Personal, String> nombre = new TableColumn<>("Nombre");
        TableColumn<Personal, String> cargo = new TableColumn<>("Cargo");

        rut.setCellValueFactory(param -> param.getValue().rutProperty().asObject());
        nombre.setCellValueFactory(param ->
                new SimpleStringProperty(param.getValue().getNombre() + " " +
                        param.getValue().getNombre2() + " " +
                        param.getValue().getApellidoP() + " " +
                        param.getValue().getApellidoM()));

        cargo.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rut, nombre, cargo);

        SelectorDialog<Personal> colaboradorDialog = new SelectorDialog<>(root);
        colaboradorDialog.setTitle("Seleccionar Colaborador");
        colaboradorDialog.setColumns(columns);
        colaboradorDialog.setColumnRelationSize(6, 1, 3, 2);
        colaboradorDialog.setData(new PersonalDaoImpl().getPersonalAsociadoSupervisor(
                supervisor));
        colaboradorDialog.setUpSearchBar("Rut/Nombre/Apellido/Cargo", (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase())
                        || item.getCargo().contains(val.toUpperCase())
        );

        return colaboradorDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Insumo> initInsumoDialog() {
        TableColumn<Insumo, Integer> codigo = new TableColumn<>("Codigo");
        TableColumn<Insumo, String> nombre = new TableColumn<>("Descripcion");
        TableColumn<Insumo, Integer> cantidad = new TableColumn<>("Stock");

        codigo.setCellValueFactory(param -> param.getValue().codigoProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());
        cantidad.setCellValueFactory(param -> param.getValue().cantidadProperty().asObject());

        ObservableList<TableColumn<Insumo, ?>> columns = FXCollections.observableArrayList(codigo, nombre, cantidad);

        SelectorDialog<Insumo> insumoDialog = new SelectorDialog<>(root);
        insumoDialog.setTitle("Seleccionar Insumo");
        insumoDialog.setColumns(columns);
        insumoDialog.setColumnRelationSize(4, 1, 2, 1);
        insumoDialog.setData(new InsumoCentroCostosDaoImpl().getInsumos(supervisor.getCodigoCcto()));
        insumoDialog.setUpSearchBar("Codigo/Descripcion", (val, item) ->
                Integer.toString(item.getCodigo()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase()));

        return insumoDialog;
    }

    @FXML
    private void handleBtnSupervisor() {
        SelectorDialog<Personal> supervisorDialog = initSupervisorDialog();

        Personal temp = supervisorDialog.showAndWait();

        if (temp != null) {
            supervisor = temp;
        }

        if (supervisor != null) {
            supervisorField.setText(supervisor.getNombre() + " " + supervisor.getNombre2() +
                    " " + supervisor.getApellidoP() + " " + supervisor.getApellidoM());

            if (colaboradorBtn.isDisable()) {
                colaboradorBtn.setDisable(false);
            }

            if (insumoBtn.isDisable()) {
                insumoBtn.setDisable(false);
            }

            colaborador = null;
            colaboradorField.setText("");
            data.clear();
        }
    }

    @FXML
    private void handleBtnColaborador() {
        SelectorDialog<Personal> colaboradorDialog = initColaboradorDialog();

        Personal temp = colaboradorDialog.showAndWait();

        if (temp != null) {
            colaborador = temp;
        }

        if (colaborador != null) {
            colaboradorField.setText(colaborador.getNombre() + " " + colaborador.getNombre2() +
                    " " + colaborador.getApellidoP() + " " + colaborador.getApellidoM());
        }
    }

    @FXML
    private void handleBtnInsumo() {
        SelectorDialog<Insumo> insumoDialog = initInsumoDialog();

        Insumo temp = insumoDialog.showAndWait();

        if (temp != null) {

            for (SolicitudInsumo si : data) {
                if (si.getInsumo() == temp.getCodigo()) {
                    si.setCantidadSolicitada(si.getCantidadSolicitada() + 1);

                    return;
                }
            }

            data.add(new SolicitudInsumo.Builder()
                    .setInsumo(temp.getCodigo())
                    .setCantidadSolicitada(1)
                    .build());
        }
    }

    @FXML
    private void handleBtnEnviar() {
        if (validateFields()) {
            AlertDialog dialog = new AlertDialog(root, 350, 120);
            dialog.setTitle("Enviar");
            dialog.setMessage("¿Desea enviar esta Solicitud?");
            dialog.setPossitiveButton("Enviar", event -> {
                Solicitud solicitud = new Solicitud.Builder()
                        .setRutSolicitante(colaborador.getRut())
                        .setFechaSolicitud(DateFormatWrapper.toLatinForm(fecha.getValue().toString()))
                        .setRutSupervisor(supervisor.getRut())
                        .setEstado("ENTREGADO")
                        .build();

                int nsolicitud = new SolicitudDaoImpl().createSolicitud(solicitud);

                new SolicitudInsumoDaoImpl().disableTrigger();

                data.forEach(solicitudInsumo -> {
                    solicitudInsumo.setCantidadEntregada(solicitudInsumo.getCantidadSolicitada());
                    solicitudInsumo.setSolicitud(nsolicitud);
                    new SolicitudInsumoDaoImpl().createSolicitudAdmin(solicitudInsumo);
                });

                new SolicitudInsumoDaoImpl().enableTrigger();

                NotificationDialog notification = new NotificationDialog(root);
                notification.setTitle("Informacion");
                notification.setContentText("La solicitud ha sido registrada correctamente");

                notification.show();
                dialog.close();
                Platform.runLater(this::update);
            });

            dialog.setNegativeButton("Cancelar");
            dialog.showAndWait();
        }
    }

    private boolean validateFields() {
        if (supervisorField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Supervisor esta vacio");
            dialog.show();

            return false;
        }

        if (fecha.getValue() == null) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Fecha esta vacio");
            dialog.show();

            return false;
        }

        if (colaboradorField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Colaborador esta vacio");
            dialog.show();

            return false;
        }

        if (data.isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se han Ingresado insumos a la solicitud");
            dialog.show();

            return false;
        }

        for (SolicitudInsumo element : data) {
            if (element.getCantidadSolicitada() == 0) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("Uno de los insumos ingresados posee cantidad 0");
                dialog.show();

                return false;
            }
        }

        return true;
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
    }

    @Override
    public void update() {
        supervisorField.setText("");
        supervisor = null;
        fecha.setValue(null);
        colaboradorBtn.setDisable(true);
        colaboradorField.setText("");
        colaborador = null;
        insumoBtn.setDisable(true);
        data.clear();
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
