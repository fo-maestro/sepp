/*
 * SolicitudesController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.Solicitud;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.model.access.*;
import cl.camanchaca.sepp.scene.control.*;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.time.LocalDate;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class CrearSolicitudController implements Communicable {
    @FXML
    private Label supervisorLabel;

    @FXML
    private Label supervisor;

    @FXML
    private Label fecha;

    @FXML
    private Label colaboradorLabel;

    @FXML
    private JFXTextField colaboradorField;

    @FXML
    private TableView<SolicitudInsumo> table;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> codigoInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, String> nombreInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> cantidad;

    @FXML
    private TableColumn<SolicitudInsumo, String> observacion;

    @FXML
    private TableColumn<SolicitudInsumo, SolicitudInsumo> eliminar;

    private Personal user;
    private Personal colaborador;
    private StackPane root;
    private ObservableList<SolicitudInsumo> data;

    @FXML
    public void initialize() {
        Text calculator = new Text(colaboradorLabel.getText());
        supervisorLabel.prefWidthProperty().bind(colaboradorLabel.prefWidthProperty());
        colaboradorLabel.setPrefWidth(calculator.getLayoutBounds().getWidth());

        user = ServiceLocator.getService(SessionManager.class).getUser();
        supervisor.setText(user.getNombre() + " " + user.getNombre2() + " " + user.getApellidoP() +
                " " + user.getApellidoM());

        initTable();
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        codigoInsumo.setCellValueFactory(param -> param.getValue().insumoProperty().asObject());
        nombreInsumo.setCellValueFactory(param -> {
            Insumo insumo = new InsumoDaoImpl().getInsumo(param.getValue().getInsumo());

            if (insumo != null ) {
                return insumo.nombreProperty();
            } else {
                return new SimpleStringProperty();
            }
        });

        cantidad.setCellValueFactory(param ->
                param.getValue().cantidadSolicitadaProperty().asObject());

        cantidad.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

        observacion.setCellValueFactory(param -> param.getValue().supComentProperty());

        observacion.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<String>() {
            @Override
            public String toString(String object) {
                if (object == null) {
                    return "";
                }

                return object;
            }

            @Override
            public String fromString(String string) {
                if (string == null) {
                    return "";
                }

                if (string.length() > 50) {
                    return string.substring(0, 50);
                }

                return string;
            }
        }));

        eliminar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        eliminar.setCellFactory(param -> new ContainerTableCell<>((item, container) -> {
            JFXButton button = new JFXButton("Eliminar");
            button.getStyleClass().add("delete-button");

            button.setOnAction(event -> data.remove(item));

            container.getChildren().add(button);
        }));

        ObservableList<TableColumn<SolicitudInsumo, ?>> columns =
                FXCollections.observableArrayList(codigoInsumo, nombreInsumo, cantidad, observacion, eliminar);

        TableConfigurator.setImmutable(table, columns);
        TableConfigurator.setRelationSize(table, columns, 11, 1, 3, 1, 5, 1);

        data = FXCollections.observableArrayList();
        table.setItems(data);
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Personal> initColaboradorDialog() {
        TableColumn<Personal, Integer> rut = new TableColumn<>("Rut");
        TableColumn<Personal, String> nombre = new TableColumn<>("Nombre");
        TableColumn<Personal, String> cargo = new TableColumn<>("Cargo");

        rut.setCellValueFactory(param -> param.getValue().rutProperty().asObject());
        nombre.setCellValueFactory(param ->
                new SimpleStringProperty(param.getValue().getNombre() + " " +
                        param.getValue().getNombre2() + " " +
                        param.getValue().getApellidoP() + " " +
                        param.getValue().getApellidoM()));

        cargo.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rut, nombre, cargo);

        SelectorDialog<Personal> colaboradorDialog = new SelectorDialog<>(root);
        colaboradorDialog.setTitle("Seleccionar Colaborador");
        colaboradorDialog.setColumns(columns);
        colaboradorDialog.setColumnRelationSize(6, 1, 3, 2);

        if (ServiceLocator.getService(SessionManager.class).getAccount().getNivel() == 5) {
            colaboradorDialog.setData(new PersonalDaoImpl().getAllPersonal());
        } else {
            colaboradorDialog.setData(new PersonalDaoImpl().getPersonalAsociadoSupervisor(user));
        }

        colaboradorDialog.setUpSearchBar("Rut/Nombre/Apellido/Cargo", (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase())
                        || item.getCargo().contains(val.toUpperCase())
        );

        return colaboradorDialog;
    }

    @SuppressWarnings("unchecked")
    private SelectorDialog<Insumo> initInsumoDialog() {
        TableColumn<Insumo, Integer> codigo = new TableColumn<>("Codigo");
        TableColumn<Insumo, String> nombre = new TableColumn<>("Descripcion");
        TableColumn<Insumo, Integer> cantidad = new TableColumn<>("Stock");

        codigo.setCellValueFactory(param -> param.getValue().codigoProperty().asObject());
        nombre.setCellValueFactory(param -> param.getValue().nombreProperty());
        cantidad.setCellValueFactory(param -> param.getValue().cantidadProperty().asObject());

        ObservableList<TableColumn<Insumo, ?>> columns = FXCollections.observableArrayList(codigo, nombre, cantidad);

        SelectorDialog<Insumo> insumoDialog = new SelectorDialog<>(root);
        insumoDialog.setTitle("Seleccionar Insumo");
        insumoDialog.setColumns(columns);
        insumoDialog.setColumnRelationSize(4, 1, 2, 1);

        if (ServiceLocator.getService(SessionManager.class).getAccount().getNivel() == 5) {
            insumoDialog.setData(new InsumoDaoImpl().getAllInsumos());
        } else {
            insumoDialog.setData(new InsumoCentroCostosDaoImpl().getInsumos(user.getCodigoCcto()));
        }

        insumoDialog.setUpSearchBar("Codigo/Descripcion", (val, item) ->
                Integer.toString(item.getCodigo()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase()));

        return insumoDialog;
    }

    @FXML
    private void handleBtnColaborador() {
        SelectorDialog<Personal> colaboradorDialog = initColaboradorDialog();

        Personal temp = colaboradorDialog.showAndWait();

        if (temp != null) {
            colaborador = temp;
        }

        if (colaborador != null) {
            colaboradorField.setText(colaborador.getNombre() + " " + colaborador.getNombre2() +
                    " " + colaborador.getApellidoP() + " " + colaborador.getApellidoM());
        }
    }

    @FXML
    private void handleBtnInsumo() {
        SelectorDialog<Insumo> insumoDialog = initInsumoDialog();

        Insumo temp = insumoDialog.showAndWait();

        if (temp != null) {
            for (SolicitudInsumo si : data) {
                if (si.getInsumo() == temp.getCodigo()) {
                    si.setCantidadSolicitada(si.getCantidadSolicitada() + 1);
                    return;
                }
            }

            data.add(new SolicitudInsumo.Builder()
                    .setInsumo(temp.getCodigo())
                    .setCantidadSolicitada(1)
                    .build());
        }
    }

    @FXML
    private void handleBtnEnviar() {
        if (validateFields()) {
            AlertDialog dialog = new AlertDialog(root, 350, 120);
            dialog.setTitle("Enviar");
            dialog.setMessage("¿Desea enviar esta Solicitud?");
            dialog.setPossitiveButton("Enviar", event -> {
                Solicitud solicitud = new Solicitud.Builder()
                        .setRutSolicitante(colaborador.getRut())
                        .setFechaSolicitud(fecha.getText())
                        .setRutSupervisor(user.getRut())
                        .setEstado("PENDIENTE")
                        .build();

                int nsolicitud = new SolicitudDaoImpl().createSolicitud(solicitud);

                data.forEach(solicitudInsumo -> {
                    solicitudInsumo.setSolicitud(nsolicitud);
                    new SolicitudInsumoDaoImpl().createSolicitud(solicitudInsumo);
                });

                NotificationDialog notification = new NotificationDialog(root);
                notification.setTitle("Informacion");
                notification.setContentText("La solicitud ha sido enviada exitosamente, Numero de" +
                        " solicitud: " + nsolicitud);

                notification.show();
                dialog.close();
                Platform.runLater(this::update);
            });

            dialog.setNegativeButton("Cancelar");
            dialog.showAndWait();
        }
    }

    private boolean validateFields() {
        if (colaboradorField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("El campo Colaborador esta vacio");
            dialog.show();

            return false;
        }

        if (data.isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se han Ingresado insumos a la solicitud");
            dialog.show();

            return false;
        }

        for (SolicitudInsumo element : data) {
            if (element.getCantidadSolicitada() == 0) {
                NotificationDialog dialog = new NotificationDialog(root);
                dialog.setTitle("Error");
                dialog.setContentText("Uno de los insumos ingresados posee cantidad 0");
                dialog.show();

                return false;
            }
        }

        return true;
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
    }

    @Override
    public void update() {
        fecha.setText(DateFormatWrapper.toLatinForm(LocalDate.now().toString()));
        colaboradorField.setText("");
        colaborador = null;
        data.clear();
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
