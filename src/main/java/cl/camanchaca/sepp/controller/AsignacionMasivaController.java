/*
 * AsignacionMasivaController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.Solicitud;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.model.access.InsumoDaoImpl;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudInsumoDaoImpl;
import cl.camanchaca.sepp.scene.JFXStage;
import cl.camanchaca.sepp.scene.control.AlertDialog;
import cl.camanchaca.sepp.scene.control.NotificationDialog;
import cl.camanchaca.sepp.scene.control.Spinner;
import cl.camanchaca.sepp.scene.control.TableConfigurator;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class AsignacionMasivaController implements Communicable {
    private StackPane root;
    private JFXStage stage;

    @FXML
    private JFXTextField pathFieldPersonal;

    @FXML
    private JFXTextField pathFieldInsumos;

    @FXML
    private Label fecha;

    @FXML
    private JFXTextField searchBar;

    @FXML
    private TableView<Personal> personalTable;

    @FXML
    private TableColumn<Personal, Integer> rutPersonal;

    @FXML
    private TableColumn<Personal, String> nombrePersonal;

    @FXML
    private TableColumn<Personal, String> cargoPersonal;

    @FXML
    private TableView<SolicitudInsumo> insumoTable;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> codigoInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, String> nombreInsumo;

    @FXML
    private TableColumn<SolicitudInsumo, Integer> cantidadInsumo;

    private File filePersonal;
    private File fileInsumos;

    private ObservableList<Personal> personalData;
    private ObservableList<SolicitudInsumo> insumoData;

    private Spinner sp;

    @FXML
    public void initialize() {
        initPersonalTable();
        initInsumoTable();
    }

    @SuppressWarnings("unchecked")
    private void initPersonalTable() {
        rutPersonal.setCellValueFactory(param -> param.getValue().rutProperty().asObject());
        nombrePersonal.setCellValueFactory(param -> {
            Personal personal = param.getValue();

            if (personal != null) {
                return new SimpleStringProperty(personal.getNombre() + " " +
                        personal.getNombre2() + " " + personal.getApellidoP() + " " +
                        personal.getApellidoM());
            } else {
                return new SimpleStringProperty();
            }
        });

        cargoPersonal.setCellValueFactory(param -> param.getValue().cargoProperty());

        ObservableList<TableColumn<Personal, ?>> columns =
                FXCollections.observableArrayList(rutPersonal, nombrePersonal, cargoPersonal);

        personalData = FXCollections.observableArrayList();

        TableConfigurator.setImmutable(personalTable, columns);
        TableConfigurator.setRelationSize(personalTable, columns, 6, 1, 3, 2);
        TableConfigurator.setUpSearchBar(searchBar, personalTable, personalData, (val, item) ->
                Integer.toString(item.getRut()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase())
                        || item.getNombre2().contains(val.toUpperCase())
                        || item.getApellidoP().contains(val.toUpperCase())
                        || item.getApellidoM().contains(val.toUpperCase()));

    }

    @SuppressWarnings("unchecked")
    private void initInsumoTable() {
        codigoInsumo.setCellValueFactory(param -> param.getValue().insumoProperty().asObject());

        nombreInsumo.setCellValueFactory(param -> {
            Insumo insumo = new InsumoDaoImpl().getInsumo(param.getValue().getInsumo());

            if (insumo != null) {
                return insumo.nombreProperty();
            } else {
                return new SimpleStringProperty();
            }
        });

        cantidadInsumo.setCellValueFactory(param ->
                param.getValue().cantidadSolicitadaProperty().asObject());

        ObservableList<TableColumn<SolicitudInsumo, ?>> columns =
                FXCollections.observableArrayList(codigoInsumo, nombreInsumo, cantidadInsumo);

        TableConfigurator.setImmutable(insumoTable, columns);
        TableConfigurator.setRelationSize(insumoTable, columns, 4, 1, 2, 1);

        insumoData = FXCollections.observableArrayList();
        insumoTable.setItems(insumoData);
    }

    @FXML
    private void handleBtnBuscarPersonal() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Cargar Personal");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Microsoft Excel", "*.xlsx"));
        filePersonal = chooser.showOpenDialog(stage.getFxStage());

        if (filePersonal != null) {
            if (filePersonal.getName().split("\\.")[1].equalsIgnoreCase("xlsx")) {
                pathFieldPersonal.setText(filePersonal.getName());
                personalData.clear();
                ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
                sp.enable();
                executor.getExecutor().execute(cargarPersonal());
            } else {
                filePersonal = null;
            }
        }
    }

    private Task<Void> cargarPersonal() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                List<String> errorList = new ArrayList<>();

                try {
                    XSSFWorkbook workbook = new XSSFWorkbook(filePersonal);
                    XSSFSheet spreadSheet = workbook.getSheetAt(0);

                    for (int i = 1; i < spreadSheet.getPhysicalNumberOfRows(); i++) {
                        Row row = spreadSheet.getRow(i);

                        if (row.getPhysicalNumberOfCells() == 0) {
                            errorList.add(row.getRowNum() + ": La Fila " +  row.getRowNum() + " no posee un formato valido\n");
                            continue;
                        }

                        Iterator<Cell> cellIt = row.cellIterator();

                        String rut = "";

                        try {
                            Cell cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                rut = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                rut = cell.getStringCellValue();
                            }

                            Personal personal = new PersonalDaoImpl().getPersonal(Integer.parseInt(rut));

                            if (personal != null) {
                                personalData.add(personal);
                            } else {
                                errorList.add(row.getRowNum() + ": El Rut " + rut + " no esta registrado\n");
                            }
                        } catch (NumberFormatException e) {
                            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                            errorList.add(row.getRowNum() + ": " + rut + "\n");
                        }
                    }

                    workbook.close();
                } catch (IOException e) {
                    LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Error");
                        dialog.setContentText("El archivo seleccionado no existe");
                        dialog.show();

                        sp.disable();
                    });

                    return null;
                }

                Platform.runLater(sp::disable);

                if (errorList.size() != 0) {
                    StringBuilder error = new StringBuilder();

                    errorList.forEach(error::append);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root, true);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("No se ha podido cargar todo el personal:\n\n" + error.toString());
                        dialog.show();
                    });
                }

                return null;
            }
        };
    }

    @FXML
    private void handleBtnBuscarInsumo() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Cargar Insumos");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Microsoft Excel", "*.xlsx"));
        fileInsumos = chooser.showOpenDialog(stage.getFxStage());

        if (fileInsumos != null) {
            if (fileInsumos.getName().split("\\.")[1].equalsIgnoreCase("xlsx")) {
                pathFieldInsumos.setText(fileInsumos.getName());
                insumoData.clear();
                ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
                sp.enable();
                executor.getExecutor().execute(cargarInsumo());
            } else {
                fileInsumos = null;
            }
        }
    }

    private Task<Void> cargarInsumo() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                List<String> errorList = new ArrayList<>();

                try {
                    XSSFWorkbook workbook = new XSSFWorkbook(fileInsumos);
                    XSSFSheet spreadSheet = workbook.getSheetAt(0);

                    for (int i = 1; i < spreadSheet.getPhysicalNumberOfRows(); i++) {
                        Row row = spreadSheet.getRow(i);

                        if (row.getPhysicalNumberOfCells() < 3) {
                            errorList.add(row.getRowNum() + ": La Fila " +  row.getRowNum() + " no posee un formato valido\n");
                            continue;
                        }

                        String codigo = "";
                        String cantidad = "";
                        Cell cell;

                        try {
                            cell = row.getCell(0);

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                codigo = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                codigo = cell.getStringCellValue();
                            }

                            cell = row.getCell(2);

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                cantidad = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                cantidad = cell.getStringCellValue();
                            }

                            if (new InsumoDaoImpl().getInsumo(Integer.parseInt(codigo)) != null) {
                                insumoData.add(new SolicitudInsumo.Builder()
                                        .setInsumo(Integer.parseInt(codigo))
                                        .setCantidadSolicitada(Integer.parseInt(cantidad))
                                        .build());
                            } else {
                                errorList.add(row.getRowNum() + ": El Insumo " + codigo + " no esta registrado\n");
                            }
                        } catch (NumberFormatException e) {
                            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                            errorList.add(row.getRowNum() + ": " + codigo + " " + cantidad + "\n");
                        }
                    }

                    workbook.close();
                } catch (IOException e) {
                    LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Error");
                        dialog.setContentText("El archivo seleccionado no existe");
                        dialog.show();

                        sp.disable();
                    });

                    return null;
                }

                Platform.runLater(sp::disable);

                if (errorList.size() != 0) {
                    StringBuilder error = new StringBuilder();

                    errorList.forEach(error::append);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root, true);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("No se ha podido cargar todos los insumos:\n\n" + error.toString());
                        dialog.show();
                    });
                }

                return null;
            }
        };
    }

    private boolean validateFields() {
        if (pathFieldPersonal.getText().length() == 0) {
            NotificationDialog notification = new NotificationDialog(root);
            notification.setTitle("Error");
            notification.setContentText("No se ha seleccionado un archivo para la carga");
            notification.show();

            return false;
        }

        if (personalData.size() == 0) {
            NotificationDialog notification = new NotificationDialog(root);
            notification.setTitle("Error");
            notification.setContentText("No se han ingresado Personal para la asignacion");
            notification.show();

            return false;
        }

        if (insumoData.size() == 0) {
            NotificationDialog notification = new NotificationDialog(root);
            notification.setTitle("Error");
            notification.setContentText("No se han ingresado Insumos para la asignacion");
            notification.show();

            return false;
        }

        return true;
    }

    @FXML
    private void handleBtnEnviar() {
        if (validateFields()) {
            AlertDialog dialog = new AlertDialog(root, 350, 120);
            dialog.setTitle("Enviar");
            dialog.setMessage("¿Desea enviar esta Solicitud?");
            dialog.setPossitiveButton("Enviar", event -> {
                dialog.close();
                ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
                sp.enable();
                executor.getExecutor().execute(enviarSolicitudes());
            });

            dialog.setNegativeButton("Cancelar");
            dialog.showAndWait();
        }
    }

    private Task<Void> enviarSolicitudes() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                SessionManager user = ServiceLocator.getService(SessionManager.class);
                personalData.forEach(personal -> {
                    Solicitud solicitud = new Solicitud.Builder()
                            .setRutSolicitante(personal.getRut())
                            .setFechaSolicitud(fecha.getText())
                            .setRutAprueba(user.getAccount().getRut())
                            .setEstado("PENDIENTE")
                            .build();

                    int nsolicitud = new SolicitudDaoImpl().createMassSolicitud(solicitud);

                    insumoData.forEach(solicitudInsumo -> {
                        solicitudInsumo.setSolicitud(nsolicitud);
                        new SolicitudInsumoDaoImpl().createSolicitud(solicitudInsumo);
                    });
                });

                Platform.runLater(() -> {
                    NotificationDialog notification = new NotificationDialog(root);
                    notification.setTitle("Informacion");
                    notification.setContentText("Las solicitudes han sido registradas con exito");

                    sp.disable();
                    notification.show();
                });

                return null;
            }
        };
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
        stage = (JFXStage) bundle.get("stage");
        sp = new Spinner(root);
    }

    @Override
    public void update() {
        fecha.setText(DateFormatWrapper.toLatinForm(LocalDate.now().toString()));
        searchBar.setText("");
        pathFieldPersonal.setText("");
        pathFieldInsumos.setText("");
        filePersonal = null;
        fileInsumos = null;
        personalData.clear();
        insumoData.clear();
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
