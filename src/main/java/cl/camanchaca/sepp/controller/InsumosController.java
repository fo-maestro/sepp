/*
 * InsumosController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.access.InsumoCentroCostosDaoImpl;
import cl.camanchaca.sepp.model.access.InsumoDaoImpl;
import cl.camanchaca.sepp.scene.JFXStage;
import cl.camanchaca.sepp.scene.control.NotificationDialog;
import cl.camanchaca.sepp.scene.control.Spinner;
import cl.camanchaca.sepp.scene.control.TableConfigurator;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumosController implements Communicable {
    private StackPane root;
    private JFXStage stage;

    @FXML
    private JFXTextField pathField;

    @FXML
    private JFXTextField searchBar;

    @FXML
    private TableView<Insumo> table;

    @FXML
    private TableColumn<Insumo, Integer> codigoColumn;

    @FXML
    private TableColumn<Insumo, String> nombreColumn;

    @FXML
    private TableColumn<Insumo, Integer> cantidadColumn;

    private File file;
    private ObservableList<Insumo> data;
    private Spinner sp;

    @FXML
    public void initialize() {
        initTable();
    }

    @SuppressWarnings("unchecked")
    private void initTable() {
        codigoColumn.setCellValueFactory(param ->
                param.getValue().codigoProperty().asObject());

        nombreColumn.setCellValueFactory(param -> param.getValue().nombreProperty());

        cantidadColumn.setCellValueFactory(param ->
                param.getValue().cantidadProperty().asObject());

        ObservableList<TableColumn<Insumo, ?>> columns =
                FXCollections.observableArrayList(codigoColumn, nombreColumn, cantidadColumn);

        data = new InsumoDaoImpl().getAllInsumos();

        TableConfigurator.setImmutable(table, columns);
        TableConfigurator.setRelationSize(table, columns, 4, 1, 2, 1);
        TableConfigurator.setUpSearchBar(searchBar, table, data, (val, item) ->
                Integer.toString(item.getCodigo()).contains(val.toUpperCase())
                        || item.getNombre().contains(val.toUpperCase()));
    }

    @FXML
    private void handleBtnBuscar() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Cargar Insumos");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Microsoft Excel", "*.xlsx"));
        file = chooser.showOpenDialog(stage.getFxStage());

        if (file != null) {
            if (file.getName().split("\\.")[1].equalsIgnoreCase("xlsx")) {
                pathField.setText(file.getName());
            }
        }
    }

    private boolean validateFields() {
        if (pathField.getText().isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se ha seleccionado ningun archivo para cargar/actualizar");
            dialog.show();

            return false;
        }

        return true;
    }

    @FXML
    private void handleBtnCargar() throws IOException {
        if (validateFields()) {
            ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
            sp.enable();
            executor.getExecutor().execute(cargarInsumos());
        }
    }

    private Task<Void> cargarInsumos() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                List<String> errorList = new ArrayList<>();

                try {
                    XSSFWorkbook workbook = new XSSFWorkbook(file);
                    XSSFSheet spreadSheet = workbook.getSheetAt(0);

                    for (int i = 1; i < spreadSheet.getPhysicalNumberOfRows(); i++) {
                        Row row = spreadSheet.getRow(i);

                        if (row.getPhysicalNumberOfCells() < 3) {
                            errorList.add(row.getRowNum() + ": La Fila " + row.getRowNum() + " no posee un formato valido\n");
                            continue;
                        }

                        Iterator<Cell> cellIt = row.cellIterator();

                        String codigo = "", nombre = "", cantidad = "";
                        Cell cell;

                        try {
                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                codigo = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                codigo = cell.getStringCellValue();
                            }

                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.STRING) {
                                nombre = cell.getStringCellValue();
                            }

                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                cantidad = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                cantidad = cell.getStringCellValue();
                            }

                            Insumo created = new Insumo.Builder()
                                    .setCodigo(Integer.parseInt(codigo))
                                    .setNombre(nombre)
                                    .setCantidad(Integer.parseInt(cantidad))
                                    .build();

                            if (new InsumoDaoImpl().createInsumo(created)) {
                                data.add(created);
                            } else {
                                errorList.add(row.getRowNum() + ": El insumo " + created.getCodigo() + " ya esta registrado\n");
                            }
                        } catch (NumberFormatException e) {
                            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                            errorList.add(row.getRowNum() + ": " + codigo + " " + nombre + " " + cantidad + "\n");
                        }
                    }

                    workbook.close();
                } catch (IOException e) {
                    LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Error");
                        dialog.setContentText("El archivo seleccionado no existe");
                        dialog.show();

                        sp.disable();
                    });

                    return null;
                }

                Platform.runLater(sp::disable);

                if (errorList.size() == 0) {
                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("Insumos cargados completamente");
                        dialog.show();
                    });
                } else {
                    StringBuilder error = new StringBuilder();

                    errorList.forEach(error::append);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("No se han podido cargar todos los insumos:\n\n" + error.toString());
                        dialog.show();
                    });

                }

                Platform.runLater(() -> {
                    resetFields();
                    update();
                });

                return null;
            }
        };
    }

    @FXML
    private void handleBtnActualizar() throws IOException {
        if (validateFields()) {
            ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
            sp.enable();
            executor.getExecutor().execute(actualizarInsumos());
        }
    }

    @FXML
    private void handleBtnExportar() {
        if (data.isEmpty()) {
            NotificationDialog dialog = new NotificationDialog(root);
            dialog.setTitle("Error");
            dialog.setContentText("No se han cargado insumos al sistema");
            dialog.show();

            return;
        }

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Exportar insumos");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Microsoft Excel", "*.xlsx"));
        file = chooser.showSaveDialog(stage.getFxStage());

        if (file != null) {
            ExecutorWrapper executor = ServiceLocator.getService(ExecutorWrapper.class);
            sp.enable();
            executor.getExecutor().execute(exportInsumos());
        }
    }

    private Task<Void> exportInsumos() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                ObservableList<Insumo> insumos = new InsumoCentroCostosDaoImpl().getInsumos();

                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet spreadSheet = workbook.createSheet();

                XSSFRow row = spreadSheet.createRow(0);
                Cell cell = row.createCell(0);
                cell.setCellValue("Codigo");

                cell = row.createCell(1);
                cell.setCellValue("Descripcion");

                cell = row.createCell(2);
                cell.setCellValue("Stock");

                for (int i = 0; i < insumos.size(); i++) {
                    Insumo insumo = insumos.get(i);
                    row = spreadSheet.createRow(i + 1);

                    cell = row.createCell(0);
                    cell.setCellValue(insumo.getCodigo());

                    cell = row.createCell(1);
                    cell.setCellValue(insumo.getNombre());

                    cell = row.createCell(2);
                    cell.setCellValue(insumo.getCantidad());
                }

                for (int i = 0; i < 3; i++) {
                    spreadSheet.autoSizeColumn(i);
                }

                workbook.write(new FileOutputStream(file));
                workbook.close();

                Platform.runLater(() -> {
                    NotificationDialog dialog = new NotificationDialog(root);
                    dialog.setTitle("Informacion");
                    dialog.setContentText("El registro de insumos ha sido exportado correctamente");
                    dialog.show();
                });

                Platform.runLater(sp::disable);

                return null;
            }
        };
    }

    private Task<Void> actualizarInsumos() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                List<String> errorList = new ArrayList<>();

                try {
                    XSSFWorkbook workbook = new XSSFWorkbook(file);
                    XSSFSheet spreadSheet = workbook.getSheetAt(0);

                    for (int i = 1; i < spreadSheet.getPhysicalNumberOfRows(); i++) {
                        Row row = spreadSheet.getRow(i);

                        if (row.getPhysicalNumberOfCells() < 3) {
                            errorList.add(row.getRowNum() + ": La Fila " + row.getRowNum() + " no posee un formato valido\n");
                            continue;
                        }

                        Iterator<Cell> cellIt = row.cellIterator();

                        String codigo = "", nombre = "", cantidad = "";
                        Cell cell;

                        try {
                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                codigo = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                codigo = cell.getStringCellValue();
                            }

                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.STRING) {
                                nombre = cell.getStringCellValue();
                            }

                            cell = cellIt.next();

                            if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                                cantidad = "" + (int) cell.getNumericCellValue();
                            } else if (cell.getCellTypeEnum() == CellType.STRING) {
                                cantidad = cell.getStringCellValue();
                            }

                            Insumo updated = new Insumo.Builder()
                                    .setCodigo(Integer.parseInt(codigo))
                                    .setNombre(nombre)
                                    .setCantidad(Integer.parseInt(cantidad))
                                    .build();

                            if (!new InsumoDaoImpl().updateInsumo(updated)) {
                                errorList.add(row.getRowNum() + ": El insumo " + updated.getCodigo() + " no esta registrado\n");
                            }
                        } catch (NumberFormatException e) {
                            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                            errorList.add(row.getRowNum() + ": " + codigo + " " + nombre + " " + cantidad + "\n");
                        }
                    }

                    workbook.close();
                } catch (IOException e) {
                    LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Error");
                        dialog.setContentText("El archivo seleccionado no existe");
                        dialog.show();

                        sp.disable();
                    });

                    return null;
                }

                Platform.runLater(sp::disable);

                if (errorList.size() == 0) {
                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("Insumos actualizados completamente");
                        dialog.show();
                    });
                } else {
                    StringBuilder error = new StringBuilder();

                    errorList.forEach(error::append);

                    Platform.runLater(() -> {
                        NotificationDialog dialog = new NotificationDialog(root, true);
                        dialog.setTitle("Informacion");
                        dialog.setContentText("No se han podido actualizar todos los insumos:\n\n" + error.toString());
                        dialog.show();
                    });

                }

                Platform.runLater(() -> {
                    resetFields();
                    update();
                });

                return null;
            }
        };
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
        stage = (JFXStage) bundle.get("stage");
        sp = new Spinner(root);
    }

    private void resetFields() {
        pathField.setText("");
        searchBar.setText("");
        file = null;
    }

    @Override
    public void update() {
        resetFields();
        data.setAll(new InsumoDaoImpl().getAllInsumos());
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
