/*
 * InicioController.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.controller;

import cl.camanchaca.sepp.content.Communicable;
import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.login.SessionManager;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.model.SolicitudPersonal;
import cl.camanchaca.sepp.model.access.SolicitudDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudInsumoDaoImpl;
import cl.camanchaca.sepp.model.access.SolicitudPersonalDaoImpl;
import cl.camanchaca.sepp.scene.control.*;
import cl.camanchaca.sepp.service.ExecutorWrapper;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InicioController implements Communicable {
    private Pagination paginator;

    private ObservableList<TableColumn<SolicitudPersonal, ?>> columns;

    @FXML
    private JFXTextField searchBar;

    @FXML
    private VBox container;

    @FXML
    private JFXComboBox<Integer> pageSelector;

    private SessionManager user;
    private StackPane root;

    private final int pageSize = 50;
    private ExecutorService executor;

    private StackPane tableRoot;
    private TableView<SolicitudPersonal> currentTable;
    private Spinner currentSpinner;

    @FXML
    public void initialize() {
        user = ServiceLocator.getService(SessionManager.class);
        currentSpinner = new Spinner(tableRoot = new StackPane());
        executor = ServiceLocator.getService(ExecutorWrapper.class).getExecutor();
        initColumns();
    }

    private Node createPage(int pageIndex) {
        searchBar.setText("");

        if (currentSpinner.isEnable()) {
            currentSpinner.disable();
            tableRoot.getChildren().remove(currentTable);
        }

        currentTable = createTable(pageIndex);
        tableRoot.getChildren().add(currentTable);

        currentSpinner.enable();

        return tableRoot;
    }

    private TableView<SolicitudPersonal> createTable(int pageIndex) {
        TableView<SolicitudPersonal> table = new TableView<>();
        table.getColumns().addAll(columns);

        table.setRowFactory(param -> {
            TableRow<SolicitudPersonal> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                if (row.getItem() != null) {
                    if (row.getItem().getSolicitud().getEstado().equals("PENDIENTE")
                            || row.getItem().getSolicitud().getEstado().equals("ESPERA")) {
                        transitDialog(row.getItem());
                    } else {
                        processDialog(row.getItem());
                    }
                }
            });

            return row;
        });

        TableConfigurator.setImmutable(table, columns);

        if (user.getAccount().getNivel() == 0) {
            TableConfigurator.setRelationSize(table, columns, 6, 1, 2, 1, 1, 1);
        } else {
            TableConfigurator.setRelationSize(table, columns, 5, 1, 2, 1, 1);
        }

        final ObservableList<SolicitudPersonal> data = FXCollections.observableArrayList();

        switch (user.getAccount().getNivel()) {
            case 0:
                executor.execute(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        data.addAll(new SolicitudPersonalDaoImpl().getAllSolicitudes(pageIndex, pageSize));
                        Platform.runLater(currentSpinner::disable);
                        return null;
                    }
                });
                break;

            case 1:
                executor.execute(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        data.addAll(new SolicitudPersonalDaoImpl().getSolicitudesAdmin(pageIndex, pageSize));
                        Platform.runLater(currentSpinner::disable);
                        return null;
                    }
                });

                break;

            case 2:
                executor.execute(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        data.addAll(new SolicitudPersonalDaoImpl().getSolicitudesPanol(pageIndex, pageSize));
                        Platform.runLater(currentSpinner::disable);
                        return null;
                    }
                });

                break;

            case 3:
                executor.execute(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        data.addAll(new SolicitudPersonalDaoImpl().getSolicitudesSupervisor(user.getAccount().getRut(), pageIndex, pageSize));
                        Platform.runLater(currentSpinner::disable);
                        return null;
                    }
                });

                break;

            case 5:
                executor.execute(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        data.addAll(new SolicitudPersonalDaoImpl().getSolicitudesSupervisor(user.getAccount().getRut(), pageIndex, pageSize));
                        Platform.runLater(currentSpinner::disable);
                        return null;
                    }
                });

                break;
        }

        TableConfigurator.setUpSearchBar(searchBar, table, data, (val, item) ->
                Integer.toString(item.getSolicitud().getNumero()).contains(val.toUpperCase())
                        || item.getSolicitante().getNombre().contains(val.toUpperCase())
                        || item.getSolicitante().getNombre2().contains(val.toUpperCase())
                        || item.getSolicitante().getApellidoP().contains(val.toUpperCase())
                        || item.getSolicitante().getApellidoM().contains(val.toUpperCase())
                        || item.getSolicitud().getFechaSolicitud().contains(val.toUpperCase()));

        return table;
    }

    private void transitDialog(SolicitudPersonal element) {
        Dialog<Void> dialog = new Dialog<>(root);

        try {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("/view/solicitud_detalle.fxml"));

            dialog.setContent(loader.load(), true);

            dialog.setTitle("Solicitud Detalle");
            Communicable controller = loader.getController();
            ContentValues bundle = new ContentValues();
            bundle.put("solicitud", element);
            bundle.put("context", dialog);
            controller.setData(bundle);
            controller.update();
            dialog.showAndWait();

            if ((boolean) controller.getData().get("response")) {
                update();
            }

        } catch (IOException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
        }
    }

    private void processDialog(SolicitudPersonal element) {
        Dialog<Void> dialog = new Dialog<>(root);

        try {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("/view/historial_detalle.fxml"));

            dialog.setContent(loader.load(), true);
            dialog.setTitle("Solicitud Detalle");
            Communicable controller = loader.getController();
            ContentValues bundle = new ContentValues();
            bundle.put("solicitud", element);
            bundle.put("context", dialog);
            controller.setData(bundle);
            controller.update();
            dialog.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void initColumns() {
        TableColumn<SolicitudPersonal, Integer> solicitud =
                new TableColumn<>("Solicitud");

        solicitud.setCellValueFactory(param ->
                param.getValue().getSolicitud().numeroProperty().asObject());

        TableColumn<SolicitudPersonal, String> colaborador =
                new TableColumn<>("Colaborador");

        colaborador.setCellValueFactory(param -> {
            Personal solicitante = param.getValue().getSolicitante();

            if (solicitante != null) {
                return new SimpleStringProperty(solicitante.getNombre() + " " +
                        solicitante.getNombre2() + " " + solicitante.getApellidoP() + " " +
                        solicitante.getApellidoM());
            } else {
                return new SimpleStringProperty();
            }
        });

        TableColumn<SolicitudPersonal, String> fecha =
                new TableColumn<>("Fecha");

        fecha.setCellValueFactory(param ->
                param.getValue().getSolicitud().fechaSolicitudProperty());

        TableColumn<SolicitudPersonal, String> estado =
                new TableColumn<>("Estado");

        estado.setCellValueFactory(param -> {
            ObservableList<SolicitudInsumo> si =
                    new SolicitudInsumoDaoImpl().getSolicitudInsumo(
                            param.getValue().getSolicitud().getNumero());

            boolean partial = false;

            for (SolicitudInsumo element : si) {
                if (element.getCantidadEntregada() > 0
                        && param.getValue().getSolicitud().getEstado().equals("PENDIENTE")) {
                    partial = true;
                }
            }

            if (!partial) {
                return param.getValue().getSolicitud().estadoProperty();
            } else {
                return new SimpleStringProperty("PENDIENTE ENTREGA PARCIAL");
            }
        });

        if (user.getAccount().getNivel() == 0) {
            TableColumn<SolicitudPersonal, SolicitudPersonal> eliminar =
                    new TableColumn<>();

            eliminar.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

            eliminar.setCellFactory(param -> new ContainerTableCell<>((item, cellContainer) -> {
                JFXButton button = new JFXButton("Eliminar");
                button.getStyleClass().add("delete-button");

                button.setOnAction(event -> {
                    AlertDialog dialog = new AlertDialog(root, 400, 120);
                    dialog.setTitle("Eliminar");
                    dialog.setMessage("¿Esta seguro que desea eliminar el elemento seleccionado?");

                    dialog.setPossitiveButton("Aceptar", event1 -> {
                        new SolicitudDaoImpl().deleteSolicitud(item.getSolicitud().getNumero());
                        update();
                        dialog.close();
                    });

                    dialog.setNegativeButton("Cancelar");

                    dialog.showAndWait();
                });

                cellContainer.getChildren().add(button);
            }));

            columns = FXCollections.observableArrayList(solicitud, colaborador, fecha, estado, eliminar);
        } else {
            columns = FXCollections.observableArrayList(solicitud, colaborador, fecha, estado);
        }
    }

    @FXML
    private void handleBtnActualizar() {
        update();
    }

    @Override
    public void setData(ContentValues bundle) {
        root = (StackPane) bundle.get("stack_pane");
    }

    @Override
    public void update() {
        searchBar.setText("");

        if (container.getChildren().contains(paginator)) {
            container.getChildren().remove(paginator);
        }

        paginator = new Pagination();
        paginator.setMaxPageIndicatorCount(5);
        paginator.setPageFactory(this::createPage);
        VBox.setVgrow(paginator, Priority.ALWAYS);
        container.getChildren().add(paginator);

        int pageCount = 0;

        switch (user.getAccount().getNivel()) {
            case 0:
                pageCount = new SolicitudDaoImpl().getAllSolicitudesCount();
                break;

            case 1:
                pageCount = new SolicitudDaoImpl().getSolicitudesEsperaCount();
                break;

            case 2:
                pageCount = new SolicitudDaoImpl().getSolicitudesPendientesCount();
                break;

            case 3:
                pageCount = new SolicitudDaoImpl()
                        .getSolicitudesPendientesSupervisorCount(user.getAccount().getRut());
                break;
        }

        if (pageCount % pageSize != 0 || pageCount == 0) {
            paginator.setPageCount((pageCount / pageSize) + 1);
            pageSelector.setItems(FXCollections.observableArrayList(IntStream.rangeClosed(1, (pageCount / pageSize) + 1).boxed().collect(Collectors.toList())));
        } else {
            paginator.setPageCount(pageCount / pageSize);
            pageSelector.setItems(FXCollections.observableArrayList(IntStream.rangeClosed(1, pageCount / pageSize).boxed().collect(Collectors.toList())));
        }

        pageSelector.getSelectionModel().selectFirst();
        pageSelector.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
            paginator.setCurrentPageIndex(newValue.intValue())
        );

        paginator.currentPageIndexProperty().addListener((observable, oldValue, newValue) ->
                pageSelector.getSelectionModel().select(newValue.intValue()));
    }

    @Override
    public ContentValues getData() {
        return null;
    }
}
