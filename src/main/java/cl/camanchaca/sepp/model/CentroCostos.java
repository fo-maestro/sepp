/*
 * CentroCostos.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "vt_CentroCostos",
        columns = {"COD_CCTO", "COD_AREA", "COD_EMPRESA", "COD_PLANTA", "NOM_CCTO", "SELECCION"})
public final class CentroCostos {
    private IntegerProperty codCcto;
    private IntegerProperty codArea;
    private IntegerProperty codEmpresa;
    private IntegerProperty codPlanta;
    private StringProperty nombre;
    private IntegerProperty seleccion;

    private CentroCostos(int codCcto, int codArea, int codEmpresa, int codPlanta, String nombre,
                         int seleccion) {
        this.codCcto = new SimpleIntegerProperty(codCcto);
        this.codArea = new SimpleIntegerProperty(codArea);
        this.codEmpresa = new SimpleIntegerProperty(codEmpresa);
        this.codPlanta = new SimpleIntegerProperty(codPlanta);
        this.nombre = new SimpleStringProperty(nombre);
        this.seleccion = new SimpleIntegerProperty(seleccion);
    }

    public int getCodCcto() {
        return codCcto.get();
    }

    public IntegerProperty codCctoProperty() {
        return codCcto;
    }

    public void setCodCcto(int codCcto) {
        this.codCcto.set(codCcto);
    }

    public int getCodArea() {
        return codArea.get();
    }

    public IntegerProperty codAreaProperty() {
        return codArea;
    }

    public void setCodArea(int codArea) {
        this.codArea.set(codArea);
    }

    public int getCodEmpresa() {
        return codEmpresa.get();
    }

    public IntegerProperty codEmpresaProperty() {
        return codEmpresa;
    }

    public void setCodEmpresa(int codEmpresa) {
        this.codEmpresa.set(codEmpresa);
    }

    public int getCodPlanta() {
        return codPlanta.get();
    }

    public IntegerProperty codPlantaProperty() {
        return codPlanta;
    }

    public void setCodPlanta(int codPlanta) {
        this.codPlanta.set(codPlanta);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public int getSeleccion() {
        return seleccion.get();
    }

    public IntegerProperty seleccionProperty() {
        return seleccion;
    }

    public void setSeleccion(int seleccion) {
        this.seleccion.set(seleccion);
    }

    public static int codigoCctoCompleto(int ccto, int area, int empresa, int planta) {
        String empresaToken = "" + empresa;
        String plantaToken = planta < 10 ? "0" + planta : "" + planta;
        String areaToken = area < 10 ? "0" + area : "" + area;
        String cctoToken;

        if (ccto < 100) {
            if (ccto < 10) {
                cctoToken = "00" + ccto;
            } else {
                cctoToken = "0" + ccto;
            }
        } else {
            cctoToken = "" + ccto;
        }

        return Integer.parseInt(empresaToken + plantaToken + areaToken + cctoToken);
    }

    public static int[] reverseCodigoCcto(int codigoCctoCompleto) {
        String descomp = "" + codigoCctoCompleto;

        if (descomp.length() != 9) {
            return null;
        }

        int[] codigoCcto = new int[4];

        codigoCcto[2] = Integer.parseInt(descomp.substring(0, 2));
        codigoCcto[3] = Integer.parseInt(descomp.substring(2, 4));
        codigoCcto[1] = Integer.parseInt(descomp.substring(4, 6));
        codigoCcto[0] = Integer.parseInt(descomp.substring(6, 9));

        return codigoCcto;
    }

    public static int reverseCcto(int codCctoCompleto) {
        String str = Integer.toString(codCctoCompleto);
        int transformed = Integer.parseInt(str.substring(6, str.length()));

        if (Integer.toString(transformed).length() == 3) {
            return Integer.parseInt(str.substring(0, 6) + "0" + transformed / 10);
        } else if (Integer.toString(transformed).length() == 2) {
            return Integer.parseInt(str.substring(0, 6) + transformed * 10);
        }

        return 0;
    }

    public static final class Builder {
        private int codCcto;
        private int codArea;
        private int codEmpresa;
        private int codPlanta;
        private String nombre = "";
        private int seleccion;

        public Builder setCodCcto(int codCcto) {
            this.codCcto = codCcto;

            return this;
        }

        public Builder setCodArea(int codArea) {
            this.codArea = codArea;

            return this;
        }

        public Builder setCodEmpresa(int codEmpresa) {
            this.codEmpresa = codEmpresa;

            return this;
        }

        public Builder setCodPlanta(int codPlanta) {
            this.codPlanta = codPlanta;

            return this;
        }

        public Builder setNombre(String nombre) {
            this.nombre = nombre;

            return this;
        }

        public Builder setSeleccion(int seleccion) {
            this.seleccion = seleccion;

            return this;
        }

        public CentroCostos build() {
            return new CentroCostos(codCcto, codArea, codEmpresa, codPlanta, nombre, seleccion);
        }
    }

    public interface Dao {
        ObservableList<CentroCostos> getAllCentroCostos();
        ObservableList<CentroCostos> getCentroCostosCompleto(int codCctoCompleto);
        CentroCostos getCentroCostos(int codCctoCompleto);
    }
}
