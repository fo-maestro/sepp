/*
 * SolicitudPersonal.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudPersonal {
    private Solicitud solicitud;
    private Personal solicitante;

    public SolicitudPersonal(Solicitud solicitud, Personal solicitante) {
        this.solicitud = solicitud;
        this.solicitante = solicitante;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public Personal getSolicitante() {
        return solicitante;
    }

    public interface Dao {
        ObservableList<SolicitudPersonal> getSolicitudesSupervisor(int rutSupervisor, int index, int pageSize);
        ObservableList<SolicitudPersonal> getSolicitudesPanol(int index, int pageSize);
        ObservableList<SolicitudPersonal> getSolicitudesAdmin(int inex, int pageSize);
        ObservableList<SolicitudPersonal> getSolicitudesProcesadas(int index, int pageSize);
        ObservableList<SolicitudPersonal> getSolicitudesProcesadasSupervisor(int rutSupervisor,
                                                                             int index,
                                                                             int pageSize);
        ObservableList<SolicitudPersonal> getAllSolicitudes(int index, int pageSize);
    }
}
