/*
 * Personal.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "vt_Personal",
        columns = {"RRHRUT", "RRHDVD", "RRHAPE", "RRHAP1", "RRHNOM", "RRHNO1", "CARGO", "FECING",
                "FECTER", "SINDICATO", "COD_CCTO_ENTERO"})
public final class Personal {
    private IntegerProperty rut;
    private StringProperty dvd;
    private StringProperty apellidoP;
    private StringProperty apellidoM;
    private StringProperty nombre;
    private StringProperty nombre2;
    private StringProperty cargo;
    private StringProperty fechaIng;
    private StringProperty fechaTer;
    private IntegerProperty sindicato;
    private IntegerProperty codigoCcto;

    private Personal(int rut, String dvd, String apellidoP, String apellidoM, String nombre,
                    String nombre2, String cargo, String fechaIng, String fechaTer, int
                            sindicato, int codigoCcto) {
        this.rut = new SimpleIntegerProperty(rut);
        this.dvd = new SimpleStringProperty(dvd);
        this.apellidoP = new SimpleStringProperty(apellidoP);
        this.apellidoM = new SimpleStringProperty(apellidoM);
        this.nombre = new SimpleStringProperty(nombre);
        this.nombre2 = new SimpleStringProperty(nombre2);
        this.cargo = new SimpleStringProperty(cargo);
        this.fechaIng = new SimpleStringProperty(fechaIng);
        this.fechaTer = new SimpleStringProperty(fechaTer);
        this.sindicato = new SimpleIntegerProperty(sindicato);
        this.codigoCcto = new SimpleIntegerProperty(codigoCcto);
    }

    public int getRut() {
        return rut.get();
    }

    public IntegerProperty rutProperty() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut.set(rut);
    }

    public String getDvd() {
        return dvd.get();
    }

    public StringProperty dvdProperty() {
        return dvd;
    }

    public void setDvd(String dvd) {
        this.dvd.set(dvd);
    }

    public String getApellidoP() {
        return apellidoP.get();
    }

    public StringProperty apellidoPProperty() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP.set(apellidoP);
    }

    public String getApellidoM() {
        return apellidoM.get();
    }

    public StringProperty apellidoMProperty() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM.set(apellidoM);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getNombre2() {
        return nombre2.get();
    }

    public StringProperty nombre2Property() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2.set(nombre2);
    }

    public String getCargo() {
        return cargo.get();
    }

    public StringProperty cargoProperty() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo.set(cargo);
    }

    public String getFechaIng() {
        return fechaIng.get();
    }

    public StringProperty fechaIngProperty() {
        return fechaIng;
    }

    public void setFechaIng(String fechaIng) {
        this.fechaIng.set(fechaIng);
    }

    public String getFechaTer() {
        return fechaTer.get();
    }

    public StringProperty fechaTerProperty() {
        return fechaTer;
    }

    public void setFechaTer(String fechaTer) {
        this.fechaTer.set(fechaTer);
    }

    public int getSindicato() {
        return sindicato.get();
    }

    public IntegerProperty sindicatoProperty() {
        return sindicato;
    }

    public void setSindicato(int sindicato) {
        this.sindicato.set(sindicato);
    }

    public int getCodigoCcto() {
        return codigoCcto.get();
    }

    public IntegerProperty codigoCctoProperty() {
        return codigoCcto;
    }

    public void setCodigoCcto(int codigo_ccto) {
        this.codigoCcto.set(codigo_ccto);
    }

    public static final class Builder {
        private int rut;
        private String dvd;
        private String apellidoP;
        private String apellidoM;
        private String nombre;
        private String nombre2;
        private String cargo;
        private String fechaIng;
        private String fechaTer;
        private int sindicato;
        private int codigoCcto;

        public Builder() {
            dvd = "";
            apellidoP = "";
            apellidoM = "";
            nombre = "";
            nombre2 = "";
            cargo = "";
            fechaIng = "";
            fechaTer = "";
        }

        public Builder setRut(int rut) {
            this.rut = rut;

            return this;
        }

        public Builder setDvd(String dvd) {
            this.dvd = dvd;

            return this;
        }

        public Builder setApellidoP(String apellidoP) {
            this.apellidoP = apellidoP;

            return this;
        }

        public Builder setApellidoM(String apellidoM) {
            this.apellidoM = apellidoM;

            return this;
        }

        public Builder setNombre(String nombre) {
            this.nombre = nombre;

            return this;
        }

        public Builder setNombre2(String nombre2) {
            this.nombre2 = nombre2;

            return this;
        }

        public Builder setCargo(String cargo) {
            this.cargo = cargo;

            return this;
        }

        public Builder setFechaIng(String fechaIng) {
            this.fechaIng = fechaIng;

            return this;
        }

        public Builder setFechaTer(String fechaTer) {
            this.fechaTer = fechaTer;

            return this;
        }

        public Builder setSindicato(int sindicato) {
            this.sindicato = sindicato;

            return this;
        }

        public Builder setCodigoCcto(int codigoCcto) {
            this.codigoCcto = codigoCcto;

            return this;
        }

        public Personal build() {
            return new Personal(rut, dvd, apellidoP, apellidoM, nombre, nombre2, cargo, fechaIng,
                    fechaTer, sindicato, codigoCcto);
        }
    }

    public interface Dao {
        Personal getPersonal(int rut);
        ObservableList<Personal> getPersonalAsociadoSupervisor(Personal supervisor);
        ObservableList<Personal> getAllSupervisores();
        ObservableList<Personal> getAllPersonal();
    }
}
