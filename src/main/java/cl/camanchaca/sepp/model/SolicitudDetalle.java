/*
 * SolicitudDetalle.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudDetalle {
    private int solicitud;
    private String fecha;
    private String rutSupervisor;
    private String nombreSupervisor;
    private String rutColaborador;
    private String nombreColaborador;
    private int insumo;
    private String descripcion;
    private int total;
    private int codigoCcto;
    private String ccto;
    private String estado;

    private SolicitudDetalle(int solicitud, String fecha, String rutSupervisor, String nombreSupervisor,
                             String rutColaborador, String nombreColaborador, int insumo, String descripcion,
                             int total, int codigoCcto, String ccto, String estado) {
        this.solicitud = solicitud;
        this.fecha = fecha;
        this.rutSupervisor = rutSupervisor;
        this.nombreSupervisor = nombreSupervisor;
        this.rutColaborador = rutColaborador;
        this.nombreColaborador = nombreColaborador;
        this.insumo = insumo;
        this.descripcion = descripcion;
        this.total = total;
        this.codigoCcto = codigoCcto;
        this.ccto = ccto;
        this.estado = estado;
    }

    public int getSolicitud() {
        return solicitud;
    }

    public String getFecha() {
        return fecha;
    }

    public String getRutSupervisor() {
        return rutSupervisor;
    }

    public String getNombreSupervisor() {
        return nombreSupervisor;
    }

    public String getRutColaborador() {
        return rutColaborador;
    }

    public String getNombreColaborador() {
        return nombreColaborador;
    }

    public int getInsumo() {
        return insumo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getTotal() {
        return total;
    }

    public int getCodigoCcto() {
        return codigoCcto;
    }

    public String getCcto() {
        return ccto;
    }

    public String getEstado() {
        return estado;
    }

    @Override
    public String toString() {
        return solicitud + "@" + fecha + "@" + rutSupervisor + "@" + nombreSupervisor + "@" +
                rutColaborador + "@" + nombreColaborador + "@" + insumo + "@" + descripcion + "@" +
                total + "@" + codigoCcto + "@" + ccto + "@" + estado;
    }

    public static final class Builder {
        private int solicitud;
        private String fecha = "";
        private String rutSupervisor = "";
        private String nombreSupervisor = "";
        private String rutColaborador = "";
        private String nombreColaborador = "";
        private int insumo;
        private String descripcion = "";
        private int total;
        private int codigoCcto;
        private String ccto = "";
        private String estado = "";

        public Builder setSolicitud(int solicitud) {
            this.solicitud = solicitud;

            return this;
        }

        public Builder setFecha(String fecha) {
            this.fecha = fecha;

            return this;
        }

        public Builder setRutSupervisor(String rutSupervisor) {
            this.rutSupervisor = rutSupervisor;

            return this;
        }

        public Builder setNombreSupervisor(String nombreSupervisor) {
            this.nombreSupervisor = nombreSupervisor;

            return this;
        }

        public Builder setRutColaborador(String rutColaborador) {
            this.rutColaborador = rutColaborador;

            return this;
        }

        public Builder setNombreColaborador(String nombreColaborador) {
            this.nombreColaborador = nombreColaborador;

            return this;
        }

        public Builder setInsumo(int insumo) {
            this.insumo = insumo;

            return this;
        }

        public Builder setDescripcion(String descripcion) {
            this.descripcion = descripcion;

            return this;
        }

        public Builder setTotal(int total) {
            this.total = total;

            return this;
        }

        public Builder setCodigoCcto(int codigoCcto) {
            this.codigoCcto = codigoCcto;

            return this;
        }

        public Builder setCcto(String ccto) {
            this.ccto = ccto;

            return this;
        }

        public Builder setEstado(String estado) {
            this.estado = estado;

            return this;
        }

        public SolicitudDetalle build() {
            return new SolicitudDetalle(solicitud, fecha, rutSupervisor, nombreSupervisor,
                    rutColaborador, nombreColaborador, insumo, descripcion, total, codigoCcto,
                    ccto, estado);
        }
    }

    public interface Dao {
        ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to);
        ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int rut, String tipo);
        ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int insumo);
        ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int insumo, int rutSupervisor);
        ObservableList<SolicitudDetalle> getSolicitudDetalleCtto(String from, String to, int codCcto);
    }
}
