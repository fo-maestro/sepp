/*
 * CuentaPersonal.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class CuentaPersonal {
    private Cuenta cuenta;
    private Personal personal;
    private Personal asociado;

    public CuentaPersonal(Cuenta cuenta, Personal personal) {
        this.cuenta = cuenta;
        this.personal = personal;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Personal getPersonal() {
        return personal;
    }

    public Personal getAsociado() {
        return asociado;
    }

    public interface Dao {
        ObservableList<CuentaPersonal> getAllCuentas();
    }
}
