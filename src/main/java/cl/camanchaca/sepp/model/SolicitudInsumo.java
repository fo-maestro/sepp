/*
 * SolicitudInsumo.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import cl.camanchaca.sepp.database.Trigger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "Solicitud_Insumo",
        columns = {"NSOLICITUD", "NINSUMO", "CSOLICITADA", "CENTREGADA", "OBS",
                "FIRMA_RECIBIDO", "FECENTREGA", "RRHRUTENTREGA", "SUPCOMENT"})
@Trigger(names = {"UPDATE_SOLICITUD", "UPDATE_STATE", "UPDATE_STOCK"})
public final class SolicitudInsumo {
    private IntegerProperty solicitud;
    private IntegerProperty insumo;
    private IntegerProperty cantidadSolicitada;
    private IntegerProperty cantidadEntregada;
    private StringProperty observacion;
    private StringProperty firma;
    private StringProperty fechaEntrega;
    private IntegerProperty rutEntrega;
    private StringProperty supComent;

    private SolicitudInsumo(int solicitud, int insumo, int cantidadSolicitada,
                            int cantidadEntregada, String observacion, String firma,
                            String fechaEntrega, int rutEntrega, String supComent) {
        this.solicitud = new SimpleIntegerProperty(solicitud);
        this.insumo = new SimpleIntegerProperty(insumo);
        this.cantidadSolicitada = new SimpleIntegerProperty(cantidadSolicitada);
        this.cantidadEntregada = new SimpleIntegerProperty(cantidadEntregada);
        this.observacion = new SimpleStringProperty(observacion);
        this.firma = new SimpleStringProperty(firma);
        this.fechaEntrega = new SimpleStringProperty(fechaEntrega);
        this.rutEntrega = new SimpleIntegerProperty(rutEntrega);
        this.supComent = new SimpleStringProperty(supComent);
    }

    public String getSupComent() {
        return supComent.get();
    }

    public StringProperty supComentProperty() {
        return supComent;
    }

    public void setSupComent(String supComent) {
        this.supComent.set(supComent);
    }

    public String getFechaEntrega() {
        return fechaEntrega.get();
    }

    public StringProperty fechaEntregaProperty() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega.set(fechaEntrega);
    }

    public int getRutEntrega() {
        return rutEntrega.get();
    }

    public IntegerProperty rutEntregaProperty() {
        return rutEntrega;
    }

    public void setRutEntrega(int rutEntrega) {
        this.rutEntrega.set(rutEntrega);
    }

    public int getSolicitud() {
        return solicitud.get();
    }

    public IntegerProperty solicitudProperty() {
        return solicitud;
    }

    public void setSolicitud(int solicitud) {
        this.solicitud.set(solicitud);
    }

    public int getInsumo() {
        return insumo.get();
    }

    public IntegerProperty insumoProperty() {
        return insumo;
    }

    public void setInsumo(int insumo) {
        this.insumo.set(insumo);
    }

    public int getCantidadSolicitada() {
        return cantidadSolicitada.get();
    }

    public IntegerProperty cantidadSolicitadaProperty() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(int cantidadSolicitada) {
        this.cantidadSolicitada.set(cantidadSolicitada);
    }

    public int getCantidadEntregada() {
        return cantidadEntregada.get();
    }

    public IntegerProperty cantidadEntregadaProperty() {
        return cantidadEntregada;
    }

    public void setCantidadEntregada(int cantidadEntregada) {
        this.cantidadEntregada.set(cantidadEntregada);
    }

    public String getObservacion() {
        return observacion.get();
    }

    public StringProperty observacionProperty() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion.set(observacion);
    }

    public String getFirma() {
        return firma.get();
    }

    public StringProperty firmaProperty() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma.set(firma);
    }

    @Override
    public SolicitudInsumo clone() {
        return new SolicitudInsumo(solicitud.get(), insumo.get(), cantidadSolicitada.get(),
                cantidadEntregada.get(), observacion.get(), firma.get(), fechaEntrega.get(),
                rutEntrega.get(), supComent.get());
    }

    public static final class Builder {
        private int solicitud;
        private int insumo;
        private int cantidadSolicitada;
        private int cantidadEntregada;
        private String observacion = "";
        private String firma = "";
        private String fechaEntrega = "";
        private String supComent = "";

        public Builder setSupComent(String supComent) {
            this.supComent = supComent;

            return this;
        }

        public Builder setFechaEntrega(String fechaEntrega) {
            this.fechaEntrega = fechaEntrega;

            return this;
        }

        public Builder setRutEntrega(int rutEntrega) {
            this.rutEntrega = rutEntrega;

            return this;
        }

        private int rutEntrega;

        public Builder setInsumo(int insumo) {
            this.insumo = insumo;

            return this;
        }

        public Builder setSolicitud(int solicitud) {
            this.solicitud = solicitud;

            return this;
        }

        public Builder setCantidadSolicitada(int cantidadSolicitada) {
            this.cantidadSolicitada = cantidadSolicitada;

            return this;
        }

        public Builder setCantidadEntregada(int cantidadEntregada) {
            this.cantidadEntregada = cantidadEntregada;

            return this;
        }

        public Builder setObservacion(String observacion) {
            this.observacion = observacion;

            return this;
        }

        public Builder setFirma(String firma) {
            this.firma = firma;

            return this;
        }

        public SolicitudInsumo build() {
            return new SolicitudInsumo(solicitud, insumo, cantidadSolicitada, cantidadEntregada,
                    observacion, firma, fechaEntrega, rutEntrega, supComent);
        }
    }

    public interface Dao {
        boolean createSolicitud(SolicitudInsumo created);
        ObservableList<SolicitudInsumo> getSolicitudInsumo(int solicitud);
        ObservableList<SolicitudInsumo> getSolicitudInsumo(String from, String to, int insumo);
        ObservableList<SolicitudInsumo> getSolicitudInsumo(String from, String to, int insumo, int rut);
        boolean updateSolicitud(SolicitudInsumo updated);
        void enableTrigger();
        void disableTrigger();
        boolean createSolicitudAdmin(SolicitudInsumo created);
    }
}
