/*
 * Insumo.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "Insumo", columns = {"NINSUMO", "NOMBRE", "CANTIDAD"})
public final class Insumo {
    private IntegerProperty codigo;
    private StringProperty nombre;
    private IntegerProperty cantidad;

    private Insumo(int codigo, String nombre, int cantidad) {
        this.codigo = new SimpleIntegerProperty(codigo);
        this.nombre = new SimpleStringProperty(nombre);
        this.cantidad = new SimpleIntegerProperty(cantidad);
    }

    public int getCodigo() {
        return codigo.get();
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo.set(codigo);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public int getCantidad() {
        return cantidad.get();
    }

    public IntegerProperty cantidadProperty() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad.set(cantidad);
    }

    @Override
    public String toString() {
        return codigo.get() + "@" + nombre.get() + "@" + cantidad.get();
    }

    public static final class Builder {
        private int codigo;
        private String nombre = "";
        private int cantidad;
        private int duracion;
        private String fechaReposicion = "";


        public Builder setCodigo(int codigo) {
            this.codigo = codigo;

            return this;
        }

        public Builder setNombre(String nombre) {
            this.nombre = nombre;

            return this;
        }

        public Builder setCantidad(int cantidad) {
            this.cantidad = cantidad;

            return this;
        }

        public Insumo build() {
            return new Insumo(codigo, nombre, cantidad);
        }
    }

    public interface Dao {
        ObservableList<Insumo> getAllInsumos();
        Insumo getInsumo(int codigo);
        boolean createInsumo(Insumo created);
        boolean updateInsumo(Insumo updated);
    }
}
