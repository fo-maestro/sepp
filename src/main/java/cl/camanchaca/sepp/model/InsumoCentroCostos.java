/*
 * InsumoCentroCostos.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "Insumo_Centro_Costos", columns = {"NINSUMO", "COD_CCTO_ENTERO", "DURACION"})
public final class InsumoCentroCostos {

    private IntegerProperty codigoInsumo;
    private IntegerProperty centroCostos;
    private IntegerProperty duracion;

    private InsumoCentroCostos(int codigoInsumo, int centroCostos, int duracion) {
        this.codigoInsumo = new SimpleIntegerProperty(codigoInsumo);
        this.centroCostos = new SimpleIntegerProperty(centroCostos);
        this.duracion = new SimpleIntegerProperty(duracion);
    }

    public int getCodigoInsumo() {
        return codigoInsumo.get();
    }

    public IntegerProperty codigoInsumoProperty() {
        return codigoInsumo;
    }

    public void setCodigoInsumo(int codigoInsumo) {
        this.codigoInsumo.set(codigoInsumo);
    }

    public int getCentroCostos() {
        return centroCostos.get();
    }

    public IntegerProperty centroCostosProperty() {
        return centroCostos;
    }

    public void setCentroCostos(int centroCostos) {
        this.centroCostos.set(centroCostos);
    }

    public int getDuracion() {
        return duracion.get();
    }

    public IntegerProperty duracionProperty() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion.set(duracion);
    }


    public static final class Builder {
        private int codigoInsumo;
        private int centroCostos;
        private int duracion;

        public Builder setCodigoInsumo(int codigoInsumo) {
            this.codigoInsumo = codigoInsumo;

            return this;
        }

        public Builder setCentroCostos(int centroCostos) {
            this.centroCostos = centroCostos;

            return this;
        }

        public Builder setDuracion(int duracion) {
            this.duracion = duracion;

            return this;
        }

        public InsumoCentroCostos build() {
            return new InsumoCentroCostos(codigoInsumo, centroCostos, duracion);
        }
    }

    public interface Dao {
        ObservableList<InsumoCentroCostos> getAllInsumoCentroCostos();
        ObservableList<Insumo> getInsumos(int codCctoCompleto);
        ObservableList<Insumo> getInsumos();
        boolean createInsumoCentroCostos(InsumoCentroCostos created);
        boolean updateInsumoCentroCostos(InsumoCentroCostos updated);
        boolean deleteInsumoCentroCostos(InsumoCentroCostos deleted);
    }
}
