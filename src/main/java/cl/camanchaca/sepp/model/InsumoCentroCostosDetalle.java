/*
 * InsumoCentroCostoDetalle.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumoCentroCostosDetalle {
    private Insumo insumo;
    private CentroCostos ccto;
    private InsumoCentroCostos insumoCcto;

    public InsumoCentroCostosDetalle(Insumo insumo, CentroCostos ccto, InsumoCentroCostos insumoCcto) {
        this.insumo = insumo;
        this.ccto = ccto;
        this.insumoCcto = insumoCcto;
    }

    public Insumo getInsumo() {
        return insumo;
    }

    public CentroCostos getCcto() {
        return ccto;
    }

    public InsumoCentroCostos getInsumoCcto() {
        return insumoCcto;
    }

    public interface Dao {
        ObservableList<InsumoCentroCostosDetalle> getAllInsumoCentroCostos();
    }
}
