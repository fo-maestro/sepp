/*
 * Solicitud.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "Solicitud",
        columns = {"NSOLICITUD", "RRHRUTSOL", "FECSOLICITUD", "ESTADO", "ADMCOMENT", "RRHRUTSUP",
                "RRHRUTAPRUEBA"})
public final class Solicitud {
    private IntegerProperty numero;
    private IntegerProperty rutSolicitante;
    private StringProperty fechaSolicitud;
    private StringProperty estado;
    private StringProperty adminComentario;
    private IntegerProperty rutSupervisor;
    private IntegerProperty rutAprueba;

    private Solicitud(int numero, int rutSolicitante, String fechaSolicitud, String estado,
                      String adminComentario, int rutSupervisor, int rutAprueba) {
        this.numero = new SimpleIntegerProperty(numero);
        this.rutSolicitante = new SimpleIntegerProperty(rutSolicitante);
        this.fechaSolicitud = new SimpleStringProperty(fechaSolicitud);
        this.estado = new SimpleStringProperty(estado);
        this.adminComentario = new SimpleStringProperty(adminComentario);
        this.rutSupervisor = new SimpleIntegerProperty(rutSupervisor);
        this.rutAprueba = new SimpleIntegerProperty(rutAprueba);
    }

    public int getNumero() {
        return numero.get();
    }

    public IntegerProperty numeroProperty() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero.set(numero);
    }

    public int getRutSolicitante() {
        return rutSolicitante.get();
    }

    public IntegerProperty rutSolicitanteProperty() {
        return rutSolicitante;
    }

    public void setRutSolicitante(int rutSolicitante) {
        this.rutSolicitante.set(rutSolicitante);
    }

    public String getFechaSolicitud() {
        return fechaSolicitud.get();
    }

    public StringProperty fechaSolicitudProperty() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud.set(fechaSolicitud);
    }

    public String getEstado() {
        return estado.get();
    }

    public StringProperty estadoProperty() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado.set(estado);
    }

    public String getAdminComentario() {
        return adminComentario.get();
    }

    public StringProperty adminComentarioProperty() {
        return adminComentario;
    }

    public void setAdminComentario(String adminComentario) {
        this.adminComentario.set(adminComentario);
    }

    public int getRutSupervisor() {
        return rutSupervisor.get();
    }

    public IntegerProperty rutSupervisorProperty() {
        return rutSupervisor;
    }

    public void setRutSupervisor(int rutSupervisor) {
        this.rutSupervisor.set(rutSupervisor);
    }

    public int getRutAprueba() {
        return rutAprueba.get();
    }

    public IntegerProperty rutApruebaProperty() {
        return rutAprueba;
    }

    public void setRutAprueba(int rutAprueba) {
        this.rutAprueba.set(rutAprueba);
    }

    @Override
    public String toString() {
        return numero.get() + ", " + rutSolicitante.get() + ", " + fechaSolicitud.get() + ", " +
                estado.get() + ", " + adminComentario.get() + ", " + rutSupervisor.get() + ", " +
                rutAprueba.get();
    }

    public static final class Builder {
        private int numero;
        private int rutSolicitante;
        private String fechaSolicitud = "";
        private String estado = "";
        private String adminComentario;
        private int rutSupervisor;
        private int rutAprueba;

        public Builder setNumero(int numero) {
            this.numero = numero;

            return this;
        }

        public Builder setEstado(String estado) {
            this.estado = estado;

            return this;
        }

        public Builder setAdminComentario(String adminComentario) {
            this.adminComentario = adminComentario;

            return this;
        }

        public Builder setRutAprueba(int rutAprueba) {
            this.rutAprueba = rutAprueba;

            return this;
        }

        public Builder setRutSolicitante(int rutSolicitante) {
            this.rutSolicitante = rutSolicitante;

            return this;
        }

        public Builder setFechaSolicitud(String fechaSolicitud) {
            this.fechaSolicitud = fechaSolicitud;

            return this;
        }

        public Builder setRutSupervisor(int rutSupervisor) {
            this.rutSupervisor = rutSupervisor;

            return this;
        }

        public Solicitud build() {
            return new Solicitud(numero, rutSolicitante, fechaSolicitud, estado, adminComentario,
                    rutSupervisor, rutAprueba);
        }
    }

    public interface Dao {
        int createSolicitud(Solicitud created);
        int createMassSolicitud(Solicitud created);
        boolean updateEstadoSolicitud(Solicitud updated);
        boolean deleteSolicitud(int nSolicitud);
        int getSolicitudesPendientesSupervisorCount(int rutSupervisor);
        int getSolicitudesProcesadasCount();
        int getSolicitudesEsperaCount();
        int getSolicitudesPendientesCount();
        int getSolicitudesProcesadasSupervisorCount(int rutSupervisor);
        int getAllSolicitudesCount();
        Solicitud getSolicitud(int nsolicitud);
        ObservableList<Solicitud> getSolicitudesPendientesSupervisor(int rutSupervisor, int index, int pageSize);
        ObservableList<Solicitud> getSolicitudesPendientes(int index, int pageSize);
        ObservableList<Solicitud> getSolicitudesEspera(int index, int pageSize);
        ObservableList<Solicitud> getSolicitudesProcesadas(int index, int pageSize);
        ObservableList<Solicitud> getSolicitudesProcesadasSupervisor(int rutSupervisor, int index, int pageSize);
        ObservableList<Solicitud> getAllSolicitudes(int index, int pageSize);
        ObservableList<Solicitud> getSolicitudes(String from, String to);
        ObservableList<Solicitud> getSolicitudes(String from, String to, int rutSupervisor, String tipo);
        ObservableList<Solicitud> getSolicitudes(String from, String to, int codCcto);
    }
}
