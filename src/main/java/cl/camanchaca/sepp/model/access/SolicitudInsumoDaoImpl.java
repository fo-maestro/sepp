/*
 * SolicitudInsumoDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.ConnectorClause;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.database.query.WhereClause;
import cl.camanchaca.sepp.model.SolicitudInsumo;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudInsumoDaoImpl extends DBModel implements SolicitudInsumo.Dao {
    private SQLServer db;

    public SolicitudInsumoDaoImpl() {
        super(SolicitudInsumo.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public boolean createSolicitud(SolicitudInsumo created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME, COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[8])
                .values(created.getSolicitud(), created.getInsumo(), created.getCantidadSolicitada(),
                        created.getSupComent())
                .toQuery();

        try {
            db.rawUpdateQuery(queryStr);

            return true;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean createSolicitudAdmin(SolicitudInsumo created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME, COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3])
                .values(created.getSolicitud(), created.getInsumo(), created.getCantidadSolicitada(), created.getCantidadEntregada())
                .toQuery();

        try {
            db.rawUpdateQuery(queryStr);

            return true;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public ObservableList<SolicitudInsumo> getSolicitudInsumo(int solicitud) {
        ObservableList<SolicitudInsumo> solicitudInsumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudInsumos;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(solicitud)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date date = set.getDate(COLUMNS[6]);
                String fecha = null;

                if (date != null) {
                    fecha = DateFormatWrapper.toLatinForm(date.toString());
                }

                solicitudInsumos.add(new SolicitudInsumo.Builder()
                        .setSolicitud(set.getInt(COLUMNS[0]))
                        .setInsumo(set.getInt(COLUMNS[1]))
                        .setCantidadSolicitada(set.getInt(COLUMNS[2]))
                        .setCantidadEntregada(set.getInt(COLUMNS[3]))
                        .setObservacion(set.getString(COLUMNS[4]))
                        .setFirma(set.getString(COLUMNS[5]))
                        .setFechaEntrega(fecha)
                        .setRutEntrega(set.getInt(COLUMNS[7]))
                        .setSupComent(set.getString(COLUMNS[8]))
                        .build());
            }

            return solicitudInsumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudInsumos;
        }
    }

    @Override
    public ObservableList<SolicitudInsumo> getSolicitudInsumo(String from, String to, int insumo) {
        ObservableList<SolicitudInsumo> solicitudInsumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudInsumos;
        }

        ConnectorClause subQuery = new WhereClause()
                .column("FECSOLICITUD").graterThanOrEqual("?")
                .and("FECSOLICITUD").lessThanOrEqual("?");

        String queryStr = new QueryBuilder().newQuery()
                .select("SI." + COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4],
                        COLUMNS[5], COLUMNS[6], COLUMNS[7], COLUMNS[8])
                .from(TABLE_NAME + " SI" + ", Solicitud S")
                .where("S." + COLUMNS[0]).equals("SI." + COLUMNS[0])
                .and(subQuery)
                .and(COLUMNS[1]).equals(insumo)
                .toQuery();

        ContentValues values = new ContentValues();
        values.put("1", from);
        values.put("2", to);

        try (Result result = db.preparedQuery(queryStr, values)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date date = set.getDate(COLUMNS[6]);
                String fecha = null;

                if (date != null) {
                    fecha = DateFormatWrapper.toLatinForm(date.toString());
                }

                solicitudInsumos.add(new SolicitudInsumo.Builder()
                        .setSolicitud(set.getInt(COLUMNS[0]))
                        .setInsumo(set.getInt(COLUMNS[1]))
                        .setCantidadSolicitada(set.getInt(COLUMNS[2]))
                        .setCantidadEntregada(set.getInt(COLUMNS[3]))
                        .setObservacion(set.getString(COLUMNS[4]))
                        .setFirma(set.getString(COLUMNS[5]))
                        .setFechaEntrega(fecha)
                        .setRutEntrega(set.getInt(COLUMNS[7]))
                        .setSupComent(set.getString(COLUMNS[8]))
                        .build());
            }

            return solicitudInsumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudInsumos;
        }
    }

    @Override
    public ObservableList<SolicitudInsumo> getSolicitudInsumo(String from, String to, int insumo, int rut) {
        ObservableList<SolicitudInsumo> solicitudInsumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudInsumos;
        }

        ConnectorClause subQuery = new WhereClause()
                .column("FECSOLICITUD").graterThanOrEqual("?")
                .and("FECSOLICITUD").lessThanOrEqual("?");

        String queryStr = new QueryBuilder().newQuery()
                .select("SI." + COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4],
                        COLUMNS[5], COLUMNS[6], COLUMNS[7], COLUMNS[8])
                .from(TABLE_NAME + " SI" + ", Solicitud S")
                .where("S." + COLUMNS[0]).equals("SI." + COLUMNS[0])
                .and(subQuery)
                .and(COLUMNS[1]).equals(insumo)
                .and("S.RRHRUTSUP").equals(rut)
                .toQuery();

        ContentValues values = new ContentValues();
        values.put("1", from);
        values.put("2", to);

        try (Result result = db.preparedQuery(queryStr, values)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date date = set.getDate(COLUMNS[6]);
                String fecha = null;

                if (date != null) {
                    fecha = DateFormatWrapper.toLatinForm(date.toString());
                }

                solicitudInsumos.add(new SolicitudInsumo.Builder()
                        .setSolicitud(set.getInt(COLUMNS[0]))
                        .setInsumo(set.getInt(COLUMNS[1]))
                        .setCantidadSolicitada(set.getInt(COLUMNS[2]))
                        .setCantidadEntregada(set.getInt(COLUMNS[3]))
                        .setObservacion(set.getString(COLUMNS[4]))
                        .setFirma(set.getString(COLUMNS[5]))
                        .setFechaEntrega(fecha)
                        .setRutEntrega(set.getInt(COLUMNS[7]))
                        .setSupComent(set.getString(COLUMNS[8]))
                        .build());
            }

            return solicitudInsumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudInsumos;
        }
    }


    @Override
    public boolean updateSolicitud(SolicitudInsumo updated) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .update()
                .into(TABLE_NAME)
                .set()
                .column(COLUMNS[3]).equals("?")
                .comma()
                .column(COLUMNS[4]).equals("?")
                .comma()
                .column(COLUMNS[5]).equals("?")
                .comma()
                .column(COLUMNS[6]).equals(SQLServer.toSqlServerDateTime("?"))
                .comma()
                .column(COLUMNS[7]).equals("?")
                .where(COLUMNS[0]).equals("?")
                .and(COLUMNS[1]).equals("?")
                .toQuery();

        ContentValues values = new ContentValues();

        values.put(COLUMNS[3], updated.getCantidadEntregada());
        values.put(COLUMNS[4], updated.getObservacion());
        values.put(COLUMNS[5], updated.getFirma());
        values.put(COLUMNS[6], DateFormatWrapper.toDatabaseForm(updated.getFechaEntrega()));
        values.put(COLUMNS[7], updated.getRutEntrega());
        values.put(COLUMNS[0], updated.getSolicitud());
        values.put(COLUMNS[1], updated.getInsumo());

        try {
            return db.preparedUpdateQuery(queryStr, values) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public void enableTrigger() {
        if (TABLE_NAME == null || TRIGGERS == null) {
            return;
        }

        try {
            db.enableTrigger(TABLE_NAME, TRIGGERS[0]);
            db.enableTrigger(TABLE_NAME, TRIGGERS[1]);
            db.enableTrigger(TABLE_NAME, TRIGGERS[2]);
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
        }
    }

    @Override
    public void disableTrigger() {
        if (TABLE_NAME == null || TRIGGERS == null) {
            return;
        }

        try {
            db.disableTrigger(TABLE_NAME, TRIGGERS[0]);
            db.disableTrigger(TABLE_NAME, TRIGGERS[1]);
            db.disableTrigger(TABLE_NAME, TRIGGERS[2]);
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);
        }
    }
}
