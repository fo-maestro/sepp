/*
 * CuentaPersonalDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.model.CuentaPersonal;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class CuentaPersonalDaoImpl implements CuentaPersonal.Dao {
    @Override
    public ObservableList<CuentaPersonal> getAllCuentas() {
        ObservableList<CuentaPersonal> data = FXCollections.observableArrayList();

        new CuentaDaoImpl().getAllCuentas().forEach(cuenta ->
                data.add(new CuentaPersonal(cuenta,
                        new PersonalDaoImpl().getPersonal(cuenta.getRut()))));

        return data;
    }
}
