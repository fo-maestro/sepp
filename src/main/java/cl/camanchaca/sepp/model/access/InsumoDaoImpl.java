/*
 * InsumoDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumoDaoImpl extends DBModel implements Insumo.Dao {
    private SQLServer db;

    public InsumoDaoImpl() {
        super(Insumo.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public ObservableList<Insumo> getAllInsumos() {
        ObservableList<Insumo> insumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return insumos;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                insumos.add(new Insumo.Builder()
                        .setCodigo(set.getInt(COLUMNS[0]))
                        .setNombre(set.getString(COLUMNS[1]))
                        .setCantidad(set.getInt(COLUMNS[2]))
                        .build());
            }

            return insumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return insumos;
        }
    }

    @Override
    public Insumo getInsumo(int codigo) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return null;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(codigo)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            if (set.next()) {
                return new Insumo.Builder()
                        .setCodigo(set.getInt(COLUMNS[0]))
                        .setNombre(set.getString(COLUMNS[1]))
                        .setCantidad(set.getInt(COLUMNS[2]))
                        .build();
            }

            return null;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return null;
        }
    }

    @Override
    public boolean createInsumo(Insumo created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME)
                .values("?", "?", "?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[0], created.getCodigo());
        values.put(COLUMNS[1], created.getNombre());
        values.put(COLUMNS[2], created.getCantidad());

        try {
            return db.preparedUpdateQuery(queryStr, values) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean updateInsumo(Insumo updated) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .update()
                .into(TABLE_NAME)
                .set()
                .column(COLUMNS[2]).equals(updated.getCantidad())
                .where(COLUMNS[0]).equals(updated.getCodigo())
                .toQuery();

        try {
            return db.rawUpdateQuery(queryStr) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }
}
