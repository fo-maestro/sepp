/*
 * CuentaDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.model.Cuenta;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class CuentaDaoImpl extends DBModel implements Cuenta.Dao {
    private SQLServer db;

    public CuentaDaoImpl() {
        super(Cuenta.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public Cuenta getCuenta(String usuario) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return null;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[1])
                .equals("?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[1], usuario);

        try (Result result = db.preparedQuery(queryStr, values)) {
            ResultSet set = result.getResultSet();

            if (set.next()) {
                return new Cuenta.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setUsuario(set.getString(COLUMNS[1]))
                        .setContra(set.getString(COLUMNS[2]))
                        .setNivel(set.getInt(COLUMNS[3]))
                        .build();
            }

            return null;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return null;
        }
    }

    @Override
    public ObservableList<Cuenta> getAllCuentas() {
        ObservableList<Cuenta> cuentas = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return cuentas;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[3])
                .distinct(0)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                cuentas.add(new Cuenta.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setUsuario(set.getString(COLUMNS[1]))
                        .setContra(set.getString(COLUMNS[2]))
                        .setNivel(set.getInt(COLUMNS[3]))
                        .build());
            }

            return cuentas;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return cuentas;
        }
    }

    @Override
    public boolean createCuenta(Cuenta created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME)
                .values("?", "?", "?", "?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[0], created.getRut());
        values.put(COLUMNS[1], created.getUsuario());
        values.put(COLUMNS[2], created.getContra());
        values.put(COLUMNS[3], created.getNivel());

        try {
            return db.preparedUpdateQuery(queryStr, values) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean updateCuenta(Cuenta updated) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .update()
                .into(TABLE_NAME)
                .set()
                .column(COLUMNS[3]).equals("?")
                .where(COLUMNS[0]).equals("?")
                .and(COLUMNS[1]).equals("?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], updated.getNivel());
        values.put(COLUMNS[0], updated.getRut());
        values.put(COLUMNS[1], updated.getUsuario());

        try {
            return db.preparedUpdateQuery(queryStr, values) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean deleteCuenta(int rut) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .delete()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(rut)
                .toQuery();

        try {
            return db.rawUpdateQuery(queryStr) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }
}
