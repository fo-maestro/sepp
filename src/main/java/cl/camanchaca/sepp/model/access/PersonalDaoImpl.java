/*
 * PersonalDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.ConnectorClause;
import cl.camanchaca.sepp.database.query.FunctionClause;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.database.query.WhereClause;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class PersonalDaoImpl extends DBModel implements Personal.Dao {
    private SQLServer db;

    public PersonalDaoImpl() {
        super(Personal.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public Personal getPersonal(int rut) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return new Personal.Builder().build();
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(rut)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            if (set.next()) {
                Date fechaIngRet = set.getDate(COLUMNS[7]);
                Date fechaTerRet = set.getDate(COLUMNS[8]);

                String fechaIng = null;
                String fechaTer = null;

                if (fechaIngRet != null) {
                    fechaIng = DateFormatWrapper.toLatinForm(fechaIngRet.toString());
                }

                if (fechaTerRet != null) {
                    fechaTer = DateFormatWrapper.toLatinForm(fechaTerRet.toString());
                }

                return new Personal.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setDvd(set.getString(COLUMNS[1]))
                        .setApellidoP(set.getString(COLUMNS[2]))
                        .setApellidoM(set.getString(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setNombre2(set.getString(COLUMNS[5]))
                        .setCargo(set.getString(COLUMNS[6]))
                        .setFechaIng(fechaIng)
                        .setFechaTer(fechaTer)
                        .setSindicato(set.getInt(COLUMNS[9]))
                        .setCodigoCcto(set.getInt(COLUMNS[10]))
                        .build();
            }
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return null;
        }

        return null;
    }

    @Override
    public ObservableList<Personal> getPersonalAsociadoSupervisor(Personal supervisor) {
        ObservableList<Personal> elements = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return elements;
        }

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[10]).equals(supervisor.getCodigoCcto())
                .or(COLUMNS[10]).equals(CentroCostos.reverseCcto(supervisor.getCodigoCcto()));

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(subQuery)
                .and(COLUMNS[6]).not().like("SUPERVISOR%")
                .or(COLUMNS[0]).equals(supervisor.getRut())
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaIngRet = set.getDate(COLUMNS[7]);
                Date fechaTerRet = set.getDate(COLUMNS[8]);

                String fechaIng = null;
                String fechaTer = null;

                if (fechaIngRet != null) {
                    fechaIng = DateFormatWrapper.toLatinForm(fechaIngRet.toString());
                }

                if (fechaTerRet != null) {
                    fechaTer = DateFormatWrapper.toLatinForm(fechaTerRet.toString());
                }

                elements.add(new Personal.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setDvd(set.getString(COLUMNS[1]))
                        .setApellidoP(set.getString(COLUMNS[2]))
                        .setApellidoM(set.getString(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setNombre2(set.getString(COLUMNS[5]))
                        .setCargo(set.getString(COLUMNS[6]))
                        .setFechaIng(fechaIng)
                        .setFechaTer(fechaTer)
                        .setSindicato(set.getInt(COLUMNS[9]))
                        .setCodigoCcto(set.getInt(COLUMNS[10]))
                        .build());
            }

            return elements;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return elements;
        }
    }

    @Override
    public ObservableList<Personal> getAllSupervisores() {
        ObservableList<Personal> elements = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return elements;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[6]).like("SUPERVISOR%")
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaIngRet = set.getDate(COLUMNS[7]);
                Date fechaTerRet = set.getDate(COLUMNS[8]);

                String fechaIng = null;
                String fechaTer = null;

                if (fechaIngRet != null) {
                    fechaIng = DateFormatWrapper.toLatinForm(fechaIngRet.toString());
                }

                if (fechaTerRet != null) {
                    fechaTer = DateFormatWrapper.toLatinForm(fechaTerRet.toString());
                }

                elements.add(new Personal.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setDvd(set.getString(COLUMNS[1]))
                        .setApellidoP(set.getString(COLUMNS[2]))
                        .setApellidoM(set.getString(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setNombre2(set.getString(COLUMNS[5]))
                        .setCargo(set.getString(COLUMNS[6]))
                        .setFechaIng(fechaIng)
                        .setFechaTer(fechaTer)
                        .setSindicato(set.getInt(COLUMNS[9]))
                        .setCodigoCcto(set.getInt(COLUMNS[10]))
                        .build());
            }

            return elements;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return elements;
        }
    }

    @Override
    public ObservableList<Personal> getAllPersonal() {
        ObservableList<Personal> elements = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return elements;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaIngRet = set.getDate(COLUMNS[7]);
                Date fechaTerRet = set.getDate(COLUMNS[8]);

                String fechaIng = null;
                String fechaTer = null;

                if (fechaIngRet != null) {
                    fechaIng = DateFormatWrapper.toLatinForm(fechaIngRet.toString());
                }

                if (fechaTerRet != null) {
                    fechaTer = DateFormatWrapper.toLatinForm(fechaTerRet.toString());
                }

                elements.add(new Personal.Builder()
                        .setRut(set.getInt(COLUMNS[0]))
                        .setDvd(set.getString(COLUMNS[1]))
                        .setApellidoP(set.getString(COLUMNS[2]))
                        .setApellidoM(set.getString(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setNombre2(set.getString(COLUMNS[5]))
                        .setCargo(set.getString(COLUMNS[6]))
                        .setFechaIng(fechaIng)
                        .setFechaTer(fechaTer)
                        .setSindicato(set.getInt(COLUMNS[9]))
                        .setCodigoCcto(set.getInt(COLUMNS[10]))
                        .build());
            }

            return elements;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return elements;
        }
    }
}
