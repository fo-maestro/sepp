/*
 * SolicitudDetalleDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudDetalleDaoImpl implements SolicitudDetalle.Dao {

    @Override
    public ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to) {
        ObservableList<SolicitudDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getSolicitudes(from, to).forEach(solicitud ->
            new SolicitudInsumoDaoImpl().getSolicitudInsumo(solicitud.getNumero()).forEach(solicitudInsumo -> {
                SolicitudDetalle.Builder builder = new SolicitudDetalle.Builder()
                        .setSolicitud(solicitud.getNumero())
                        .setFecha(solicitud.getFechaSolicitud())
                        .setEstado(solicitud.getEstado());

                Personal colaborador = new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante());
                Personal supervisor = new PersonalDaoImpl().getPersonal(solicitud.getRutSupervisor());
                Insumo insumo = new InsumoDaoImpl().getInsumo(solicitudInsumo.getInsumo());

                if (colaborador != null) {
                    builder.setRutColaborador(colaborador.getRut() + "-" + colaborador.getDvd())
                            .setNombreColaborador(colaborador.getNombre() + " " +
                                    colaborador.getNombre2() + " " +
                                    colaborador.getApellidoP() + " " +
                                    colaborador.getApellidoM());

                    CentroCostos ccto = new CentroCostosDaoImpl().getCentroCostos(colaborador.getCodigoCcto());

                    if (ccto != null) {
                        builder.setCodigoCcto(colaborador.getCodigoCcto())
                                .setCcto(ccto.getNombre());
                    }
                }

                if (insumo != null) {
                    builder.setInsumo(insumo.getCodigo())
                            .setDescripcion(insumo.getNombre())
                            .setTotal(solicitudInsumo.getCantidadEntregada());
                }

                if (supervisor != null) {
                    builder.setRutSupervisor(supervisor.getRut() + "-" + supervisor.getDvd())
                            .setNombreSupervisor(supervisor.getNombre() + " " +
                                    supervisor.getNombre2() + " " +
                                    supervisor.getApellidoP() + " " +
                                    supervisor.getApellidoM());
                }

                detalle.add(builder.build());
            }));

        return detalle;
    }

    @Override
    public ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int rut, String tipo) {
        ObservableList<SolicitudDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getSolicitudes(from, to, rut, tipo).forEach(solicitud ->
                new SolicitudInsumoDaoImpl().getSolicitudInsumo(solicitud.getNumero()).forEach(solicitudInsumo -> {
                    SolicitudDetalle.Builder builder = new SolicitudDetalle.Builder()
                            .setSolicitud(solicitud.getNumero())
                            .setFecha(solicitud.getFechaSolicitud())
                            .setEstado(solicitud.getEstado());

                    Personal colaborador = new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante());
                    Personal supervisor = new PersonalDaoImpl().getPersonal(solicitud.getRutSupervisor());
                    Insumo insumo = new InsumoDaoImpl().getInsumo(solicitudInsumo.getInsumo());

                    if (colaborador != null) {
                        builder.setRutColaborador(colaborador.getRut() + "-" + colaborador.getDvd())
                                .setNombreColaborador(colaborador.getNombre() + " " +
                                        colaborador.getNombre2() + " " +
                                        colaborador.getApellidoP() + " " +
                                        colaborador.getApellidoM());

                        CentroCostos ccto = new CentroCostosDaoImpl().getCentroCostos(colaborador.getCodigoCcto());

                        if (ccto != null) {
                            builder.setCodigoCcto(colaborador.getCodigoCcto())
                                    .setCcto(ccto.getNombre());
                        }
                    }

                    if (supervisor != null) {
                        builder.setRutSupervisor(supervisor.getRut() + "-" + supervisor.getDvd())
                                .setNombreSupervisor(supervisor.getNombre() + " " +
                                        supervisor.getNombre2() + " " +
                                        supervisor.getApellidoP() + " " +
                                        supervisor.getApellidoM());
                    }

                    if (insumo != null) {
                        builder.setInsumo(insumo.getCodigo())
                                .setDescripcion(insumo.getNombre())
                                .setTotal(solicitudInsumo.getCantidadEntregada());
                    }

                    detalle.add(builder.build());
                }));

        return detalle;
    }

    @Override
    public ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int insumo) {
        ObservableList<SolicitudDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudInsumoDaoImpl().getSolicitudInsumo(from, to, insumo)
                .forEach(solicitudInsumo -> {
                    SolicitudDetalle.Builder builder = new SolicitudDetalle.Builder();

                    Solicitud solicitud = new SolicitudDaoImpl().getSolicitud(solicitudInsumo.getSolicitud());
                    Insumo insumoGet = new InsumoDaoImpl().getInsumo(insumo);

                    if (solicitud != null) {
                        Personal colaborador = new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante());
                        Personal supervisor = new PersonalDaoImpl().getPersonal(solicitud.getRutSupervisor());

                        builder.setSolicitud(solicitud.getNumero())
                                .setFecha(solicitud.getFechaSolicitud())
                                .setEstado(solicitud.getEstado());

                        if (supervisor != null) {
                            builder.setRutSupervisor(supervisor.getRut() + "-" + supervisor.getDvd())
                                    .setNombreSupervisor(supervisor.getNombre() + " " +
                                            supervisor.getNombre2() + " " +
                                            supervisor.getApellidoP() + " " +
                                            supervisor.getApellidoM());
                        }

                        if (colaborador != null) {
                            builder.setRutColaborador(colaborador.getRut() + "-" + colaborador.getDvd())
                                    .setNombreColaborador(colaborador.getNombre() + " " +
                                            colaborador.getNombre2() + " " +
                                            colaborador.getApellidoP() + " " +
                                            colaborador.getApellidoM());

                            CentroCostos ccto = new CentroCostosDaoImpl().getCentroCostos(colaborador.getCodigoCcto());

                            if (ccto != null) {
                                builder.setCodigoCcto(colaborador.getCodigoCcto())
                                        .setCcto(ccto.getNombre());
                            }
                        }
                    }

                    if (insumoGet != null) {
                        builder.setInsumo(insumoGet.getCodigo())
                                .setDescripcion(insumoGet.getNombre())
                                .setTotal(solicitudInsumo.getCantidadEntregada());
                    }

                    detalle.add(builder.build());
                });

        return detalle;
    }

    @Override
    public ObservableList<SolicitudDetalle> getSolicitudDetalle(String from, String to, int insumo, int rutSupervisor) {
        ObservableList<SolicitudDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudInsumoDaoImpl().getSolicitudInsumo(from, to, insumo, rutSupervisor)
                .forEach(solicitudInsumo -> {
                    SolicitudDetalle.Builder builder = new SolicitudDetalle.Builder();

                    Solicitud solicitud = new SolicitudDaoImpl().getSolicitud(solicitudInsumo.getSolicitud());
                    Personal supervisor = new PersonalDaoImpl().getPersonal(rutSupervisor);

                    Insumo insumoGet = new InsumoDaoImpl().getInsumo(solicitudInsumo.getInsumo());

                    if (solicitud != null) {
                        builder.setSolicitud(solicitud.getNumero())
                                .setFecha(solicitud.getFechaSolicitud())
                                .setEstado(solicitud.getEstado());

                        Personal colaborador = new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante());

                        if (colaborador != null) {
                            builder.setRutColaborador(colaborador.getRut() + "-" + colaborador.getDvd())
                                    .setNombreColaborador(colaborador.getNombre() + " " +
                                            colaborador.getNombre2() + " " +
                                            colaborador.getApellidoP() + " " +
                                            colaborador.getApellidoM());

                            CentroCostos ccto = new CentroCostosDaoImpl().getCentroCostos(colaborador.getCodigoCcto());

                            if (ccto != null) {
                                builder.setCodigoCcto(colaborador.getCodigoCcto())
                                        .setCcto(ccto.getNombre());
                            }
                        }
                    }

                    if (insumoGet != null) {
                        builder.setInsumo(insumoGet.getCodigo())
                                .setDescripcion(insumoGet.getNombre())
                                .setTotal(solicitudInsumo.getCantidadEntregada());
                    }

                    if (supervisor != null) {
                        builder.setRutSupervisor(supervisor.getRut() + "-" + supervisor.getDvd())
                                .setNombreSupervisor(supervisor.getNombre() + " " +
                                        supervisor.getNombre2() + " " +
                                        supervisor.getApellidoP() + " " +
                                        supervisor.getApellidoM());
                    }

                    detalle.add(builder.build());
                });

        return detalle;
    }

    @Override
    public ObservableList<SolicitudDetalle> getSolicitudDetalleCtto(String from, String to, int codCcto) {
        ObservableList<SolicitudDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getSolicitudes(from, to, codCcto)
                .forEach(solicitud -> new SolicitudInsumoDaoImpl().getSolicitudInsumo(solicitud.getNumero())
                        .forEach(solicitudInsumo -> {
                            SolicitudDetalle.Builder builder = new SolicitudDetalle.Builder()
                                    .setSolicitud(solicitud.getNumero())
                                    .setFecha(solicitud.getFechaSolicitud())
                                    .setEstado(solicitud.getEstado());

                            Personal colaborador = new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante());
                            Personal supervisor = new PersonalDaoImpl().getPersonal(solicitud.getRutSupervisor());
                            Insumo insumo = new InsumoDaoImpl().getInsumo(solicitudInsumo.getInsumo());

                            if (colaborador != null) {
                                builder.setRutColaborador(colaborador.getRut() + "-" + colaborador.getDvd())
                                        .setNombreColaborador(colaborador.getNombre() + " " +
                                                colaborador.getNombre2() + " " +
                                                colaborador.getApellidoP() + " " +
                                                colaborador.getApellidoM());

                            }

                            CentroCostos ccto = new CentroCostosDaoImpl().getCentroCostos(codCcto);

                            if (ccto != null) {
                                builder.setCodigoCcto(colaborador.getCodigoCcto())
                                        .setCcto(ccto.getNombre());
                            }

                            if (supervisor != null) {
                                builder.setRutSupervisor(supervisor.getRut() + "-" + supervisor.getDvd())
                                        .setNombreSupervisor(supervisor.getNombre() + " " +
                                                supervisor.getNombre2() + " " +
                                                supervisor.getApellidoP() + " " +
                                                supervisor.getApellidoM());
                            }

                            if (insumo != null) {
                                builder.setInsumo(insumo.getCodigo())
                                        .setDescripcion(insumo.getNombre())
                                        .setTotal(solicitudInsumo.getCantidadEntregada());
                            }

                            detalle.add(builder.build());
                        }));

        return detalle;
    }
}
