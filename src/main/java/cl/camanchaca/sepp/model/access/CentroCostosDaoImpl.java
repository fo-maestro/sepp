/*
 * CentroCostosDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.ConnectorClause;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.database.query.WhereClause;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class CentroCostosDaoImpl extends DBModel implements CentroCostos.Dao {
    private SQLServer db;

    public CentroCostosDaoImpl() {
        super(CentroCostos.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public ObservableList<CentroCostos> getAllCentroCostos() {
        ObservableList<CentroCostos> elemets = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return elemets;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                elemets.add(new CentroCostos.Builder()
                        .setCodCcto(set.getInt(COLUMNS[0]))
                        .setCodArea(set.getInt(COLUMNS[1]))
                        .setCodEmpresa(set.getInt(COLUMNS[2]))
                        .setCodPlanta(set.getInt(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setSeleccion(set.getInt(COLUMNS[5]))
                        .build());
            }

            return elemets;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return elemets;
        }
    }

    @Override
    public ObservableList<CentroCostos> getCentroCostosCompleto(int codCctoCompleto) {
        ObservableList<CentroCostos> elemets = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return elemets;
        }

        int reverse_ccto = CentroCostos.reverseCcto(codCctoCompleto);

        int[] ccto = CentroCostos.reverseCodigoCcto(codCctoCompleto);
        int[] ccto_reverse = CentroCostos.reverseCodigoCcto(reverse_ccto);

        if (ccto == null && ccto_reverse == null) {
            return elemets;
        }

        ConnectorClause cctoC = new WhereClause()
                .column(COLUMNS[0]).equals(ccto[0])
                .or(COLUMNS[0]).equals(ccto_reverse[0]);

        ConnectorClause areaC = new WhereClause()
                .column(COLUMNS[1]).equals(ccto[1])
                .or(COLUMNS[1]).equals(ccto_reverse[1]);

        ConnectorClause empresaC = new WhereClause()
                .column(COLUMNS[2]).equals(ccto[2])
                .or(COLUMNS[2]).equals(ccto_reverse[2]);

        ConnectorClause plantaC = new WhereClause()
                .column(COLUMNS[3]).equals(ccto[3])
                .or(COLUMNS[3]).equals(ccto_reverse[3]);

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(cctoC)
                .and(areaC)
                .and(empresaC)
                .and(plantaC)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                elemets.add(new CentroCostos.Builder()
                        .setCodCcto(set.getInt(COLUMNS[0]))
                        .setCodArea(set.getInt(COLUMNS[1]))
                        .setCodEmpresa(set.getInt(COLUMNS[2]))
                        .setCodPlanta(set.getInt(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setSeleccion(set.getInt(COLUMNS[5]))
                        .build());
            }

            return elemets;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return elemets;
        }
    }

    @Override
    public CentroCostos getCentroCostos(int codCctoCompleto) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return null;
        }

        int[] ccto = CentroCostos.reverseCodigoCcto(codCctoCompleto);

        if (ccto == null) {
            return null;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(ccto[0])
                .and(COLUMNS[1]).equals(ccto[1])
                .and(COLUMNS[2]).equals(ccto[2])
                .and(COLUMNS[3]).equals(ccto[3])
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            if (set.next()) {
                return new CentroCostos.Builder()
                        .setCodCcto(set.getInt(COLUMNS[0]))
                        .setCodArea(set.getInt(COLUMNS[1]))
                        .setCodEmpresa(set.getInt(COLUMNS[2]))
                        .setCodPlanta(set.getInt(COLUMNS[3]))
                        .setNombre(set.getString(COLUMNS[4]))
                        .setSeleccion(set.getInt(COLUMNS[5]))
                        .build();
            }

            return null;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return null;
        }
    }
}
