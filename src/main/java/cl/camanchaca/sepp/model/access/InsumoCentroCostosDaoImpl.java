/*
 * InsumoCentroCostosDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.ConnectorClause;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.database.query.WhereClause;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.InsumoCentroCostos;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumoCentroCostosDaoImpl extends DBModel
        implements InsumoCentroCostos.Dao {

    private SQLServer db;

    public InsumoCentroCostosDaoImpl() {
        super(InsumoCentroCostos.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public ObservableList<InsumoCentroCostos> getAllInsumoCentroCostos() {
        ObservableList<InsumoCentroCostos> ccto = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return ccto;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                ccto.add(new InsumoCentroCostos.Builder()
                        .setCodigoInsumo(set.getInt(COLUMNS[0]))
                        .setCentroCostos(set.getInt(COLUMNS[1]))
                        .setDuracion(set.getInt(COLUMNS[2]))
                        .build());
            }

            return ccto;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return ccto;
        }
    }

    @Override
    public ObservableList<Insumo> getInsumos(int codCctoCompleto) {
        ObservableList<Insumo> insumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return insumos;
        }
        String insumoTable = new InsumoDaoImpl().getTableName();
        String[] insumoColumns = new InsumoDaoImpl().getColumns();

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[1]).equals(codCctoCompleto)
                .or(COLUMNS[1]).equals(CentroCostos.reverseCcto(codCctoCompleto));

        String queryStr = new QueryBuilder().newQuery()
                .select("I." + insumoColumns[0],
                        "I." + insumoColumns[1],
                        "I." + insumoColumns[2])
                .from(insumoTable + " I, " + TABLE_NAME + " IC")
                .where("I." + insumoColumns[0]).equals("IC." + COLUMNS[0])
                .and(subQuery)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();

            while (set.next()) {
                insumos.add(new Insumo.Builder()
                        .setCodigo(set.getInt(insumoColumns[0]))
                        .setNombre(set.getString(insumoColumns[1]))
                        .setCantidad(set.getInt(insumoColumns[2]))
                        .build());
            }

            return insumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return insumos;
        }
    }

    @Override
    public ObservableList<Insumo> getInsumos() {
        ObservableList<Insumo> insumos = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return insumos;
        }

        String insumoTable = new InsumoDaoImpl().getTableName();
        String[] insumoColumns = new InsumoDaoImpl().getColumns();

        String queryStr = new QueryBuilder().newQuery()
                .select("I." + insumoColumns[0],
                        "I." + insumoColumns[1],
                        "I." + insumoColumns[2])
                .from(insumoTable + " I, " + TABLE_NAME + " IC")
                .where("I." + insumoColumns[0]).equals("IC." + COLUMNS[0])
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();

            while (set.next()) {
                insumos.add(new Insumo.Builder()
                        .setCodigo(set.getInt(insumoColumns[0]))
                        .setNombre(set.getString(insumoColumns[1]))
                        .setCantidad(set.getInt(insumoColumns[2]))
                        .build());
            }

            return insumos;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return insumos;
        }
    }

    @Override
    public boolean createInsumoCentroCostos(InsumoCentroCostos created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME)
                .values(created.getCodigoInsumo(), created.getCentroCostos(), created.getDuracion())
                .toQuery();

        try {
            db.rawUpdateQuery(queryStr);

            return true;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean updateInsumoCentroCostos(InsumoCentroCostos updated) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .update()
                .into(TABLE_NAME)
                .set()
                .column(COLUMNS[2]).equals(updated.getDuracion())
                .where(COLUMNS[0]).equals(updated.getCodigoInsumo())
                .and(COLUMNS[1]).equals(updated.getCentroCostos())
                .toQuery();

        try {
            return db.rawUpdateQuery(queryStr) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean deleteInsumoCentroCostos(InsumoCentroCostos deleted) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .delete()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(deleted.getCodigoInsumo())
                .and(COLUMNS[1]).equals(deleted.getCentroCostos())
                .toQuery();

        try {
            db.rawUpdateQuery(queryStr);

            return true;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }
}
