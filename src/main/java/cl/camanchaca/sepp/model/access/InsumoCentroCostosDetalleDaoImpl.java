/*
 * InsumoCentroCostosDetalleDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.model.InsumoCentroCostosDetalle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class InsumoCentroCostosDetalleDaoImpl implements InsumoCentroCostosDetalle.Dao {

    @Override
    public ObservableList<InsumoCentroCostosDetalle> getAllInsumoCentroCostos() {
        ObservableList<InsumoCentroCostosDetalle> data = FXCollections.observableArrayList();

        new InsumoCentroCostosDaoImpl().getAllInsumoCentroCostos()
                .forEach(consumer ->
                        data.add(new InsumoCentroCostosDetalle(new InsumoDaoImpl()
                                .getInsumo(consumer.getCodigoInsumo()),
                                new CentroCostosDaoImpl().getCentroCostos(consumer
                                        .getCentroCostos()), consumer)));

        return data;
    }
}
