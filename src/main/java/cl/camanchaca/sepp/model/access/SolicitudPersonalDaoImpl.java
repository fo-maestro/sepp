/*
 * SolicitudPersonalDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.model.SolicitudPersonal;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudPersonalDaoImpl implements SolicitudPersonal.Dao {

    @Override
    public ObservableList<SolicitudPersonal> getSolicitudesSupervisor(int rutSupervisor,
                                                                      int index,
                                                                      int pageSize) {
        ObservableList<SolicitudPersonal> solicitudesPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl()
                .getSolicitudesPendientesSupervisor(rutSupervisor, index, pageSize).forEach(solicitud ->
            solicitudesPersonal.add(new SolicitudPersonal(solicitud,
                    new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudesPersonal;
    }

    @Override
    public ObservableList<SolicitudPersonal> getSolicitudesPanol(int index, int pageSize) {
        ObservableList<SolicitudPersonal> solicitudesPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getSolicitudesPendientes(index, pageSize).forEach(solicitud ->
                solicitudesPersonal.add(new SolicitudPersonal(solicitud,
                        new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudesPersonal;
    }

    @Override
    public ObservableList<SolicitudPersonal> getSolicitudesAdmin(int index, int pageSize) {
        ObservableList<SolicitudPersonal> solicitudesPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl()
                .getSolicitudesEspera(index, pageSize).forEach(solicitud ->
                solicitudesPersonal.add(new SolicitudPersonal(solicitud,
                        new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudesPersonal;
    }

    @Override
    public ObservableList<SolicitudPersonal> getSolicitudesProcesadas(int index, int pageSize) {
        ObservableList<SolicitudPersonal> solicitudPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getSolicitudesProcesadas(index, pageSize).forEach(solicitud ->
            solicitudPersonal.add(new SolicitudPersonal(solicitud,
                    new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudPersonal;
    }

    @Override
    public ObservableList<SolicitudPersonal> getSolicitudesProcesadasSupervisor(int rutSupervisor,
                                                                                int index,
                                                                                int pageSize) {
        ObservableList<SolicitudPersonal> solicitudesPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl()
                .getSolicitudesProcesadasSupervisor(rutSupervisor, index, pageSize).forEach(solicitud ->
                solicitudesPersonal.add(new SolicitudPersonal(solicitud,
                        new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudesPersonal;
    }

    @Override
    public ObservableList<SolicitudPersonal> getAllSolicitudes(int index, int pageSize) {
        ObservableList<SolicitudPersonal> solicitudPersonal = FXCollections.observableArrayList();

        new SolicitudDaoImpl().getAllSolicitudes(index, pageSize).forEach(solicitud ->
                solicitudPersonal.add(new SolicitudPersonal(solicitud,
                        new PersonalDaoImpl().getPersonal(solicitud.getRutSolicitante())))
        );

        return solicitudPersonal;
    }
}
