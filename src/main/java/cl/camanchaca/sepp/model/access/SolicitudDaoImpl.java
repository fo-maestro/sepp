/*
 * SolicitudDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.database.DBModel;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.*;
import cl.camanchaca.sepp.model.Solicitud;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import cl.camanchaca.sepp.util.LoggerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudDaoImpl extends DBModel implements Solicitud.Dao {
    private SQLServer db;

    public SolicitudDaoImpl() {
        super(Solicitud.class);
        db = ServiceLocator.getService(SQLServer.class);
    }

    @Override
    public int createSolicitud(Solicitud created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return -1;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME, COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[5])
                .values("?", SQLServer.toSqlServerDateTime("?"), "?", "?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[1], created.getRutSolicitante());
        values.put(COLUMNS[2], DateFormatWrapper.toDatabaseForm(created.getFechaSolicitud()));
        values.put(COLUMNS[3], created.getEstado());
        values.put(COLUMNS[5], created.getRutSupervisor());

        try {
            db.preparedUpdateQuery(queryStr, values);

            queryStr = new QueryBuilder().newQuery()
                    .select(FunctionClause.max(COLUMNS[0]).as(COLUMNS[0]).toString())
                    .from(TABLE_NAME)
                    .where(COLUMNS[1]).equals("?")
                    .and(COLUMNS[2]).equals(SQLServer.toSqlServerDateTime("?"))
                    .and(COLUMNS[5]).equals("?")
                    .toQuery();

            values.clear();
            values.put(COLUMNS[1], created.getRutSolicitante());
            values.put(COLUMNS[2], DateFormatWrapper.toDatabaseForm(created.getFechaSolicitud()));
            values.put(COLUMNS[5], created.getRutSupervisor());

            Result result = db.preparedQuery(queryStr, values);
            ResultSet set = result.getResultSet();
            set.next();
            int retval = set.getInt(1);
            result.close();

            return retval;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return -1;
        }
    }

    @Override
    public int createMassSolicitud(Solicitud created) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return -1;
        }

        String queryStr = new QueryBuilder().newQuery()
                .insert()
                .into(TABLE_NAME, COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[6])
                .values("?", SQLServer.toSqlServerDateTime("?"), "?", "?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[1], created.getRutSolicitante());
        values.put(COLUMNS[2], DateFormatWrapper.toDatabaseForm(created.getFechaSolicitud()));
        values.put(COLUMNS[3], created.getEstado());
        values.put(COLUMNS[6], created.getRutAprueba());

        try {
            db.preparedUpdateQuery(queryStr, values);

            queryStr = new QueryBuilder().newQuery()
                    .select(FunctionClause.max(COLUMNS[0]).as(COLUMNS[0]).toString())
                    .from(TABLE_NAME)
                    .where(COLUMNS[1]).equals("?")
                    .and(COLUMNS[2]).equals(SQLServer.toSqlServerDateTime("?"))
                    .toQuery();

            values.clear();
            values.put(COLUMNS[1], created.getRutSolicitante());
            values.put(COLUMNS[2], DateFormatWrapper.toDatabaseForm(created.getFechaSolicitud()));

            Result result = db.preparedQuery(queryStr, values);
            ResultSet set = result.getResultSet();
            set.next();
            int retval = set.getInt(1);
            result.close();

            return retval;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return -1;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudesPendientesSupervisor(int rutSupervisor,
                                                                        int index,
                                                                        int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[3]).equals("PENDIENTE")
                .or(COLUMNS[3]).equals("ESPERA");

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .where(COLUMNS[5]).equals(rutSupervisor)
                .and(subQuery)
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudesPendientes(int index, int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("PENDIENTE")
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudesEspera(int index, int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("ESPERA")
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public int getSolicitudesProcesadasCount() {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("ENTREGADO")
                .or(COLUMNS[3]).equals("RECHAZADO")
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public int getSolicitudesEsperaCount() {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("ESPERA")
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public int getSolicitudesPendientesCount() {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("PENDIENTE")
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public int getSolicitudesProcesadasSupervisorCount(int rutSupervisor) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[3]).equals("ENTREGADO")
                .or(COLUMNS[3]).equals("RECHAZADO");

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .where(COLUMNS[5]).equals(rutSupervisor)
                .and(subQuery)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public int getAllSolicitudesCount() {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public Solicitud getSolicitud(int nsolicitud) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return null;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(nsolicitud)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                return new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build();
            }
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return null;
        }

        return null;
    }

    @Override
    public ObservableList<Solicitud> getSolicitudesProcesadas(int index, int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .where(COLUMNS[3]).equals("ENTREGADO")
                .or(COLUMNS[3]).equals("RECHAZADO")
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudesProcesadasSupervisor(int rutSupervisor, int index,
                                                                        int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[3]).equals("ENTREGADO")
                .or(COLUMNS[3]).equals("RECHAZADO");

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .where(COLUMNS[5]).equals(rutSupervisor)
                .and(subQuery)
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getAllSolicitudes(int index, int pageSize) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        OrderClause order = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(order).as("ROW_ID").toString())
                .from(TABLE_NAME)
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select(COLUMNS[0], COLUMNS[1], COLUMNS[2], COLUMNS[3], COLUMNS[4], COLUMNS[5], COLUMNS[6])
                .from(subTable)
                .where("ROW_ID").between((pageSize * index) + 1).and((pageSize * index) + pageSize)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudes(String from, String to) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME)
                .where(COLUMNS[2]).graterThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(from)))
                .and(COLUMNS[2]).lessThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(to)))
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudes(String from, String to, int rutSupervisor, String tipo) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[2]).graterThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(from)))
                .and(COLUMNS[2]).lessThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(to)));

        WhereClause query = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME);

        String queryStr = "";

        if (tipo.equals("SUPERVISOR")) {
            queryStr = query.where(COLUMNS[5]).equals(rutSupervisor).and(subQuery).toQuery();
        } else if (tipo.equals("COLABORADOR")) {
            queryStr = query.where(COLUMNS[1]).equals(rutSupervisor).and(subQuery).toQuery();
        }

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public ObservableList<Solicitud> getSolicitudes(String from, String to, int codCcto) {
        ObservableList<Solicitud> solicitudes = FXCollections.observableArrayList();

        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return solicitudes;
        }

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[2]).graterThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(from)))
                .and(COLUMNS[2]).lessThanOrEqual(SQLServer.toSqlServerDateTime(DateFormatWrapper.toDatabaseForm(to)));

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(TABLE_NAME + " S, vt_Personal P")
                .where("S." + COLUMNS[1]).equals("P.RRHRUT")
                .and(subQuery)
                .and("P.COD_CCTO_ENTERO").equals(codCcto)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)) {
            ResultSet set = result.getResultSet();

            while (set.next()) {
                Date fechaSolRet = set.getDate(COLUMNS[2]);
                String fechaSol = null;

                if (fechaSolRet != null) {
                    fechaSol = DateFormatWrapper.toLatinForm(fechaSolRet.toString());
                }

                solicitudes.add(new Solicitud.Builder()
                        .setNumero(set.getInt(COLUMNS[0]))
                        .setRutSolicitante(set.getInt(COLUMNS[1]))
                        .setFechaSolicitud(fechaSol)
                        .setEstado(set.getString(COLUMNS[3]))
                        .setAdminComentario(set.getString(COLUMNS[4]))
                        .setRutSupervisor(set.getInt(COLUMNS[5]))
                        .setRutAprueba(set.getInt(COLUMNS[6]))
                        .build());
            }

            return solicitudes;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return solicitudes;
        }
    }

    @Override
    public int getSolicitudesPendientesSupervisorCount(int rutSupervisor) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return 0;
        }

        ConnectorClause subQuery = new WhereClause()
                .column(COLUMNS[3]).equals("PENDIENTE")
                .or(COLUMNS[3]).equals("ESPERA");

        String queryStr = new QueryBuilder().newQuery()
                .select(FunctionClause.count().toString())
                .from(TABLE_NAME)
                .where(COLUMNS[5]).equals(rutSupervisor)
                .and(subQuery)
                .toQuery();

        try (Result result = db.rawQuery(queryStr)){
            ResultSet set = result.getResultSet();
            if (set.next()) {
                return set.getInt(1);
            }

            return 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return 0;
        }
    }

    @Override
    public boolean updateEstadoSolicitud(Solicitud updated) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .update()
                .into(TABLE_NAME)
                .set()
                .column(COLUMNS[3]).equals("?")
                .comma()
                .column(COLUMNS[4]).equals("?")
                .comma()
                .column(COLUMNS[6]).equals("?")
                .where(COLUMNS[0]).equals("?")
                .toQuery();

        ContentValues values = new ContentValues();
        values.put(COLUMNS[3], updated.getEstado());
        values.put(COLUMNS[4], updated.getAdminComentario());
        values.put(COLUMNS[6], updated.getRutAprueba());
        values.put(COLUMNS[0], updated.getNumero());

        try {
            return db.preparedUpdateQuery(queryStr, values) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }

    @Override
    public boolean deleteSolicitud(int nSolicitud) {
        if (db == null || TABLE_NAME == null || COLUMNS == null) {
            return false;
        }

        String queryStr = new QueryBuilder().newQuery()
                .delete()
                .from(TABLE_NAME)
                .where(COLUMNS[0]).equals(nSolicitud)
                .toQuery();

        try {
            return db.rawUpdateQuery(queryStr) != 0;
        } catch (SQLException e) {
            LoggerWrapper.getLogger().log(Level.SEVERE, e.toString(), e);

            return false;
        }
    }
}
