/*
 * SolicitudInsumoDetalleDaoImpl.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model.access;

import cl.camanchaca.sepp.model.Insumo;
import cl.camanchaca.sepp.model.SolicitudInsumoDetalle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudInsumoDetalleDaoImpl implements SolicitudInsumoDetalle.Dao {
    @Override
    public ObservableList<SolicitudInsumoDetalle> getSolicitudInsumo(int solicitud) {
        ObservableList<SolicitudInsumoDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudInsumoDaoImpl().getSolicitudInsumo(solicitud).forEach(solicitudInsumo ->
            detalle.add(new SolicitudInsumoDetalle(solicitudInsumo, new InsumoDaoImpl().getInsumo
                    (solicitudInsumo.getInsumo())))
        );

        return detalle;
    }

    @Override
    public ObservableList<SolicitudInsumoDetalle> getSolicitudInsumoPorInsumo(String from,
                                                                              String to,
                                                                              Insumo insumo) {
        ObservableList<SolicitudInsumoDetalle> detalle = FXCollections.observableArrayList();

        new SolicitudInsumoDaoImpl()
                .getSolicitudInsumo(from, to, insumo.getCodigo())
                .forEach(solicitudInsumo ->
                        detalle.add(new SolicitudInsumoDetalle(solicitudInsumo, insumo))
        );

        return detalle;
    }
}
