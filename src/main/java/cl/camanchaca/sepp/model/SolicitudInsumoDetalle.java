/*
 * SolicitudInsumoDetale.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class SolicitudInsumoDetalle {
    private SolicitudInsumo solicitudInsumo;
    private Insumo insumo;

    public SolicitudInsumoDetalle(SolicitudInsumo solicitudInsumo, Insumo insumo)  {
        this.solicitudInsumo = solicitudInsumo;
        this.insumo = insumo;
    }

    public SolicitudInsumo getSolicitudInsumo() {
        return solicitudInsumo;
    }

    public Insumo getInsumo() {
        return insumo;
    }

    public interface Dao {
        ObservableList<SolicitudInsumoDetalle> getSolicitudInsumo(int solicitud);
        ObservableList<SolicitudInsumoDetalle> getSolicitudInsumoPorInsumo(String from,
                                                                           String to,
                                                                           Insumo insumo);
    }
}
