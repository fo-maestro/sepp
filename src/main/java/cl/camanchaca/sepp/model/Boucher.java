/*
 * Boucher.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.model;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class Boucher {
    private int codigo;
    private String nombre;
    private int solicitado;
    private int entregado;
    private String fecha;
    private String observacion;
    private String firma = "";

    private Boucher(int codigo, String nombre, int solicitado, int entregado, String fecha, String observacion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.solicitado = solicitado;
        this.entregado = entregado;
        this.fecha = fecha;
        this.observacion = observacion;
    }

    public int getCodigo() {
        return codigo;
    }

    public Boucher setCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public Boucher setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public int getSolicitado() {
        return solicitado;
    }

    public Boucher setSolicitado(int solicitado) {
        this.solicitado = solicitado;
        return this;
    }

    public int getEntregado() {
        return entregado;
    }

    public Boucher setEntregado(int entregado) {
        this.entregado = entregado;
        return this;
    }

    public String getFecha() {
        return fecha;
    }

    public Boucher setFecha(String fecha) {
        this.fecha = fecha;
        return this;
    }

    public String getObservacion() {
        return observacion;
    }

    public Boucher setObservacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public String getFirma() {
        return firma;
    }

    public Boucher setFirma(String firma) {
        this.firma = firma;
        return this;
    }

    public static final class Builder {
        private int codigo;
        private String nombre = "";
        private int solicitado;
        private int entregado;
        private String fecha = "";
        private String observacion = "";

        public Builder setCodigo(int codigo) {
            this.codigo = codigo;
            return this;
        }

        public Builder setNombre(String nombre) {
            this.nombre = nombre;
            return this;
        }

        public Builder setSolicitado(int solicitado) {
            this.solicitado = solicitado;
            return this;
        }

        public Builder setEntregado(int entregado) {
            this.entregado = entregado;
            return this;
        }

        public Builder setFecha(String fecha) {
            this.fecha = fecha;
            return this;
        }

        public Builder setObservacion(String observacion) {
            this.observacion = observacion;
            return this;
        }

        public Boucher build() {
            return new Boucher(codigo, nombre, solicitado, entregado, fecha, observacion);
        }
    }
}
