/*
 * Usuarios.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.model;

import cl.camanchaca.sepp.database.Table;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
@Table(name = "Cuenta", columns = {"RRHRUT", "USUARIO", "CONTRA", "NIVEL"})
public final class Cuenta {
    private IntegerProperty rut;
    private StringProperty usuario;
    private StringProperty contra;
    private IntegerProperty nivel;

    private Cuenta(int rut, String usuario, String contra, int nivel) {
        this.rut = new SimpleIntegerProperty(rut);
        this.usuario = new SimpleStringProperty(usuario);
        this.contra = new SimpleStringProperty(contra);
        this.nivel = new SimpleIntegerProperty(nivel);
    }

    public int getRut() {
        return rut.get();
    }

    public IntegerProperty rutProperty() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut.set(rut);
    }

    public String getUsuario() {
        return usuario.get();
    }

    public StringProperty usuarioProperty() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    public String getContra() {
        return contra.get();
    }

    public StringProperty contraProperty() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra.set(contra);
    }

    public int getNivel() {
        return nivel.get();
    }

    public IntegerProperty nivelProperty() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel.set(nivel);
    }

    public static final class Builder {
        private int rut;
        private String usuario;
        private String contra;
        private int nivel;

        public Builder() {
            usuario = "";
            contra = "";
        }

        public Builder setRut(int rut) {
            this.rut = rut;

            return this;
        }

        public Builder setUsuario(String usuario) {
            this.usuario = usuario;

            return this;
        }

        public Builder setContra(String contra) {
            this.contra = contra;

            return this;
        }

        public Builder setNivel(int nivel) {
            this.nivel = nivel;

            return this;
        }

        public Cuenta build() {
            return new Cuenta(rut, usuario, contra, nivel);
        }
    }

    public interface Dao {
        Cuenta getCuenta(String usuario);
        ObservableList<Cuenta> getAllCuentas();
        boolean createCuenta(Cuenta created);
        boolean updateCuenta(Cuenta updated);
        boolean deleteCuenta(int rut);
    }
}
