/*
 * FileUtils.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.util;

import java.io.File;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class FileUtils {
    private FileUtils() {
    }

    public static String getExtension(File file) {
        if (file == null) {
            return "";
        }

        return file.getName().substring(file.getName().indexOf(".") + 1, file.getName().length());
    }
}
