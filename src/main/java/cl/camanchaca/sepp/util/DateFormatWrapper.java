/*
 * DateFormatWrapper.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class DateFormatWrapper {

    public static String toLatinForm(String date) {
        return LocalDate.parse(date).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public static LocalDate fromLatinForm(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public static String toDatabaseForm(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")).toString() +
                " 00:00:00.000";
    }
}
