/*
 * LoggerWrapper.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.util;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.*;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class LoggerWrapper {
    private static Logger logger = null;

    public static Logger getLogger() {
        if (logger == null) {
            initLogger();
            return logger;
        }

        return logger;
    }

    private static void initLogger() {
        logger = Logger.getGlobal();
        Logger rootLogger = Logger.getLogger("");

        Handler[] handlers = rootLogger.getHandlers();

        if (handlers[0] instanceof ConsoleHandler) {
            rootLogger.removeHandler(handlers[0]);
        }

        logger.setLevel(Level.SEVERE);

        try {
            File logDir = new File("logs");

            if (!logDir.exists()) {
                logDir.mkdir();
            }
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");

            logger.addHandler(new FileHandler("logs/log_" + dtf.format(LocalDateTime.now()) + ".xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
