/*
 * Resource.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class Result implements AutoCloseable {
    private Statement statement;
    private ResultSet resultSet;

    Result(Statement statement, ResultSet resultSet) {
        this.statement = statement;
        this.resultSet = resultSet;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    @Override
    public void close() throws SQLException {
        if (!resultSet.isClosed()) {
            resultSet.close();
        }

        if (!statement.isClosed()) {
            statement.close();
        }
    }
}
