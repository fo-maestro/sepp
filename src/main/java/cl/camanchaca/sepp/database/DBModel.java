/*
 * DBModel.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.database;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public abstract class DBModel {
    protected String TABLE_NAME;
    protected String[] COLUMNS;
    protected String[] TRIGGERS;

    public <T> DBModel(Class<T> clazz) {
        if (clazz != null) {
            if (clazz.isAnnotationPresent(Table.class)) {
                Table annotation = clazz.getAnnotation(Table.class);

                TABLE_NAME = annotation.name();
                COLUMNS = annotation.columns();
            }

            if (clazz.isAnnotationPresent(Trigger.class)) {
                TRIGGERS = clazz.getAnnotation(Trigger.class).names();
            }
        }
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public String[] getColumns() {
        return COLUMNS.clone();
    }

    public String[] getTriggers() {
        return TRIGGERS;
    }
}
