/*
 * WhereClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class WhereClause extends Query {

    public WhereClause() {
        super();
    }

    public WhereClause(StringBuilder query) {
        super(query);
    }

    public ConnectorClause where(ConnectorClause subQuery) {
        query.append(" WHERE ").append(subQuery.toTable());

        return new ConnectorClause(query);
    }

    public WhereClause where(String column) {
        query.append(" WHERE ").append(column);

        return this;
    }

    public ConnectorClause equals(String value) {
        query.append(" = ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause equals(int value) {
        query.append(" = ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause equals(FunctionClause value) {
        query.append(" = ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause distinct(String value) {
        query.append(" <> ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause distinct(int value) {
        query.append(" <> ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause distinct(FunctionClause value) {
        query.append(" <> ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThan(String value) {
        query.append(" < ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThan(int value) {
        query.append(" < ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThan(FunctionClause value) {
        query.append(" < ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThanOrEqual(String value) {
        query.append(" <= ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThanOrEqual(int value) {
        query.append(" <= ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause lessThanOrEqual(FunctionClause value) {
        query.append(" <= ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause greaterThan(String value) {
        query.append(" > ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause greaterThan(int value) {
        query.append(" > ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause greaterThan(FunctionClause value) {
        query.append(" > ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause graterThanOrEqual(String value) {
        query.append(" >= ");

        if (value.equals("?") || value.contains(".")) {
            query.append(value);
        } else {
            query.append("'").append(value).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause graterThanOrEqual(int value) {
        query.append(" >= ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause graterThanOrEqual(FunctionClause value) {
        query.append(" >= ").append(value);

        return new ConnectorClause(query);
    }

    public ConnectorClause like(String pattern) {
        query.append(" LIKE ");

        if (pattern.equals("?") || pattern.contains(".")) {
            query.append(pattern);
        } else {
            query.append("'").append(pattern).append("'");
        }

        return new ConnectorClause(query);
    }

    public ConnectorClause between(int number) {
        query.append(" BETWEEN ").append(number);

        return new ConnectorClause(query);
    }

    public WhereClause values(Object... values) {
        query.append(" VALUES (");

        for (int i = 0; i < values.length - 1; i++) {
            if (values[i] instanceof String) {
                if (values[i].equals("?") || ((String) values[i]).contains(".")) {
                    query.append(values[i]).append(", ");
                } else {
                    query.append("'").append(values[i]).append("', ");
                }
            } else if (values[i] instanceof Integer || values[i] instanceof FunctionClause) {
                query.append(values[i]).append(", ");
            }
        }

        if (values[values.length - 1] instanceof String) {
            if (values[values.length - 1].equals("?") || ((String)values[values.length - 1])
                    .contains(".")) {
                query.append(values[values.length - 1]).append(")");
            } else {
                query.append("'").append(values[values.length - 1]).append("')");
            }
        } else if (values[values.length - 1] instanceof Integer
                || values[values.length - 1] instanceof FunctionClause) {
            query.append(values[values.length - 1]).append(")");
        }

        return this;
    }

    public WhereClause set() {
        query.append(" SET");

        return this;
    }

    public WhereClause column(String column) {
        query.append(" ").append(column);

        return this;
    }

    public WhereClause value(int value) {
        query.append(" ").append(value);

        return this;
    }

    public OrderClause orderBy(String column) {
        query.append(" ORDER BY ").append(column);

        return new OrderClause(query);
    }

    public OrderClause groupBy(String column) {
        query.append(" GROUP BY ").append(column);

        return new OrderClause(query);
    }

    public WhereClause not() {
        query.append(" NOT");

        return new WhereClause(query);
    }
}
