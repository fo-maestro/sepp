/*
 * FunctionClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class FunctionClause extends Query {

    private FunctionClause() {
        super();
    }

    private FunctionClause(StringBuilder function) {
        super(function);
    }

    public static FunctionClause convert(Type type, String data, int format) {
        if (!data.equals("?")) {
            data = "'" + data + "'";
        }

        StringBuilder function = new StringBuilder();
        function.append("CONVERT(")
                .append(type)
                .append(", ")
                .append(data)
                .append(", ")
                .append(format)
                .append( ")");

        return new FunctionClause(function);
    }

    public static FunctionClause convert(Type type, int data, int format) {
        StringBuilder function = new StringBuilder();

        function.append("CONVERT(")
                .append(type)
                .append(", ")
                .append(data)
                .append(format);

        return new FunctionClause(function);
    }

    public static FunctionClause max(String column) {
        StringBuilder function = new StringBuilder();

        function.append("MAX(").append(column).append(")");

        return new FunctionClause(function);
    }

    public static FunctionClause count() {
        StringBuilder funtion = new StringBuilder();
        funtion.append("COUNT(*)");

        return new FunctionClause(funtion);
    }

    public static FunctionClause row_number() {
        StringBuilder function = new StringBuilder();
        function.append("ROW_NUMBER()");

        return new FunctionClause(function);
    }

    public static FunctionClause exists(String table) {
        StringBuilder function = new StringBuilder();
        function.append(" EXISTS ").append(table);

        return new FunctionClause(function);
    }

    public FunctionClause over(OrderClause clause) {
        query.append(" OVER(").append(clause.toQuery()).append(")");

        return this;
    }

    public FunctionClause as(String colName) {
        query.append(" AS ").append(colName);

        return this;
    }

    @Override
    public String toString() {
        return toQuery();
    }
}
