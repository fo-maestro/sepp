/*
 * FromClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class FromClause extends Query {

    public FromClause() {
        super();
    }

    public FromClause(StringBuilder query) {
        super(query);
    }

    public WhereClause from(String table) {
        query.append(" FROM ").append(table);

        return new WhereClause(query);
    }

    public WhereClause into(String table) {
        query.append(" ").append(table);

        return new WhereClause(query);
    }

    public WhereClause into(String table, String... columns) {
        query.append(" ").append(table).append("(");

        for (int i = 0; i < columns.length - 1; i++) {
            query.append(columns[i]).append(", ");
        }

        query.append(columns[columns.length - 1]).append(")");

        return new WhereClause(query);
    }
}
