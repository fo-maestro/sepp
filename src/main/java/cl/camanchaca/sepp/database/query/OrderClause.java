/*
 * OrderClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class OrderClause extends Query {

    public OrderClause() {
        super();
    }

    public OrderClause(StringBuilder query) {
        super(query);
    }

    public OrderClause asc() {
        query.append(" ASC");

        return this;
    }

    public OrderClause desc() {
        query.append(" DESC");

        return this;
    }

    public WhereClause having(String column) {
        query.append(" HAVING ").append(column);

        return new WhereClause(query);
    }

    public OrderClause offset(int row) {
        query.append(" OFFSET ").append(row).append(" ROWS");

        return this;
    }

    public OrderClause fetchNext(int rows) {
        query.append(" FETCH NEXT ").append(rows).append(" ROWS ONLY");

        return this;
    }
}
