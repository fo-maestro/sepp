/*
 * ConditionClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ConditionClause extends Query {
    public ConditionClause() {
        super();
    }

    public ConditionClause(StringBuilder query) {
        super(query);
    }

    public ConditionClause If(String condition) {
        query.append("IF ").append(condition);

        return this;
    }

    public ConditionClause then(String subQuery) {
        query.append(" BEGIN ").append(subQuery).append(" END ");

        return this;
    }

    public ConditionClause Else(String subQuery) {
        query.append("ELSE").append(new ConditionClause().then(subQuery).query);

        return this;
    }
}
