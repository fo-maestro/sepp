/*
 * QueryBuilder.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class QueryBuilder {

    public SelectClause newQuery() {
        return new SelectClause();
    }

    public ConditionClause newCondition() {
        return new ConditionClause();
    }
}
