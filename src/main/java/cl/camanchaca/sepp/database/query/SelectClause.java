/*
 * SelectClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SelectClause extends Query {

    public FromClause select() {
        query.append("SELECT *");

        return new FromClause(query);
    }

    public FromClause select(String... columns) {
        query.append("SELECT ");

        for (int i = 0; i < columns.length - 1; i++) {
            query.append(columns[i]).append(", ");
        }

        query.append(columns[columns.length - 1]);

        return new FromClause(query);
    }

    public FromClause insert() {
        query.append("INSERT");

        return new FromClause(query);
    }

    public FromClause update() {
        query.append("UPDATE");

        return new FromClause(query);
    }

    public FromClause delete() {
        query.append("DELETE");

        return new FromClause(query);
    }
}
