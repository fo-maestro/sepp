/*
 * ConnectorClause.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ConnectorClause extends Query{

    public ConnectorClause() {
        super();
    }

    public ConnectorClause(StringBuilder query) {
        super(query);
    }

    public WhereClause and(String column) {
        query.append(" AND ").append(column);

        return new WhereClause(query);
    }

    public ConnectorClause and(Query subQuery) {
        query.append(" AND ").append(subQuery.toTable());

        return this;
    }

    public ConnectorClause and(int number) {
        query.append(" AND ").append(number);

        return this;
    }

    public WhereClause or(String column) {
        query.append(" OR ").append(column);

        return new WhereClause(query);
    }

    public ConnectorClause or(Query subQuery) {
        query.append(" OR ").append(subQuery.toTable());

        return this;
    }

    public OrderClause orderBy(String column) {
        query.append(" ORDER BY ").append(column);

        return new OrderClause(query);
    }

    public OrderClause groupBy(String column) {
        query.append(" GROUP BY ").append(column);

        return new OrderClause(query);
    }

    public WhereClause comma() {
        query.append(", ");

        return new WhereClause(query);
    }

    public WhereClause where(String column) {
        query.append(" WHERE ").append(column);

        return new WhereClause(query);
    }
}
