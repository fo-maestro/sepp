/*
 * Query.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database.query;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public abstract class Query {
    protected StringBuilder query;

    public Query() {
        query = new StringBuilder();
    }

    public Query(StringBuilder query) {
        this.query = query;
    }

    public String toQuery() {
        return query.toString();
    }

    public String toTable() {
        return "(" + query.toString() + ")";
    }

    public String toTable(String tableName) {
        return "(" + query.toString() + ") AS " + tableName;
    }
}
