/*
 * DBHelper.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.database;

import cl.camanchaca.sepp.content.ContentValues;
import cl.camanchaca.sepp.service.Service;

import java.sql.*;
import java.util.Iterator;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
abstract class DBHelper implements Service {
    private String jdbc_driver;
    private String db_url;
    private String user;
    private String password;
    private Connection connection;

    DBHelper(String jdbc_driver, String engine, String hostname, int port, String database,
                    String user, String password) {
        this.jdbc_driver = jdbc_driver;
        this.user = user;
        this.password = password;
        db_url = "jdbc:" + engine + "://" + hostname + ":" + port + ";databaseName=" + database;
    }

    public void connect() throws ClassNotFoundException, SQLException {
        Class.forName(jdbc_driver);
        connection = DriverManager.getConnection(db_url, user, password);
    }

    public Connection getConnection() {
        return connection;
    }

    public Result rawQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(query);

        return new Result(statement, result);
    }

    public int rawUpdateQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        int status = statement.executeUpdate(query);
        statement.close();

        return status;
    }

    public Result preparedQuery(String query, Object[] params) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        if (params == null) {
            return null;
        }

        for (int i = 1; i <= params.length; i++) {
            statement.setObject(i, params[i - 1]);
        }

        ResultSet result = statement.executeQuery();

        return new Result(statement, result);
    }

    public Result preparedQuery(String query, ContentValues params) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        if (params == null) {
            return null;
        }

        Iterator<Object> it = params.values().iterator();

        for (int i = 1; it.hasNext(); i++) {
            statement.setObject(i, it.next());
        }

        ResultSet result = statement.executeQuery();

        return new Result(statement, result);
    }

    public int preparedUpdateQuery(String query, Object[] params) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        if (params == null) {
            return 0;
        }

        for (int i = 1; i <= params.length; i++) {
            statement.setObject(i, params[i - 1]);
        }

        int status = statement.executeUpdate();
        statement.close();

        return status;
    }

    public int preparedUpdateQuery(String query, ContentValues params) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        if (params == null) {
            return 0;
        }

        Iterator<Object> it = params.values().iterator();

        for (int i = 1; it.hasNext(); i++) {
            statement.setObject(i, it.next());
        }

        int status = statement.executeUpdate();
        statement.close();

        return status;
    }

    public void enableTrigger(String tableName, String triggerName) throws SQLException {
        rawUpdateQuery("ENABLE TRIGGER " + triggerName + " ON " + tableName);
    }

    public void disableTrigger(String tableName, String triggerName) throws SQLException {
        rawUpdateQuery("DISABLE TRIGGER " + triggerName + " ON " + tableName);
    }

    public void close() throws SQLException {
        connection.close();
    }

    public static abstract class ConnectionBuilder<T> {
        String hostname;
        int port;
        String database;
        String user;
        String password;

        public ConnectionBuilder<T> setHostname(String hostname) {
            this.hostname = hostname;

            return this;
        }

        public ConnectionBuilder<T> setPort(int port) {
            this.port = port;

            return this;
        }

        public ConnectionBuilder<T> setDatabase(String database) {
            this.database = database;

            return this;
        }

        public ConnectionBuilder<T> setUser(String user) {
            this.user = user;

            return this;
        }

        public ConnectionBuilder<T> setPassword(String password) {
            this.password = password;

            return this;
        }

        public abstract T makeInstance();
    }
}
