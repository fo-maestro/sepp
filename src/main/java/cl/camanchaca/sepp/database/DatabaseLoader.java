/*
 * DatabaseLoader.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class DatabaseLoader {
    private DatabaseLoader() {
    }

    public static SQLServer loadDatabase(File configFile) throws ConfigFileException {
        if (!configFile.exists()) {
            throw new ConfigFileException("El archivo de configuracion especificado no existe");
        }


        try {
            SQLServer.Instance builder = new SQLServer.Instance();

            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(configFile);
            doc.getDocumentElement().normalize();

            NodeList root = doc.getElementsByTagName("connection");

            if (root.getLength() == 0) {
                throw new ConfigFileException("No se encuentra el elemento \"connection\" en el " +
                        "archivo de configuracion");
            }

            for (int i = 0; i < root.getLength(); i++) {
                if (root.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) root.item(i);

                    NodeList host = element.getElementsByTagName("host");

                    if (host.getLength() == 0) {
                        throw new ConfigFileException("No se encuentra el elemento \"host\" en el " +
                                "archivo de configuracion");
                    }

                    if (host.getLength() > 1) {
                        throw new ConfigFileException("El elemento \"host\" no posee el formato valido");
                    }

                    builder.setHostname(host.item(0).getTextContent());

                    NodeList database = element.getElementsByTagName("database");

                    if (database.getLength() == 0) {
                        throw new ConfigFileException("No se encuentra el elemento \"database\" en el " +
                                "archivo de configuracion");
                    }

                    if (database.getLength() > 1) {
                        throw new ConfigFileException("El elemento \"database\" no posee el formato valido");
                    }

                    builder.setDatabase(database.item(0).getTextContent());

                    NodeList user = element.getElementsByTagName("user");

                    if (user.getLength() == 0) {
                        throw new ConfigFileException("No se encuentra el elemento \"user\" en el " +
                                "archivo de configuracion");
                    }

                    if (user.getLength() > 1) {
                        throw new ConfigFileException("El elemento \"user\" no posee el formato valido");
                    }

                    builder.setUser(user.item(0).getTextContent());

                    NodeList password = element.getElementsByTagName("password");

                    if (password.getLength() == 0) {
                        throw new ConfigFileException("No se encuentra el elemento \"password\" en el " +
                                "archivo de configuracion");
                    }

                    if (password.getLength() > 1) {
                        throw new ConfigFileException("El elemento \"password\" no posee el formato valido");
                    }

                    builder.setPassword(password.item(0).getTextContent());

                    if (element.getElementsByTagName("port").getLength() == 1) {
                        builder.setPort(Integer.parseInt(
                                element.getElementsByTagName("port").item(0).getTextContent()));
                    }
                }
            }

            return builder.makeInstance();
        } catch (Exception e) {
            throw new ConfigFileException(e);
        }
    }
}
