/*
 * SQLServer.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.database;

import cl.camanchaca.sepp.database.query.FunctionClause;
import cl.camanchaca.sepp.database.query.Type;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SQLServer extends DBHelper {
    private static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String ENGINE = "sqlserver";

    SQLServer(String hostname, int port, String database, String user, String password) {
        super(JDBC_DRIVER, ENGINE, hostname, port, database, user, password);
    }

    public static FunctionClause toSqlServerDateTime(String dbDate) {
        return FunctionClause.convert(Type.DATETIME, dbDate, 21);
    }

    public static class Instance extends ConnectionBuilder<SQLServer> {

        public Instance() {
            port = 1433;
            user = "SA";
        }

        @Override
        public SQLServer makeInstance() {
            return new SQLServer(hostname, port, database, user, password);
        }
    }
}
