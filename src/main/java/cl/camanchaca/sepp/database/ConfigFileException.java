/*
 * ConfigFileException.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.database;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ConfigFileException extends Exception {
    public ConfigFileException() {
        super();
    }

    public ConfigFileException(String message) {
        super(message);
    }

    public ConfigFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigFileException(Throwable cause) {
        super(cause);
    }

    protected ConfigFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
