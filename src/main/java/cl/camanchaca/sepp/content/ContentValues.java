/*
 * ContentValues.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.content;

import java.util.LinkedHashMap;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class ContentValues extends LinkedHashMap<String, Object> {
}
