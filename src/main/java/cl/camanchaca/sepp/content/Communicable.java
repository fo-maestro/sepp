/*
 * Communicable.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.content;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public interface Communicable {
    void setData(ContentValues bundle);
    void update();
    ContentValues getData();
}
