/*
 * ServiceLocator.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp.service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class ServiceLocator {
    private static Cache cache = new Cache();

    @SuppressWarnings("unchecked")
    public static <T> T getService(Class<T> serviceClass) {
        return (T) cache.getService(serviceClass);
    }

    public static void registerService(Service newService) {
        cache.addService(newService);
    }

    public static void unregisterService(Class clazz) {
        cache.removeService(clazz);
    }

    private static class Cache {
        private List<Service> services = new ArrayList<>();

        Service getService(Class clazz) {
            for (Service service : services) {
                if (clazz.isInstance(service)) {
                    return service;
                }
            }

            return null;
        }

        void addService(Service newService) {
            for (Service service : services) {
                if (newService.getClass().isInstance(service)) {
                    return;
                }
            }

            services.add(newService);
        }

        void removeService(Class clazz) {
            for (Service service : services) {
                if (clazz.isInstance(service)) {
                    services.remove(service);

                    return;
                }
            }
        }
    }
}
