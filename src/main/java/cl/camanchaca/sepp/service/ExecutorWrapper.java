/*
 * ExecutorWrapper.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.service;

import java.util.concurrent.ExecutorService;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public final class ExecutorWrapper implements Service{
    private ExecutorService executor;

    public ExecutorWrapper(ExecutorService executor) {
        this.executor = executor;
    }

    public ExecutorService getExecutor() {
        return executor;
    }
}
