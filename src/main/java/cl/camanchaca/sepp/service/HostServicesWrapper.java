/*
 * HostServiceWrapper.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp.service;

import javafx.application.HostServices;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class HostServicesWrapper implements Service {
    private HostServices hostServices;

    public HostServicesWrapper(HostServices hostServices) {
        this.hostServices = hostServices;
    }

    public HostServices getHostServices() {
        return hostServices;
    }
}
