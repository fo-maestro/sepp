/*
 * UtilsTest.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp;

import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.FunctionClause;
import cl.camanchaca.sepp.database.query.QueryBuilder;
import cl.camanchaca.sepp.model.CentroCostos;
import cl.camanchaca.sepp.model.Cuenta;
import cl.camanchaca.sepp.model.Personal;
import cl.camanchaca.sepp.model.access.CentroCostosDaoImpl;
import cl.camanchaca.sepp.model.access.CuentaDaoImpl;
import cl.camanchaca.sepp.model.access.PersonalDaoImpl;
import cl.camanchaca.sepp.service.ServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class UtilsTest {
    private SQLServer db;

    @Before
    public void setUp() {
        SQLServer db = new SQLServer.Instance()
                .setHostname("192.168.3.33")
                .setDatabase("CARGOS")
                .setUser("usu_cargo")
                .setPassword("cargo.2017")
                .makeInstance();

        try {
            db.connect();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        ServiceLocator.registerService(db);
    }

    @Test
    public void reverseCcto() {
        Assert.assertEquals(180210019, CentroCostos.reverseCcto(180210190));
    }

    @Test
    public void personalTest() {
        String queryIF = new QueryBuilder().newQuery()
                .select("1")
                .from("vt_Personal")
                .where("COD_CCTO_ENTERO").equals(CentroCostos.reverseCcto(180210019))
                .and("CARGO").not().like("SUPERVISOR%")
                .toTable();

        String queryIfThen = new QueryBuilder().newQuery()
                .select()
                .from("vt_Personal")
                .where("COD_CCTO_ENTERO").equals(CentroCostos.reverseCcto(180210019))
                .or("RRHRUT").equals(10512842)
                .toQuery();

        String queryElse = new QueryBuilder().newQuery()
                .select()
                .from("vt_Personal")
                .where("COD_CCTO_ENTERO").equals(180210019)
                .and("CARGO").not().like("SUPERVISOR%")
                .or("RRHRUT").equals(10512842)
                .toQuery();

        String queryStr = new QueryBuilder().newCondition()
                .If(FunctionClause.exists(queryIF).toQuery())
                .then(queryIfThen)
                .Else(queryElse)
                .toQuery();

        System.out.println(queryStr);
    }

    @Test
    public void insertAllSup() {
        new PersonalDaoImpl().getAllSupervisores()
                .forEach(personal -> new CuentaDaoImpl()
                        .createCuenta(new Cuenta.Builder()
                                .setRut(personal.getRut())
                                .setUsuario((personal.getNombre().charAt(0) + personal.getApellidoP()).toLowerCase())
                                .setNivel(3)
                                .build()));
    }

    @Test
    public void testCctoComplement() {
        new CentroCostosDaoImpl().getCentroCostosCompleto(180210190)
        .forEach(centroCostos -> System.out.println(centroCostos.getNombre()));
    }
}
