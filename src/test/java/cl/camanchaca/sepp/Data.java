/*
 * Data.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class Data {
    private String state;
    private String branch;
    private String productLine;
    private String item;
    private long id;
    private long quantity;
    private float amount;

    public Data(String state, String branch, String productLine, String item, long id, long quantity, float amount) {
        this.state = state;
        this.branch = branch;
        this.productLine = productLine;
        this.item = item;
        this.id = id;
        this.quantity = quantity;
        this.amount = amount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
