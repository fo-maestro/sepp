/*
 * PoiTest.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */

package cl.camanchaca.sepp;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class PoiTest {

    @Test
    public void poiRead() throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook(new File("insumos.xlsx"));
        XSSFSheet spreadSheet = workbook.getSheetAt(0);

        for (int i = 1; i < spreadSheet.getPhysicalNumberOfRows(); i++) {
            Row row = spreadSheet.getRow(i);
            Iterator<Cell> cellIt = row.cellIterator();
            System.out.println(Integer.parseInt(cellIt.next().getStringCellValue()) + " " + cellIt.next().getStringCellValue() + " " + (int) cellIt.next().getNumericCellValue());
        }
    }

    @Test
    public void testToken() {
        String str = "data@@element";
        String[] tokens = str.split("@");

        System.out.println(tokens[1]);
    }
}
