/*
 * SeppAppTest.java
 *
 * Copyright (C) 2017 Felipe Oyarzun <http://fomaster.com>
 */
package cl.camanchaca.sepp;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import cl.camanchaca.sepp.database.Result;
import cl.camanchaca.sepp.database.SQLServer;
import cl.camanchaca.sepp.database.query.*;
import cl.camanchaca.sepp.model.*;
import cl.camanchaca.sepp.model.access.*;
import cl.camanchaca.sepp.service.ServiceLocator;
import cl.camanchaca.sepp.util.DateFormatWrapper;
import javafx.collections.ObservableList;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Felipe Oyarzun
 * @version 1.0
 */
public class SeppAppTest {
    private SQLServer db;

    @Before public void setUp() {
        SQLServer db = new SQLServer.Instance()
                .setHostname("FO-INFORMATICO\\SQLEXPRESS")
                .setPort(1000)
                .setDatabase("camanchaca")
                .setPassword("fodbdata")
                .makeInstance();

        try {
            db.connect();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        ServiceLocator.registerService(db);
    }

    @Test public void stringTest() {
        String inicio = "inicio";
        Assert.assertEquals(inicio, inicio.replace("_", " "));
    }


    @Test public void stressTest() {
        ServiceLocator.registerService(db);

        Solicitud solicitud = new Solicitud.Builder()
                .setRutSupervisor(10150773)
                .setEstado("RECHAZADO")
                .setFechaSolicitud("03-08-2017")
                .setRutSolicitante(10150773)
                .build();

        for (int i = 0; i < 400; i++) {
            new SolicitudDaoImpl().createSolicitud(solicitud);
        }

    }

    @Test public void pcNameTest() throws UnknownHostException {
        InetAddress addr = InetAddress.getLocalHost();
        System.out.println(addr.getHostName());
    }

    @Test public void timeTest() {
        long time = System.nanoTime();
        for (int i = 0; i < 500; i++) {
            new PersonalDaoImpl().getPersonal(10020014);
        }
        long deltaTime = (System.nanoTime() - time);
        System.out.println(TimeUnit.NANOSECONDS.toMillis(deltaTime));

        Assert.assertTrue(true);
    }

    @Test public void reportTest(){
        FastReportBuilder drb = new FastReportBuilder();
        DynamicReport dr = null;
        Style style = new Style();
        style.setBorder(Border.DASHED());

        AbstractColumn s = ColumnBuilder.getNew().setTitle("Quantity")
                .setColumnProperty("quantity", Long.class.getName())
                .setFixedWidth(true)
                .build();
        try {

            DynamicReportBuilder builder = drb.addColumn("State", "state", String.class.getName(),30)
                    .addColumn("Branch", "branch", String.class.getName(),30)
                    .addColumn("Product Line", "productLine", String.class.getName(),50)
                    .addColumn("Item", "item", String.class.getName(),50)
                    .addColumn("Item Code", "id", Long.class.getName(),30,true)
                    .addColumn("Amount", "amount", Float.class.getName(),70,true)
                    .setTitle("November 2006 sales report")
                    .setSubtitle("This report was generated at " + new Date())
                    .setPrintBackgroundOnOddRows(true)
                    .setUseFullPageWidth(true)
                    .setPageSizeAndOrientation(Page.Page_A4_Landscape())
                    .setDefaultStyles(null, null, null, style);

            dr = builder.addColumn(s).build();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        List<Data> collection = new ArrayList<>();
        collection.add(new Data("example", "sde", "sadad", "asdad",
                12, 12, 12.6f));
        JRDataSource ds = new JRBeanCollectionDataSource(collection);
        JasperPrint jp = null;
        try {
            jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
        } catch (JRException e) {
            e.printStackTrace();
        }

        try {
            JasperExportManager.exportReportToPdfFile(jp, "report.pdf");
        } catch (JRException e) {
            e.printStackTrace();
        }
        //JasperViewer.viewReport(jp);    //finally display the report report
    }

    @Test public void testQueryBuilder() {
        String queryStr = new QueryBuilder()
                .newQuery()
                .select("NINSUMO")
                .from("SOLICITUDES")
                .where("RUTSUPERVISOR")
                .equals(12345)
                .and("FECHA")
                .equals("27-02-21")
                .toQuery();

        System.out.println(queryStr);

        String testQuery = new QueryBuilder().newQuery()
                .select("MAX(NINSUMO) AS NINSUMO")
                .from("SOLICITUD")
                .toQuery();

        System.out.println(testQuery);

        String insertQuery = new QueryBuilder().newQuery()
                .insert()
                .into("Table", "column1", "olumn2")
                .values("?", 123, "STR")
                .toQuery();

        System.out.println(insertQuery);

        String updateQuery = new QueryBuilder().newQuery()
                .update()
                .into("Table")
                .set()
                .column("Column_1").equals(123)
                .comma()
                .column("Van").equals("Data")
                .comma()
                .column("Dar").equals("?")
                .where("Col_1").equals("dat")
                .toQuery();

        System.out.println(updateQuery);

        String deleteStr = new QueryBuilder().newQuery()
                .delete()
                .from("Table")
                .where("Name").equals("nm")
                .toQuery();

        System.out.println(deleteStr);
    }

    @Test public void transformTest() {
        try (Result result = db.rawQuery("SELECT FECSOLICITUD FROM Solicitud")) {
            ResultSet set = result.getResultSet();

            if (set.next()) {
                String dateStr = DateFormatWrapper.toLatinForm(set.getDate(1).toString());
                System.out.println(DateFormatWrapper.toDatabaseForm(dateStr));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test public void queryTest() {
        System.out.println(new QueryBuilder().newQuery()
                .select(SQLServer.toSqlServerDateTime("2017-08-27").as("Time").toString())
                .from("Personal")
                .where("Fecha").equals(SQLServer.toSqlServerDateTime("2019-01-22"))
                .toQuery());

        System.out.println(new QueryBuilder().newQuery()
                .insert()
                .into("Personal")
                .values(SQLServer.toSqlServerDateTime("2017-08-27"), "Data", 3)
                .toQuery());

        System.out.println(new QueryBuilder().newQuery()
                .update()
                .into("Personal")
                .set()
                .column("Fecha").equals(SQLServer.toSqlServerDateTime("2017-08-27"))
                .comma()
                .column("Nombre").equals("dasdasd")
                .comma()
                .column("Number").equals(23)
                .toQuery());

        System.out.println(new QueryBuilder().newQuery()
                .delete()
                .from("Personal")
                .where("Date").equals(SQLServer.toSqlServerDateTime("2017-08-27"))
                .toQuery());
    }

    @Test public void paginationTest() {
        /*SELECT *
                FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS ROW_ID FROM Solicitud) AS RESULT
        WHERE ROW_ID BETWEEN 1 AND 5;*/

        OrderClause subOrder = new WhereClause()
                .orderBy(new SelectClause().select("NULL").toTable());

        String subTable = new QueryBuilder().newQuery()
                .select("*", FunctionClause.row_number().over(subOrder).as("ROW_ID").toString())
                .toTable("RESULT");

        String queryStr = new QueryBuilder().newQuery()
                .select()
                .from(subTable)
                .where("ROW_ID").between(1).and(5)
                .toQuery();

        System.out.println(queryStr);
    }

    @Test public void groupByTest() {
        ObservableList<SolicitudDetalle> result =
                new SolicitudDetalleDaoImpl().getSolicitudDetalle("01-09-2017", "29-09-2017");

        result.forEach(solicitudDetalle -> {
            try {
                System.out.println(solicitudDetalle.getClass().getMethod("getCcto").invoke(solicitudDetalle));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        });
    }
}
